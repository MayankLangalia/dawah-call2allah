package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReferUserAddedListModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("verified_status")
    @Expose
    private String verifiedStatus;
    @SerializedName("pan_card_number")
    @Expose
    private Object panCardNumber;
    @SerializedName("uidai_number")
    @Expose
    private Object uidaiNumber;
    @SerializedName("front_photo_id")
    @Expose
    private Object frontPhotoId;
    @SerializedName("back_photo_id")
    @Expose
    private Object backPhotoId;
    @SerializedName("short_video")
    @Expose
    private Object shortVideo;
    @SerializedName("pan_card_photo")
    @Expose
    private Object panCardPhoto;
    @SerializedName("signature_photo")
    @Expose
    private Object signaturePhoto;
    @SerializedName("user_avatar")
    @Expose
    private String userAvatar;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_token")
    @Expose
    private String userToken;
    @SerializedName("share_code")
    @Expose
    private String shareCode;
    @SerializedName("wallet_total")
    @Expose
    private String walletTotal;
    @SerializedName("amount")
    @Expose
    private String amount;
    public final static Parcelable.Creator<ReferUserAddedListModel> CREATOR = new Creator<ReferUserAddedListModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ReferUserAddedListModel createFromParcel(Parcel in) {
            return new ReferUserAddedListModel(in);
        }

        public ReferUserAddedListModel[] newArray(int size) {
            return (new ReferUserAddedListModel[size]);
        }

    };

    protected ReferUserAddedListModel(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.fullName = ((String) in.readValue((String.class.getClassLoader())));
        this.phoneNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.username = ((String) in.readValue((String.class.getClassLoader())));
        this.password = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.verifiedStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.panCardNumber = ((Object) in.readValue((Object.class.getClassLoader())));
        this.uidaiNumber = ((Object) in.readValue((Object.class.getClassLoader())));
        this.frontPhotoId = ((Object) in.readValue((Object.class.getClassLoader())));
        this.backPhotoId = ((Object) in.readValue((Object.class.getClassLoader())));
        this.shortVideo = ((Object) in.readValue((Object.class.getClassLoader())));
        this.panCardPhoto = ((Object) in.readValue((Object.class.getClassLoader())));
        this.signaturePhoto = ((Object) in.readValue((Object.class.getClassLoader())));
        this.userAvatar = ((String) in.readValue((String.class.getClassLoader())));
        this.createdOn = ((String) in.readValue((String.class.getClassLoader())));
        this.userToken = ((String) in.readValue((String.class.getClassLoader())));
        this.shareCode = ((String) in.readValue((String.class.getClassLoader())));
        this.walletTotal = ((String) in.readValue((String.class.getClassLoader())));
        this.amount = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ReferUserAddedListModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVerifiedStatus() {
        return verifiedStatus;
    }

    public void setVerifiedStatus(String verifiedStatus) {
        this.verifiedStatus = verifiedStatus;
    }

    public Object getPanCardNumber() {
        return panCardNumber;
    }

    public void setPanCardNumber(Object panCardNumber) {
        this.panCardNumber = panCardNumber;
    }

    public Object getUidaiNumber() {
        return uidaiNumber;
    }

    public void setUidaiNumber(Object uidaiNumber) {
        this.uidaiNumber = uidaiNumber;
    }

    public Object getFrontPhotoId() {
        return frontPhotoId;
    }

    public void setFrontPhotoId(Object frontPhotoId) {
        this.frontPhotoId = frontPhotoId;
    }

    public Object getBackPhotoId() {
        return backPhotoId;
    }

    public void setBackPhotoId(Object backPhotoId) {
        this.backPhotoId = backPhotoId;
    }

    public Object getShortVideo() {
        return shortVideo;
    }

    public void setShortVideo(Object shortVideo) {
        this.shortVideo = shortVideo;
    }

    public Object getPanCardPhoto() {
        return panCardPhoto;
    }

    public void setPanCardPhoto(Object panCardPhoto) {
        this.panCardPhoto = panCardPhoto;
    }

    public Object getSignaturePhoto() {
        return signaturePhoto;
    }

    public void setSignaturePhoto(Object signaturePhoto) {
        this.signaturePhoto = signaturePhoto;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getShareCode() {
        return shareCode;
    }

    public void setShareCode(String shareCode) {
        this.shareCode = shareCode;
    }

    public String getWalletTotal() {
        return walletTotal;
    }

    public void setWalletTotal(String walletTotal) {
        this.walletTotal = walletTotal;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(fullName);
        dest.writeValue(phoneNumber);
        dest.writeValue(email);
        dest.writeValue(address);
        dest.writeValue(username);
        dest.writeValue(password);
        dest.writeValue(status);
        dest.writeValue(verifiedStatus);
        dest.writeValue(panCardNumber);
        dest.writeValue(uidaiNumber);
        dest.writeValue(frontPhotoId);
        dest.writeValue(backPhotoId);
        dest.writeValue(shortVideo);
        dest.writeValue(panCardPhoto);
        dest.writeValue(signaturePhoto);
        dest.writeValue(userAvatar);
        dest.writeValue(createdOn);
        dest.writeValue(userToken);
        dest.writeValue(shareCode);
        dest.writeValue(walletTotal);
        dest.writeValue(amount);
    }

    public int describeContents() {
        return 0;
    }
}