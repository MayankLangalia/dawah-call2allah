package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserFollowerModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("user_avatar")
    @Expose
    private String userAvatar;
    @SerializedName("verified_status")
    @Expose
    private String verifiedStatus;
    public final static Parcelable.Creator<DonorModel> CREATOR = new Creator<DonorModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DonorModel createFromParcel(Parcel in) {
            return new DonorModel(in);
        }

        public DonorModel[] newArray(int size) {
            return (new DonorModel[size]);
        }

    };

    protected UserFollowerModel(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.fullName = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.username = ((String) in.readValue((String.class.getClassLoader())));
        this.userAvatar = ((String) in.readValue((String.class.getClassLoader())));
        this.verifiedStatus = ((String) in.readValue((String.class.getClassLoader())));
    }

    public UserFollowerModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getVerifiedStatus() {
        return verifiedStatus;
    }

    public void setVerifiedStatus(String verifiedStatus) {
        this.verifiedStatus = verifiedStatus;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(fullName);
        dest.writeValue(email);
        dest.writeValue(username);
        dest.writeValue(userAvatar);
        dest.writeValue(verifiedStatus);
    }

    public int describeContents() {
        return 0;
    }

}
