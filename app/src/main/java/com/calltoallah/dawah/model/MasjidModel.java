package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MasjidModel implements Parcelable {

    @SerializedName("masjid_list")
    @Expose
    private List<MasjidListModel> masjidListModel = null;
    public final static Creator<MasjidModel> CREATOR = new Creator<MasjidModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MasjidModel createFromParcel(Parcel in) {
            return new MasjidModel(in);
        }

        public MasjidModel[] newArray(int size) {
            return (new MasjidModel[size]);
        }

    };

    protected MasjidModel(Parcel in) {
        in.readList(this.masjidListModel, (MasjidListModel.class.getClassLoader()));
    }

    public MasjidModel() {
    }

    public List<MasjidListModel> getMasjidListModel() {
        return masjidListModel;
    }

    public void setMasjidListModel(List<MasjidListModel> masjidListModel) {
        this.masjidListModel = masjidListModel;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(masjidListModel);
    }

    public int describeContents() {
        return 0;
    }

}