package com.calltoallah.dawah.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CustomerModel implements Serializable {
    private int id;
    private String name;
    private List<String> subCategoryList = new ArrayList<>();

    public CustomerModel(int id, String name, List<String> subCategoryList) {
        this.id = id;
        this.name = name;
        this.subCategoryList = subCategoryList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getSubCategoryList() {
        return subCategoryList;
    }

    public void setSubCategoryList(List<String> subCategoryList) {
        this.subCategoryList = subCategoryList;
    }
}
