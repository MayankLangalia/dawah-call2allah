package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomeModel  implements Parcelable {

    @SerializedName("masjid_post_list")
    @Expose
    private List<HomeMasjidPostList> homeMasjidPostList = null;
    @SerializedName("nearest_masjid")
    @Expose
    private List<MasjidListModel> masjidListModel = null;
    public final static Creator<HomeModel> CREATOR = new Creator<HomeModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public HomeModel createFromParcel(Parcel in) {
            return new HomeModel(in);
        }

        public HomeModel[] newArray(int size) {
            return (new HomeModel[size]);
        }

    };

    protected HomeModel(Parcel in) {
        in.readList(this.homeMasjidPostList, (HomeMasjidPostList.class.getClassLoader()));
        in.readList(this.masjidListModel, (MasjidListModel.class.getClassLoader()));
    }

    public HomeModel() {
    }

    public List<HomeMasjidPostList> getHomeMasjidPostList() {
        return homeMasjidPostList;
    }

    public void setHomeMasjidPostList(List<HomeMasjidPostList> homeMasjidPostList) {
        this.homeMasjidPostList = homeMasjidPostList;
    }

    public List<MasjidListModel> getHomeNearestMasjid() {
        return masjidListModel;
    }

    public void setHomeNearestMasjid(List<MasjidListModel> masjidListModel) {
        this.masjidListModel = masjidListModel;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(homeMasjidPostList);
        dest.writeList(masjidListModel);
    }

    public int describeContents() {
        return 0;
    }

}