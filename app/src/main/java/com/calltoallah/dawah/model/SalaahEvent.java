package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalaahEvent implements Parcelable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("salaah_name")
    @Expose
    private String salaahName;
    @SerializedName("salaah_location")
    @Expose
    private String salaahLocation;
    @SerializedName("salaah_timestamp")
    @Expose
    private String salaahTimestamp;
    public final static Creator<SalaahEvent> CREATOR = new Creator<SalaahEvent>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SalaahEvent createFromParcel(Parcel in) {
            return new SalaahEvent(in);
        }

        public SalaahEvent[] newArray(int size) {
            return (new SalaahEvent[size]);
        }

    }
            ;

    protected SalaahEvent(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.userId = ((String) in.readValue((String.class.getClassLoader())));
        this.salaahName = ((String) in.readValue((String.class.getClassLoader())));
        this.salaahLocation = ((String) in.readValue((String.class.getClassLoader())));
        this.salaahTimestamp = ((String) in.readValue((String.class.getClassLoader())));
    }

    public SalaahEvent() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSalaahName() {
        return salaahName;
    }

    public void setSalaahName(String salaahName) {
        this.salaahName = salaahName;
    }

    public String getSalaahLocation() {
        return salaahLocation;
    }

    public void setSalaahLocation(String salaahLocation) {
        this.salaahLocation = salaahLocation;
    }

    public String getSalaahTimestamp() {
        return salaahTimestamp;
    }

    public void setSalaahTimestamp(String salaahTimestamp) {
        this.salaahTimestamp = salaahTimestamp;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(userId);
        dest.writeValue(salaahName);
        dest.writeValue(salaahLocation);
        dest.writeValue(salaahTimestamp);
    }

    public int describeContents() {
        return 0;
    }

}