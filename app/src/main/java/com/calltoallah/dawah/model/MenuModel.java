package com.calltoallah.dawah.model;

public class MenuModel {

    private String menuID;
    private String menuTitle;
    private int menuIcon;

    public MenuModel(String menuID, String menuTitle, int menuIcon) {
        this.menuID = menuID;
        this.menuTitle = menuTitle;
        this.menuIcon = menuIcon;
    }

    public String getMenuID() {
        return menuID;
    }

    public void setMenuID(String menuID) {
        this.menuID = menuID;
    }

    public String getMenuTitle() {
        return menuTitle;
    }

    public void setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle;
    }

    public int getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(int menuIcon) {
        this.menuIcon = menuIcon;
    }
}
