package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TransactionModel implements Parcelable {
    @SerializedName("user_transection_list")
    @Expose
    private List<TransectionListModel> userTransectionList = null;
    public final static Parcelable.Creator<PostModel> CREATOR = new Creator<PostModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PostModel createFromParcel(Parcel in) {
            return new PostModel(in);
        }

        public PostModel[] newArray(int size) {
            return (new PostModel[size]);
        }

    };

    protected TransactionModel(Parcel in) {
        in.readList(this.userTransectionList, (TransectionListModel.class.getClassLoader()));
    }

    public TransactionModel() {
    }

    public List<TransectionListModel> getUserTransectionList() {
        return userTransectionList;
    }

    public void setUserTransectionList(List<TransectionListModel> userTransectionList) {
        this.userTransectionList = userTransectionList;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(userTransectionList);
    }

    public int describeContents() {
        return 0;
    }
}