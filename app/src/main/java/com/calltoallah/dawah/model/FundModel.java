package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FundModel implements Parcelable {

    @SerializedName("fundraiser_list")
    @Expose
    private List<FundListModel> fundraiserList = null;
    public final static Parcelable.Creator<FundModel> CREATOR = new Creator<FundModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public FundModel createFromParcel(Parcel in) {
            return new FundModel(in);
        }

        public FundModel[] newArray(int size) {
            return (new FundModel[size]);
        }

    };

    protected FundModel(Parcel in) {
        in.readList(this.fundraiserList, (FundListModel.class.getClassLoader()));
    }

    public FundModel() {
    }

    public List<FundListModel> getFundraiserList() {
        return fundraiserList;
    }

    public void setFundraiserList(List<FundListModel> fundraiserList) {
        this.fundraiserList = fundraiserList;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(fundraiserList);
    }

    public int describeContents() {
        return 0;
    }

}