package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuranDataModel implements Parcelable {

    @SerializedName("index")
    @Expose
    private String index;
    @SerializedName("sura")
    @Expose
    private String sura;
    @SerializedName("aya")
    @Expose
    private String aya;
    @SerializedName("en_text")
    @Expose
    private String en_text;
    @SerializedName("hi_text")
    @Expose
    private String hi_text;
    @SerializedName("ur_text")
    @Expose
    private String ur_text;
    @SerializedName("ar_text")
    @Expose
    private String ar_text;
    @SerializedName("translator_english")
    @Expose
    private String translator_english;
    @SerializedName("translator_hindi")
    @Expose
    private String translator_hindi;
    @SerializedName("translator_urdu")
    @Expose
    private String translator_urdu;
    @SerializedName("translator_arabic")
    @Expose
    private String translator_arabic;

    public String getTranslator_english() {
        return translator_english;
    }

    public void setTranslator_english(String translator_english) {
        this.translator_english = translator_english;
    }

    public String getTranslator_hindi() {
        return translator_hindi;
    }

    public void setTranslator_hindi(String translator_hindi) {
        this.translator_hindi = translator_hindi;
    }

    public String getTranslator_urdu() {
        return translator_urdu;
    }

    public void setTranslator_urdu(String translator_urdu) {
        this.translator_urdu = translator_urdu;
    }

    public String getTranslator_arabic() {
        return translator_arabic;
    }

    public void setTranslator_arabic(String translator_arabic) {
        this.translator_arabic = translator_arabic;
    }

    public String getText_english() {
        return en_text;
    }

    public void setText_english(String en_text) {
        this.en_text = en_text;
    }

    public String getText_hindi() {
        return hi_text;
    }

    public void setText_hindi(String hi_text) {
        this.hi_text = hi_text;
    }

    public String getText_urdu() {
        return ur_text;
    }

    public void setText_urdu(String ur_text) {
        this.ur_text = ur_text;
    }

    public String getText_arabic() {
        return ar_text;
    }

    public void setText_arabic(String ar_text) {
        this.ar_text = ar_text;
    }

    public final static Creator<QuranDataModel> CREATOR = new Creator<QuranDataModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public QuranDataModel createFromParcel(Parcel in) {
            return new QuranDataModel(in);
        }

        public QuranDataModel[] newArray(int size) {
            return (new QuranDataModel[size]);
        }

    };

    protected QuranDataModel(Parcel in) {
        this.index = ((String) in.readValue((String.class.getClassLoader())));
        this.sura = ((String) in.readValue((String.class.getClassLoader())));
        this.aya = ((String) in.readValue((String.class.getClassLoader())));
        this.en_text = ((String) in.readValue((String.class.getClassLoader())));
        this.hi_text = ((String) in.readValue((String.class.getClassLoader())));
        this.ur_text = ((String) in.readValue((String.class.getClassLoader())));
        this.ar_text = ((String) in.readValue((String.class.getClassLoader())));
        this.translator_english = ((String) in.readValue((String.class.getClassLoader())));
        this.translator_hindi = ((String) in.readValue((String.class.getClassLoader())));
        this.translator_urdu = ((String) in.readValue((String.class.getClassLoader())));
        this.translator_arabic = ((String) in.readValue((String.class.getClassLoader())));
    }

    public QuranDataModel() {
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getSura() {
        return sura;
    }

    public void setSura(String sura) {
        this.sura = sura;
    }

    public String getAya() {
        return aya;
    }

    public void setAya(String aya) {
        this.aya = aya;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(index);
        dest.writeValue(sura);
        dest.writeValue(aya);
        dest.writeValue(en_text);
        dest.writeValue(hi_text);
        dest.writeValue(ur_text);
        dest.writeValue(ar_text);
        dest.writeValue(translator_english);
        dest.writeValue(translator_hindi);
        dest.writeValue(translator_urdu);
        dest.writeValue(translator_arabic);
    }

    public int describeContents() {
        return 0;
    }

}