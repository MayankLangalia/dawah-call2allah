package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileUserInfo implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("verified_status")
    @Expose
    private String verify_status;
    @SerializedName("pan_card_number")
    @Expose
    private String pan_card_number;
    @SerializedName("uidai_number")
    @Expose
    private String uidai_number;
    @SerializedName("front_photo_id")
    @Expose
    private String front_photo_id;
    @SerializedName("back_photo_id")
    @Expose
    private String back_photo_id;
    @SerializedName("short_video")
    @Expose
    private String short_video;
    @SerializedName("pan_card_photo")
    @Expose
    private String pan_card_photo;
    @SerializedName("signature_photo")
    @Expose
    private String signature_photo;
    @SerializedName("user_avatar")
    @Expose
    private String user_avatar;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_token")
    @Expose
    private String userToken;
    @SerializedName("share_code")
    @Expose
    private String shareCode;
    @SerializedName("wallet_total")
    @Expose
    private String walletTotal;
    @SerializedName("user_donated")
    @Expose
    private String user_donated;
    @SerializedName("follower_count")
    @Expose
    private String follower_count;
    @SerializedName("following_count")
    @Expose
    private String following_count;

    public ProfileUserInfo() {
    }

    protected ProfileUserInfo(Parcel in) {
        id = in.readString();
        fullName = in.readString();
        phoneNumber = in.readString();
        email = in.readString();
        address = in.readString();
        username = in.readString();
        password = in.readString();
        status = in.readString();
        verify_status = in.readString();
        pan_card_number = in.readString();
        uidai_number = in.readString();
        front_photo_id = in.readString();
        back_photo_id = in.readString();
        short_video = in.readString();
        pan_card_photo = in.readString();
        signature_photo = in.readString();
        user_avatar = in.readString();
        createdOn = in.readString();
        userToken = in.readString();
        shareCode = in.readString();
        walletTotal = in.readString();
        user_donated = in.readString();
        follower_count = in.readString();
        following_count = in.readString();
    }

    public static final Creator<ProfileUserInfo> CREATOR = new Creator<ProfileUserInfo>() {
        @Override
        public ProfileUserInfo createFromParcel(Parcel in) {
            return new ProfileUserInfo(in);
        }

        @Override
        public ProfileUserInfo[] newArray(int size) {
            return new ProfileUserInfo[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVerify_status() {
        return verify_status;
    }

    public void setVerify_status(String verify_status) {
        this.verify_status = verify_status;
    }

    public String getPan_card_number() {
        return pan_card_number;
    }

    public void setPan_card_number(String pan_card_number) {
        this.pan_card_number = pan_card_number;
    }

    public String getUidai_number() {
        return uidai_number;
    }

    public void setUidai_number(String uidai_number) {
        this.uidai_number = uidai_number;
    }

    public String getFront_photo_id() {
        return front_photo_id;
    }

    public void setFront_photo_id(String front_photo_id) {
        this.front_photo_id = front_photo_id;
    }

    public String getBack_photo_id() {
        return back_photo_id;
    }

    public void setBack_photo_id(String back_photo_id) {
        this.back_photo_id = back_photo_id;
    }

    public String getShort_video() {
        return short_video;
    }

    public void setShort_video(String short_video) {
        this.short_video = short_video;
    }

    public String getPan_card_photo() {
        return pan_card_photo;
    }

    public void setPan_card_photo(String pan_card_photo) {
        this.pan_card_photo = pan_card_photo;
    }

    public String getSignature_photo() {
        return signature_photo;
    }

    public void setSignature_photo(String signature_photo) {
        this.signature_photo = signature_photo;
    }

    public String getUser_avatar() {
        return user_avatar;
    }

    public void setUser_avatar(String user_avatar) {
        this.user_avatar = user_avatar;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getShareCode() {
        return shareCode;
    }

    public void setShareCode(String shareCode) {
        this.shareCode = shareCode;
    }

    public String getWalletTotal() {
        return walletTotal;
    }

    public void setWalletTotal(String walletTotal) {
        this.walletTotal = walletTotal;
    }

    public String getUser_donated() {
        return user_donated;
    }

    public void setUser_donated(String user_donated) {
        this.user_donated = user_donated;
    }

    public String getFollower_count() {
        return follower_count;
    }

    public void setFollower_count(String follower_count) {
        this.follower_count = follower_count;
    }

    public String getFollowing_count() {
        return following_count;
    }

    public void setFollowing_count(String following_count) {
        this.following_count = following_count;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(fullName);
        dest.writeString(phoneNumber);
        dest.writeString(email);
        dest.writeString(address);
        dest.writeString(username);
        dest.writeString(password);
        dest.writeString(status);
        dest.writeString(verify_status);
        dest.writeString(pan_card_number);
        dest.writeString(uidai_number);
        dest.writeString(front_photo_id);
        dest.writeString(back_photo_id);
        dest.writeString(short_video);
        dest.writeString(pan_card_photo);
        dest.writeString(signature_photo);
        dest.writeString(user_avatar);
        dest.writeString(createdOn);
        dest.writeString(userToken);
        dest.writeString(shareCode);
        dest.writeString(walletTotal);
        dest.writeString(user_donated);
        dest.writeString(follower_count);
        dest.writeString(following_count);
    }
}