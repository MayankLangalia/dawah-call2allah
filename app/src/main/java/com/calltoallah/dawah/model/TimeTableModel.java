package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

public class TimeTableModel implements Parcelable {

    private String tt_name;
    private String tt_time;
    private String tt_id;

    public TimeTableModel(String name, String time) {
        this.tt_name = name;
        this.tt_time = time;
    }

    public TimeTableModel() {

    }

    public TimeTableModel(Parcel in) {
        tt_id = in.readString();
        tt_name = in.readString();
        tt_time = in.readString();
    }

    public static final Creator<TimeTableModel> CREATOR = new Creator<TimeTableModel>() {
        @Override
        public TimeTableModel createFromParcel(Parcel in) {
            return new TimeTableModel(in);
        }

        @Override
        public TimeTableModel[] newArray(int size) {
            return new TimeTableModel[size];
        }
    };

    public String getTt_id() {
        return tt_id;
    }

    public void setTt_id(String tt_id) {
        this.tt_id = tt_id;
    }


    public String getTt_name() {
        return tt_name;
    }

    public void setTt_name(String tt_name) {
        this.tt_name = tt_name;
    }

    public String getTt_time() {
        return tt_time;
    }

    public void setTt_time(String tt_time) {
        this.tt_time = tt_time;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(tt_id);
        dest.writeString(tt_name);
        dest.writeString(tt_time);
    }
}