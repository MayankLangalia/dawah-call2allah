package com.calltoallah.dawah;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

//import com.calltoallah.dawah.qrcode_reader.BarcodeReaderActivity;
//import com.calltoallah.dawah.qrcode_reader.BarcodeReaderFragment;
//import com.google.android.gms.vision.barcode.Barcode;

//implements BarcodeReaderFragment.BarcodeReaderListener
public class ScannerActivity extends AppCompatActivity  {
    private static final int BARCODE_READER_ACTIVITY_REQUEST = 1208;
    private TextView mTvResult;
    private TextView mTvResultHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
        Fragment fragmentById = supportFragmentManager.findFragmentById(R.id.fm_container);
        if (fragmentById != null) {
            fragmentTransaction.remove(fragmentById);
        }
        fragmentTransaction.commitAllowingStateLoss();
        launchBarCodeActivity();
    }

    private void launchBarCodeActivity() {
//        Intent launchIntent = BarcodeReaderActivity.getLaunchIntent(this, true, false);
//        startActivityForResult(launchIntent, BARCODE_READER_ACTIVITY_REQUEST);
    }

    @Override

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK) {
            Toast.makeText(this, "error in  scanning", Toast.LENGTH_SHORT).show();
            return;
        }

        if (requestCode == BARCODE_READER_ACTIVITY_REQUEST && data != null) {
//            Barcode barcode = data.getParcelableExtra(BarcodeReaderActivity.KEY_CAPTURED_BARCODE);
//            Toast.makeText(this, barcode.rawValue, Toast.LENGTH_SHORT).show();
//            mTvResultHeader.setText("On Activity Result");
//            mTvResult.setText(barcode.rawValue);
        }

    }

//    @Override
//    public void onScanned(Barcode barcode) {
//        Toast.makeText(this, barcode.rawValue, Toast.LENGTH_SHORT).show();
//        mTvResultHeader.setText("Barcode value from fragment");
//        mTvResult.setText(barcode.rawValue);
//    }
//
//    @Override
//    public void onScannedMultiple(List<Barcode> barcodes) {
//
//    }
//
//    @Override
//    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {
//
//    }

//    @Override
//    public void onScanError(String errorMessage) {
//
//    }
//
//    @Override
//    public void onCameraPermissionDenied() {
//        Toast.makeText(this, "Camera permission denied!", Toast.LENGTH_LONG).show();
//    }
}
