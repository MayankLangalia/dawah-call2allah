package com.calltoallah.dawah;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.model.MasjidModel;
import com.calltoallah.dawah.model.PostModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.Gson;

import java.io.Reader;
import java.io.StringReader;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoadActivity extends AppCompatActivity {

    private Integer masjidID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        if (appLinkIntent.getDataString() != null) {

            Log.d("app_link: ", "Result " + appLinkIntent.getDataString());
            if (appLinkIntent.getDataString().contains(getString(R.string.panel_url))) {

                if (appLinkIntent.getDataString().contains("post")) {
                    String postID = appLinkIntent.getDataString()
                            .substring(appLinkIntent.getDataString().lastIndexOf("/"))
                            .replace("/", "");
                    Log.d("app_link: ", "Found! postID " + postID);
                    getPostDetails(postID);

                } else {
                    String masjidID = appLinkIntent.getDataString()
                            .substring(appLinkIntent.getDataString().lastIndexOf("/"))
                            .replace("/", "");
                    Log.d("app_link: ", "Found! MasjidID " + masjidID);
                    getMasjidDetails(masjidID);
                }

            } else {
                Log.d("app_link: ", "Not Found! 0");
            }

        } else {
            Log.d("app_link: ", "Not Found! 1");
        }
    }

    private void getPostDetails(String postID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> loginCall = apiInterface.getPostDetails(postID);
        loginCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    Log.d("POST_DETAIL: ", response.body().toString());

                    try {
                        Gson gson = new Gson();
                        Reader reader = new StringReader(response.body().toString());
                        final PostModel postModel = gson.fromJson(reader, PostModel.class);

                        if (postModel != null) {
                            Intent intent = new Intent(LoadActivity.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                            intent.putExtra(Constants.DATA, postModel);
                            intent.putExtra(Constants.MODE, 14);
                            startActivity(intent);
                            finish();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    private void getMasjidDetails(String masjidID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> loginCall = apiInterface.getMasjidDetails(masjidID);
        loginCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    Log.d("MASJID_DETAIL: ", response.body().toString());

                    try {
                        Gson gson = new Gson();
                        Reader reader = new StringReader(response.body().toString());
                        final MasjidModel masjidModel = gson.fromJson(reader, MasjidModel.class);

                        if (masjidModel.getMasjidListModel().size() > 0) {
                            Intent intent = new Intent(LoadActivity.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                            intent.putExtra(Constants.DATA, masjidModel.getMasjidListModel().get(0));
                            intent.putExtra(Constants.MODE, 13);
                            startActivity(intent);
                            finish();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }
}
