package com.calltoallah.dawah.comman;

import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class ScreenFocusHelper {
    public View previousFocus;
    public List<View> focusableViews = new ArrayList<>();

    public void setEnableView(ViewGroup viewGroup, boolean isEnabled){
        //Find focusable elements
        findFocusableViews(viewGroup);

        for (View view : focusableViews) {
            view.setEnabled(isEnabled);
            view.setFocusable(isEnabled);
        }
    }

    private void findFocusableViews(ViewGroup viewGroup) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = viewGroup.getChildAt(i);
            if (view.isFocusable()) {
                if (!focusableViews.contains(view)) {
                    focusableViews.add(view);
                }
            }
            if (view instanceof ViewGroup) {
                findFocusableViews((ViewGroup) view);
            }
        }
    }
}
