package com.calltoallah.dawah.fragment;

import android.app.Activity;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.adapter.MasjidAdapter;
import com.calltoallah.dawah.comman.LocationTrack;
import com.calltoallah.dawah.comman.ProgressbarManager;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.MasjidListModel;
import com.calltoallah.dawah.model.MasjidModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.calltoallah.dawah.popup.PopupWindow;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class MasjidFragment extends BaseFragment {

    private boolean responseFailed = false;
    private ProgressbarManager progressbarManager;
    private RecyclerView rcvMasjidList;
    private RelativeLayout masjidContainer;
    public static SearchView mSearchMasjidView;
    private int REQUEST_VOICE = 13;
    private MasjidAdapter masjidAdapter;

    private double latitude, longitude;
    private ImageView btnFilter, btnAdd;
    private List<MasjidListModel> mFilterList = new ArrayList<>();
    private SwipeRefreshLayout swipeRefreshLayout;
    private LocationTrack mLocationTrack;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        return inflater.inflate(R.layout.fragment_masjid, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        hideKeyboard();
        Init(v);

        mLocationTrack = new LocationTrack(getActivity());
        if (mLocationTrack.canGetLocation()) {
            longitude = mLocationTrack.getLongitude();
            latitude = mLocationTrack.getLatitude();
            getMasjidList(latitude, longitude);
        } else {
            mLocationTrack.showSettingsAlert();
        }

        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getMasjidList(latitude, longitude);
            }
        });

        setData();
    }

    private void setData() {
        if (UserPreferences.loadUser(getActivity()).getVerify_status().equalsIgnoreCase("1")) {
            btnAdd.setVisibility(View.VISIBLE);
        } else {
            btnAdd.setVisibility(View.GONE);
        }

//        ImageView searchIcon = (ImageView) mSearchMasjidView.findViewById(androidx.appcompat.R.id.search_mag_icon);
//        searchIcon.setVisibility(View.GONE);
//
//        EditText txtSearch = ((EditText) mSearchMasjidView.findViewById(androidx.appcompat.R.id.search_src_text));
//        txtSearch.setHint(getResources().getString(R.string.search_hint));
//        txtSearch.setHintTextColor(Color.LTGRAY);
//        txtSearch.setTextColor(Color.WHITE);

        mSearchMasjidView.setQueryRefinementEnabled(true);
        mSearchMasjidView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (masjidAdapter != null) {
                    masjidAdapter.getFilter().filter(query);
                    hideKeyboard();
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (masjidAdapter != null) {
                    masjidAdapter.getFilter().filter(query);
                }
                return false;
            }
        });


        ImageView clearButton = (ImageView) mSearchMasjidView.findViewById(R.id.search_close_btn);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchMasjidView.setQuery("", false);
                masjidAdapter.getFilter().filter("");
                mSearchMasjidView.clearFocus();
            }
        });

        ImageView voiceButton = (ImageView) mSearchMasjidView.findViewById(R.id.search_voice_btn);
        voiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askSpeechInput();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupWindow testPopupWindow = new PopupWindow(getActivity(), new int[]{R.id.txt2, R.id.txt3});
                testPopupWindow.showAsDropDown(btnFilter);

                ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt1)).setVisibility(View.GONE);
                ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt4)).setVisibility(View.GONE);
                ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt2)).setText("Add Madrasah");
                ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt3)).setText("Add Masjid");

                testPopupWindow.setOnItemClickListener(new PopupWindow.OnItemClickListener() {
                    @Override
                    public void OnItemClick(View v) {
                        switch (v.getId()) {
                            case R.id.txt2:
                                activity.loadFragment(new AddMadrasahFragment());
                                break;

                            case R.id.txt3:
                                activity.loadFragment(new AddMasjidFragment());
                                break;
                        }
                    }
                });
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupWindow testPopupWindow = new PopupWindow(getActivity(), new int[]{R.id.txt1, R.id.txt2,
                        R.id.txt3, R.id.txt4, R.id.txt5});
                testPopupWindow.showAsDropDown(btnFilter);

                ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt1)).setText("All");
                ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt2)).setText("Masjid");
                ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt3)).setText("Madrasah");
                ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt4)).setText("Followed");
                ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt5)).setText("My List");
                ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt5)).setVisibility(View.VISIBLE);
                testPopupWindow.setOnItemClickListener(new PopupWindow.OnItemClickListener() {
                    @Override
                    public void OnItemClick(View v) {
                        switch (v.getId()) {
                            case R.id.txt1:
                                masjidAdapter = new MasjidAdapter(activity, mFilterList);
                                rcvMasjidList.setAdapter(masjidAdapter);
                                masjidAdapter.notifyDataSetChanged();
                                break;

                            case R.id.txt2:
                                List<MasjidListModel> mTemp = new ArrayList<>();
                                for (MasjidListModel masjidListModel : mFilterList) {
                                    if (masjidListModel.getType().equalsIgnoreCase("0"))
                                        mTemp.add(masjidListModel);
                                }

                                masjidAdapter = new MasjidAdapter(activity, mTemp);
                                rcvMasjidList.setAdapter(masjidAdapter);
                                masjidAdapter.notifyDataSetChanged();
                                break;

                            case R.id.txt3:
                                List<MasjidListModel> mTemp1 = new ArrayList<>();
                                for (MasjidListModel masjidListModel : mFilterList) {
                                    if (masjidListModel.getType().equalsIgnoreCase("1"))
                                        mTemp1.add(masjidListModel);
                                }

                                masjidAdapter = new MasjidAdapter(activity, mTemp1);
                                rcvMasjidList.setAdapter(masjidAdapter);
                                masjidAdapter.notifyDataSetChanged();
                                break;

                            case R.id.txt4:
                                List<MasjidListModel> mTemp2 = new ArrayList<>();
                                for (MasjidListModel masjidListModel : mFilterList) {
                                    if (masjidListModel.getFollow_status() == 1)
                                        mTemp2.add(masjidListModel);
                                }

                                masjidAdapter = new MasjidAdapter(activity, mTemp2);
                                rcvMasjidList.setAdapter(masjidAdapter);
                                masjidAdapter.notifyDataSetChanged();
                                break;

                            case R.id.txt5:
                                List<MasjidListModel> mTemp3 = new ArrayList<>();
                                for (MasjidListModel masjidListModel : mFilterList) {
                                    if (masjidListModel.getUserId().equalsIgnoreCase(UserPreferences.loadUser(getActivity()).getId()))
                                        mTemp3.add(masjidListModel);
                                }

                                masjidAdapter = new MasjidAdapter(activity, mTemp3);
                                rcvMasjidList.setAdapter(masjidAdapter);
                                masjidAdapter.notifyDataSetChanged();
                                break;
                        }
                    }
                });
            }
        });


    }

    private void getMasjidList(double lat, double lon) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> apiCall = apiInterface.getAllMasjidList(UserPreferences.loadUser(getActivity()).getId(),
                String.valueOf(lat), String.valueOf(lon));
        apiCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                swipeRefreshLayout.setRefreshing(false);

                if (response.isSuccessful()) {

                    try {
                        String res = response.body().string();
                        Log.d("MASJID: ", res);

                        try {
                            Gson gson = new Gson();
                            Reader reader = new StringReader(res);
                            final MasjidModel masjidModel = gson.fromJson(reader, MasjidModel.class);

                            if (masjidModel.getMasjidListModel().size() > 0) {

                                mFilterList = new ArrayList<>();
                                for (MasjidListModel masjidListModel : masjidModel.getMasjidListModel()) {
                                    if (masjidListModel.getStatus().equalsIgnoreCase("1") ||
                                            masjidListModel.getVerified_status().equalsIgnoreCase("1")) {
                                        mFilterList.add(masjidListModel);
                                    }
                                }

                                if (mFilterList.size() > 0) {
                                    masjidAdapter = new MasjidAdapter(activity, mFilterList);
                                    rcvMasjidList.setAdapter(masjidAdapter);
                                    masjidAdapter.notifyDataSetChanged();
                                }

                            } else {
                                Toast.makeText(activity, "Data not found!", Toast.LENGTH_SHORT).show();
                            }

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressbarManager.dismiss();
                Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Init(View v) {
        progressbarManager = new ProgressbarManager(getActivity());
        btnFilter = v.findViewById(R.id.btnFilter);
        btnAdd = v.findViewById(R.id.btnAdd);

        swipeRefreshLayout = v.findViewById(R.id.swipeRefresh);
        rcvMasjidList = v.findViewById(R.id.rcvMasjidList);
        rcvMasjidList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rcvMasjidList.setItemAnimator(new DefaultItemAnimator());

        SearchManager mSearchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mSearchMasjidView = (SearchView) v.findViewById(R.id.searchView);

        mSearchMasjidView.setSearchableInfo(mSearchManager.getSearchableInfo(getActivity().getComponentName()));
        mSearchMasjidView.setMaxWidth(Integer.MAX_VALUE);
        mSearchMasjidView.setIconified(false);
        mSearchMasjidView.clearFocus();
    }

    private void askSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Try saying something");
        try {
            startActivityForResult(intent, REQUEST_VOICE);
        } catch (ActivityNotFoundException a) {
            a.printStackTrace();
        }
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_VOICE && resultCode == RESULT_OK) {
            String query = data.getStringExtra(SearchManager.QUERY);
            mSearchMasjidView.setQuery(query, true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLocationTrack.stopListener();
    }
}

