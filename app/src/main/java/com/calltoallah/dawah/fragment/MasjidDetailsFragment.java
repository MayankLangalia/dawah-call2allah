package com.calltoallah.dawah.fragment;

import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.calltoallah.dawah.BuildConfig;
import com.calltoallah.dawah.DonationActivity;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.adapter.AvatarAdapter;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.comman.OverlapDecoration;
import com.calltoallah.dawah.comman.ProgressbarManager;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.firebase.SendNotification;
import com.calltoallah.dawah.model.FollowerModel;
import com.calltoallah.dawah.model.MasjidListModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.calltoallah.dawah.popup.PopupWindow;
import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import net.alhazmy13.hijridatepicker.date.hijri.HijriDatePickerDialog;

import org.json.JSONObject;

import java.io.File;
import java.io.Reader;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static com.calltoallah.dawah.network.APIInterface.API_DOMAIN;

public class MasjidDetailsFragment extends BaseFragment {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private MasjidListModel masjidListModel;
//    private int[] tabIcons = {
//            R.drawable.ic_action_admin,
//            R.drawable.ic_action_alarm,
//            R.drawable.ic_action_location
//    };

    private ImageView imgDMasjidIcon, imgDFollow, imgDQRCode;
    private TextView txtDMasjidName, txtDMasjidUname, txtDMasjidAddress, txtMasjidAbout, txtDMasjidFollower;
    private Button btnDonate;
    private FloatingActionButton writePost;

    private static final String IMAGE_DIRECTORY_NAME = "Dawah";
    private static final int REQUEST_PICK_VIDEO = 01;
    private static final int REQUEST_TAKE_PHOTO = 02;
    private static final int REQUEST_PICK_PHOTO = 13;
    private static final int CAMERA_PIC_REQUEST = 16;
    private Uri fileUri;
    private String mImageFileLocation = "", videoPath, imagePath;
    private ImageView previewMedia, imgMore;
    private ProgressbarManager progressbarManager;
    private boolean responseFailed = false;

    private FollowerModel followerModel;
    private int mode;
    private RecyclerView mRecyclerViewAvatar;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_masjid_details, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        if (getArguments() != null) {
            mode = getArguments().getInt(Constants.MODE);
            masjidListModel = getArguments().getParcelable(Constants.DATA);
        }

        Init(v);
        setData();
    }

    private void setData() {

        if (UserPreferences.loadUser(getActivity()).getVerify_status().equalsIgnoreCase("1") &&
                masjidListModel.getVerified_status().equalsIgnoreCase("1")) {
            btnDonate.setEnabled(true);
        } else {
            btnDonate.setEnabled(false);
        }

        if (UserPreferences.loadUser(getActivity()).getId().equalsIgnoreCase(masjidListModel.getUserId())) {
            imgMore.setVisibility(View.VISIBLE);
            writePost.setVisibility(View.VISIBLE);
        } else {
            imgMore.setVisibility(View.INVISIBLE);
            writePost.setVisibility(View.GONE);
        }

        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.no_profile_image)
                .error(R.drawable.no_profile_image);

        Glide.with(getActivity())
                .load(API_DOMAIN + masjidListModel.getImagePath())
                .apply(options)
                .into(imgDMasjidIcon);

        if (masjidListModel.getFollow_status() == 1) {
            imgDFollow.setImageResource(R.drawable.ic_action_follow);
        } else {
            imgDFollow.setImageResource(R.drawable.ic_action_unfollow);
        }

        if (masjidListModel.getVerified_status().equalsIgnoreCase("1")) {
            txtDMasjidName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_verified, 0);
            txtDMasjidName.setCompoundDrawablePadding(5);
        }

        txtDMasjidName.setText(masjidListModel.getMasjidName());
        txtDMasjidUname.setText(masjidListModel.getMasjidUsername());
        txtDMasjidAddress.setText(masjidListModel.getAddress());
        txtMasjidAbout.setText(masjidListModel.getAboutMasjid());

        getFollowers(masjidListModel.getId());
        imgMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupWindow testPopupWindow = new PopupWindow(getActivity(), new int[]{R.id.txt1, R.id.txt2,
                        R.id.txt3, R.id.txt4, R.id.txt5});
                testPopupWindow.showAsDropDown(v);

                ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt1)).setText("My Wallet");
                ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt2)).setText("Change Ownership");
                ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt4)).setText("My Documents");
                ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt5)).setText("Modify Info");
                ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt5)).setVisibility(View.VISIBLE);

                if (masjidListModel.getVerified_status().equalsIgnoreCase("1")) {
                    ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt3)).setText("Verified");
                    ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt3)).setEnabled(false);
                } else {
                    ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt3)).setText("Verification");
                    ((TextView) testPopupWindow.getContentView().findViewById(R.id.txt3)).setEnabled(true);
                }

                testPopupWindow.setOnItemClickListener(new PopupWindow.OnItemClickListener() {
                    @Override
                    public void OnItemClick(View v) {
                        switch (v.getId()) {
                            case R.id.txt1:
                                Bundle bundleWallet = new Bundle();
                                bundleWallet.putParcelable(Constants.DATA, masjidListModel);

                                WalletMasjidFragment walletMasjidFragment = new WalletMasjidFragment();
                                walletMasjidFragment.setArguments(bundleWallet);
                                activity.loadFragment(walletMasjidFragment);

                                break;

                            case R.id.txt2:
                                Bundle bundleOwner = new Bundle();
                                bundleOwner.putParcelable(Constants.OTHER, masjidListModel);
                                bundleOwner.putParcelableArrayList(Constants.DATA, (ArrayList<? extends Parcelable>) followerModel.getUserList());

                                MasjidOwnershipFragment masjidOwnershipFragment = new MasjidOwnershipFragment();
                                masjidOwnershipFragment.setArguments(bundleOwner);
                                activity.loadFragment(masjidOwnershipFragment);

                                break;

                            case R.id.txt3:
                                Bundle bundle = new Bundle();
                                bundle.putParcelable(Constants.DATA, masjidListModel);
                                bundle.putInt(Constants.MODE, mode);

                                MasjidVerificationFragment masjidVerificationFragment = new MasjidVerificationFragment();
                                masjidVerificationFragment.setArguments(bundle);
                                activity.loadFragment(masjidVerificationFragment);

                                break;

                            case R.id.txt4:
                                Bundle bundleDocs = new Bundle();
                                bundleDocs.putParcelable(Constants.DATA, masjidListModel);
                                bundleDocs.putInt(Constants.MODE, mode);

                                DocumentFragment documentFragment = new DocumentFragment();
                                documentFragment.setArguments(bundleDocs);
                                activity.loadFragment(documentFragment);

                                break;

                            case R.id.txt5:

                                if (masjidListModel.getType().equalsIgnoreCase("0")) {
                                    Bundle bundleModify = new Bundle();
                                    bundleModify.putParcelable(Constants.DATA, masjidListModel);

                                    AddMasjidFragment addMasjidFragment = new AddMasjidFragment();
                                    addMasjidFragment.setArguments(bundleModify);
                                    activity.loadFragment(addMasjidFragment);

                                } else {
                                    Bundle bundleModify = new Bundle();
                                    bundleModify.putParcelable(Constants.DATA, masjidListModel);

                                    AddMadrasahFragment addMadrasahFragment = new AddMadrasahFragment();
                                    addMadrasahFragment.setArguments(bundleModify);
                                    activity.loadFragment(addMadrasahFragment);
                                }

                                break;

                        }
                    }
                });
            }
        });

        imgDQRCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundleQR = new Bundle();
                bundleQR.putParcelable(Constants.DATA, masjidListModel);

                MasjidQRCodeFragment masjidQRCodeFragment = new MasjidQRCodeFragment();
                masjidQRCodeFragment.setArguments(bundleQR);
                activity.loadFragment(masjidQRCodeFragment);
            }
        });

        imgDFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                final Call<String> loginCall = apiInterface.addMasjidFollow(masjidListModel.getId(),
                        UserPreferences.loadUser(getActivity()).getId());
                loginCall.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {
                            imgDFollow.setImageResource(R.drawable.ic_action_follow);

                            SendNotification.getUserNotificationToken(masjidListModel.getUserId(),
                                    getActivity().getString(R.string.app_name),
                                    UserPreferences.loadUser(getActivity()).getFullName() + " Followed your Masjid (" + masjidListModel.getMasjidName() + ")", getActivity());

//                            activity.loadFragment(new HomeFragment());
//                            mBottomNavigationView.setSelectedItemId(R.id.nav_item_1);
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                    }
                });
            }
        });

        btnDonate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.MODE, 1);
                bundle.putParcelable(Constants.DATA, masjidListModel);

                Intent intent = new Intent(getActivity(), DonationActivity.class);
                intent.putExtras(bundle);
                activity.startActivity(intent);
            }
        });

        writePost.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                View parentView = getLayoutInflater().inflate(R.layout.dialog_posts, null);
                BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
                dialog.setContentView(parentView);

                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        BottomSheetDialog dialogc = (BottomSheetDialog) dialog;
                        FrameLayout bottomSheet = dialogc.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                        BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
                    }
                });

                dialog.show();

                previewMedia = (ImageView) dialog.findViewById(R.id.previewMedia);
                ImageView imgPMasjidLogo = (ImageView) dialog.findViewById(R.id.imgPMasjidLogo);
                TextView imgPMasjidName = (TextView) dialog.findViewById(R.id.imgPMasjidName);
                TextView imgPMasjidUsername = (TextView) dialog.findViewById(R.id.imgPMasjidUsername);
                LinearLayout donationContainer = (LinearLayout) dialog.findViewById(R.id.donationContainer);
                Button btnPOST = (Button) dialog.findViewById(R.id.btnPOST);
                EditText edtMessage = (EditText) dialog.findViewById(R.id.edtMessage);
                EditText editGoalAmt = (EditText) dialog.findViewById(R.id.editGoalAmt);

                if (UserPreferences.loadUser(getActivity()).getVerify_status().equalsIgnoreCase("1") &&
                        masjidListModel.getVerified_status().equalsIgnoreCase("1")) {
                    donationContainer.setVisibility(View.VISIBLE);
                } else {
                    donationContainer.setVisibility(View.GONE);
                }

//                if (UserPreferences.loadUser(getActivity()).getVerify_status().equalsIgnoreCase("1") &&
//                        UserPreferences.loadUser(getActivity()).getStatus().equalsIgnoreCase("1")) {
//                    donationContainer.setVisibility(View.VISIBLE);
//                } else if (UserPreferences.loadUser(getActivity()).getVerify_status().equalsIgnoreCase("0") &&
//                        UserPreferences.loadUser(getActivity()).getStatus().equalsIgnoreCase("1")) {
//                    donationContainer.setVisibility(View.GONE);
//                }

                imgPMasjidName.setText(masjidListModel.getMasjidName() + "");
                imgPMasjidUsername.setText(masjidListModel.getMasjidUsername() + "");

                RequestOptions options = new RequestOptions()
                        .placeholder(R.drawable.no_profile_image)
                        .error(R.drawable.no_profile_image);

                Glide.with(getActivity())
                        .load(API_DOMAIN + masjidListModel.getImagePath())
                        .apply(options)
                        .into(imgPMasjidLogo);

                btnPOST.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edtMessage.getText().toString().equalsIgnoreCase("")) {
                            Toast.makeText(getActivity(), "Message should not be blank!", Toast.LENGTH_SHORT).show();
                        } else {

                            if (imagePath != null || videoPath != null)
                                addPostImageVideo(edtMessage.getText().toString(), editGoalAmt.getText().toString(), dialog);
                            else
                                addPost("", edtMessage.getText().toString(), editGoalAmt.getText().toString(), dialog);
                        }

                    }
                });

                LinearLayout addEvent = (LinearLayout) dialog.findViewById(R.id.addEvent);
                addEvent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        View parentView = getLayoutInflater().inflate(R.layout.dialog_event, null);
                        BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
                        dialog.setContentView(parentView);

                        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                BottomSheetDialog dialogc = (BottomSheetDialog) dialog;
                                FrameLayout bottomSheet = dialogc.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
                            }
                        });

                        dialog.show();

                        EditText edtEventDate = (EditText) dialog.findViewById(R.id.edtEventDate);
                        EditText edtEventName = (EditText) dialog.findViewById(R.id.edtEventName);
                        ImageView pickDate = (ImageView) dialog.findViewById(R.id.pickDate);
                        Button btnAddEvent = (Button) dialog.findViewById(R.id.btnAddEvent);

                        btnAddEvent.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                                final Call<String> apiCall = apiInterface.addCalender(
                                        edtEventDate.getText().toString(), edtEventName.getText().toString(),
                                        UserPreferences.loadUser(getActivity()).getId());
                                apiCall.enqueue(new Callback<String>() {
                                    @Override
                                    public void onResponse(Call<String> call, Response<String> response) {
                                        if (response.isSuccessful()) {
                                            dialog.dismiss();
                                            Toast.makeText(getActivity(), "Event added successfully!", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<String> call, Throwable t) {
                                    }
                                });
                            }
                        });

                        pickDate.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                UmmalquraCalendar now = new UmmalquraCalendar();
                                HijriDatePickerDialog dpd = HijriDatePickerDialog.newInstance(
                                        new HijriDatePickerDialog.OnDateSetListener() {
                                            @Override
                                            public void onDateSet(HijriDatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                edtEventDate.setText(getMonth(monthOfYear) + " - " + dayOfMonth + ", " + year);
                                            }
                                        },
                                        now.get(UmmalquraCalendar.YEAR),
                                        now.get(UmmalquraCalendar.MONTH),
                                        now.get(UmmalquraCalendar.DAY_OF_MONTH));
                                dpd.show(getChildFragmentManager(), getString(R.string.app_name));
                                dpd.setVersion(HijriDatePickerDialog.Version.VERSION_2);
                                dpd.setAccentColor(getResources().getColor(R.color.colorPrimary));
                                dpd.setThemeDark(false);
                                dpd.setLocale(Locale.getDefault());
                            }
                        });
                    }
                });

                previewMedia.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle("Choose your photo/video");
                        String[] images = {"Take From Gallery", "Take Video From Gallery"};
                        builder.setItems(images, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                        startActivityForResult(galleryIntent, REQUEST_PICK_PHOTO);
                                        break;

                                    case 1:
                                        Intent pickVideoIntent = new Intent(Intent.ACTION_PICK,
                                                MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                                        startActivityForResult(pickVideoIntent, REQUEST_PICK_VIDEO);
                                        break;
                                }
                            }
                        });

                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                });
            }
        });
    }

    public void getFollowers(String masjid_id) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> apiCall = apiInterface.getFollowers(masjid_id, UserPreferences.loadUser(getActivity()).getId());
        apiCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    Log.d("FOLLOWER: ", response.body().toString());

                    Gson gson = new Gson();
                    Reader reader = new StringReader(response.body().toString());
                    followerModel = gson.fromJson(reader, FollowerModel.class);

                    if (followerModel.getUserList() != null) {
                        txtDMasjidFollower.setText(followerModel.getUserList().size() + "+  Followers");

                        AvatarAdapter avatarAdapter = new AvatarAdapter(activity, followerModel.getUserList());
                        mRecyclerViewAvatar.setAdapter(avatarAdapter);
                        avatarAdapter.notifyDataSetChanged();

                        avatarAdapter.setOnItemClickListener(new AvatarAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Bundle bundle = new Bundle();
                                bundle.putParcelable(Constants.OTHER, masjidListModel);
                                bundle.putParcelableArrayList(Constants.DATA, (ArrayList<? extends Parcelable>) followerModel.getUserList());

                                MasjidFollowersFragment masjidFollowersFragment = new MasjidFollowersFragment();
                                masjidFollowersFragment.setArguments(bundle);
                                activity.loadFragment(masjidFollowersFragment);
                            }
                        });

                    } else {
                        txtDMasjidFollower.setText("0  Followers");
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }


    private void addPost(String document_path, String message, String goalAmt, BottomSheetDialog dialog) {

        String donation = "0";
        if (goalAmt.equalsIgnoreCase("")) {
            donation = "0";
        } else {
            donation = "1";
        }

        String postType = "0";
        if (imagePath != null) {
            postType = "1";
        } else if (videoPath != null) {
            postType = "2";
        }

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentDate = df.format(c.getTime());

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<String> callAPI;

        callAPI = apiInterface.addMasjidPost(
                document_path, message, donation, postType,
                masjidListModel.getId(), currentDate, UserPreferences.loadUser(getActivity()).getId(), goalAmt);
        callAPI.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                progressbarManager.dismiss();
                dialog.dismiss();

                try {
                    String res = response.body().toString();
                    Log.d("RESPONSE: ", res);

                    try {

                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getInt("status") == 1) {
                            Toast.makeText(activity, "Post has been published", Toast.LENGTH_SHORT).show();
                            activity.startActivity(new Intent(getActivity(), MainActivity.class));

                        } else {
                            Toast.makeText(activity, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (IllegalStateException |
                            JsonSyntaxException exception) {
                        responseFailed = true;
                    }

                    if (responseFailed) {
                        progressbarManager.dismiss();
                        Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(activity, "" + t.getMessage().toString(), Toast.LENGTH_SHORT).show();
                progressbarManager.dismiss();
            }
        });
    }

    private void addPostImageVideo(String message, String goalAmt, BottomSheetDialog dialog) {

        MultipartBody.Part image_path = null;
        if (imagePath != null) {
            File file = new File(imagePath);
            RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            image_path = MultipartBody.Part.createFormData("file_name", file.getName(), mFile);
        }

        if (videoPath != null) {
            File file = new File(videoPath);
            RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            image_path = MultipartBody.Part.createFormData("file_name", file.getName(), mFile);
        }

        RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), UserPreferences.loadUser(getActivity()).getId());

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<String> callAPI;

        callAPI = apiInterface.addMasjidPostFile(
                user_id, image_path);
        callAPI.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                progressbarManager.dismiss();
                dialog.dismiss();

                try {
                    String res = response.body().toString();
                    Log.d("UPLOAD: ", res + "");

                    try {

                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getInt("status") == 1) {
                            addPost(jsonObject.getString("file_name"), message, goalAmt, dialog);
                        } else {
                            Toast.makeText(activity, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (IllegalStateException | JsonSyntaxException exception) {
                        responseFailed = true;
                    }

                    if (responseFailed) {
                        progressbarManager.dismiss();
                        Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(activity, "" + t.getMessage().toString(), Toast.LENGTH_SHORT).show();
                progressbarManager.dismiss();
            }
        });
    }

    private String getMonth(int event_month) {
        String islamicMonth = "";
        if (event_month == 0) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[0];
        } else if (event_month == 1) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[1];
        } else if (event_month == 2) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[2];
        } else if (event_month == 3) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[3];
        } else if (event_month == 4) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[4];
        } else if (event_month == 5) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[5];
        } else if (event_month == 6) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[6];
        } else if (event_month == 7) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[7];
        } else if (event_month == 8) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[8];
        } else if (event_month == 9) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[9];
        } else if (event_month == 10) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[10];
        } else if (event_month == 11) {
            islamicMonth = getResources().getStringArray(R.array.custom_months)[11];
        }
        return islamicMonth;
    }

    private void captureImage() {
        if (Build.VERSION.SDK_INT > 21) { //use this if Lollipop_Mr1 (API 22) or above
            Intent callCameraApplicationIntent = new Intent();
            callCameraApplicationIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

            // We give some instruction to the intent to save the image
            File photoFile = null;

            // If the createImageFile will be successful, the photo file will have the address of the file
            photoFile = createImageFile();
            // Here we call the function that will try to catch the exception made by the throw function
            // Here we add an extra file to the intent to put the address on to. For this purpose we use the FileProvider, declared in the AndroidManifest.
            Uri outputUri = FileProvider.getUriForFile(
                    getActivity(),
                    BuildConfig.APPLICATION_ID + ".provider",
                    photoFile);
            callCameraApplicationIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

            // The following is a new line with a trying attempt
            callCameraApplicationIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);

            Logger.getAnonymousLogger().info("Calling the camera App by intent");

            // The following strings calls the camera app and wait for his file in return.
            startActivityForResult(callCameraApplicationIntent, CAMERA_PIC_REQUEST);
        } else {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

            // start the image capture Intent
            startActivityForResult(intent, CAMERA_PIC_REQUEST);
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("Directory Error: ", "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    private File createImageFile() {
        Logger.getAnonymousLogger().info("Generating the image - method started");

        // Here we create a "non-collision file name", alternatively said, "an unique filename" using the "timeStamp" functionality
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmSS").format(new Date());
        String imageFileName = "IMAGE_" + timeStamp;
        // Here we specify the environment location and the exact path where we want to save the so-created file
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/photo_saving_app");
        Logger.getAnonymousLogger().info("Storage directory set");

        // Then we create the storage directory if does not exists
        if (!storageDirectory.exists()) storageDirectory.mkdir();

        // Here we create the file using a prefix, a suffix and a directory
        File image = new File(storageDirectory, imageFileName + ".jpg");
        // File image = File.createTempFile(imageFileName, ".jpg", storageDirectory);

        // Here the location is saved into the string mImageFileLocation
        Logger.getAnonymousLogger().info("File name and path set");

        mImageFileLocation = image.getAbsolutePath();
        // fileUri = Uri.parse(mImageFileLocation);
        // The file is returned to the previous intent across the camera application
        return image;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        System.gc();
        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_PICK_VIDEO) {
                if (data != null) {
                    Uri selectedVideo = data.getData();

                    videoPath = getPath(selectedVideo);
                    previewMedia.setImageBitmap(ThumbnailUtils.createVideoThumbnail(
                            videoPath, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND));
                }
            }

            if (requestCode == REQUEST_TAKE_PHOTO || requestCode == REQUEST_PICK_PHOTO) {
                if (data != null) {
                    // Get the Image from data
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    assert cursor != null;
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imagePath = cursor.getString(columnIndex);
                    previewMedia.setImageBitmap(BitmapFactory.decodeFile(imagePath));
                    cursor.close();
                }


            } else if (requestCode == CAMERA_PIC_REQUEST) {
                if (Build.VERSION.SDK_INT > 21) {
                    Glide.with(getActivity()).load(mImageFileLocation).into(previewMedia);
                    imagePath = mImageFileLocation;

                } else {
                    Glide.with(getActivity()).load(fileUri).into(previewMedia);
                    imagePath = fileUri.getPath();
                }
            }

        } else if (resultCode != RESULT_CANCELED) {
            activity.getSnackBar("Sorry, there was an error!");
        }
    }

    public String getSignRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    @Nullable
    public static String getPath(Context context, Uri uri) {
        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            } else if (isDownloadsDocument(uri)) {// DownloadsProvider
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);

            } else if (isMediaDocument(uri)) { // MediaProvider
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);

            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {// MediaStore (and general)
            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);

        } else if ("file".equalsIgnoreCase(uri.getScheme())) {// File
            return uri.getPath();
        }
        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    private void Init(View v) {
        progressbarManager = new ProgressbarManager(getActivity());
        imgDMasjidIcon = (ImageView) v.findViewById(R.id.imgDMasjidIcon);
        imgDFollow = (ImageView) v.findViewById(R.id.imgDFollow);
        imgDQRCode = (ImageView) v.findViewById(R.id.imgDQRCode);
        imgMore = (ImageView) v.findViewById(R.id.imgMore);

        txtDMasjidName = (TextView) v.findViewById(R.id.txtDMasjidName);
        txtDMasjidUname = (TextView) v.findViewById(R.id.txtDMasjidUname);
        txtDMasjidAddress = (TextView) v.findViewById(R.id.txtDMasjidAddress);
        txtMasjidAbout = (TextView) v.findViewById(R.id.txtMasjidAbout);
        txtDMasjidFollower = (TextView) v.findViewById(R.id.txtDMasjidFollower);

        writePost = (FloatingActionButton) v.findViewById(R.id.writePost);
        btnDonate = (Button) v.findViewById(R.id.btnDonate);

        viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) v.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        mRecyclerViewAvatar = v.findViewById(R.id.rcvAvatarList);
        mRecyclerViewAvatar.setLayoutManager(new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false));
        mRecyclerViewAvatar.addItemDecoration(new OverlapDecoration());
        mRecyclerViewAvatar.setHasFixedSize(true);


//        setupTabIcons();
//        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                int tabIconColor = ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark);
//                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//                int tabIconColor = ContextCompat.getColor(getActivity(), R.color.colorPrimaryLight);
//                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//            }
//        });
    }

//    private void setupTabIcons() {
//        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
//        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
//        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
//        int tabIconColorSelected = ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark);
//        tabLayout.getTabAt(0).getIcon().setColorFilter(tabIconColorSelected, PorterDuff.Mode.SRC_IN);
//        int tabIconColor = ContextCompat.getColor(getActivity(), R.color.colorPrimaryLight);
//        tabLayout.getTabAt(1).getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
//        tabLayout.getTabAt(2).getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
//    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(MyPostFragment.newInstance(masjidListModel), getString(R.string.post));

        if (masjidListModel.getType().equalsIgnoreCase("0")) {
            adapter.addFragment(TimeTableFragment.newInstance(masjidListModel), getString(R.string.timetable));
        }

        adapter.addFragment(LocationFragment.newInstance(masjidListModel), getString(R.string.location));
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}

