package com.calltoallah.dawah.fragment;

import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.calltoallah.dawah.MainActivity;

public abstract class BaseFragment extends Fragment {
    public MainActivity activity;

    @RequiresApi(api = Build.VERSION_CODES.M)
    protected void setActivity(MainActivity activity) {
        this.activity = activity;
    }

    public abstract void onBack();
}


