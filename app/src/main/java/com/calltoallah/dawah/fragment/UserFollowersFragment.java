package com.calltoallah.dawah.fragment;

import android.app.Activity;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SearchView;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.adapter.UserFollowerAdapter;

import java.util.Locale;

public class UserFollowersFragment extends BaseFragment {

    private RecyclerView rcvAdminList;
    private SearchView mSearchView;
    private int REQUEST_VOICE = 13;
    private UserFollowerAdapter followerAdapter;
    private CardView adminContainer;
    private LinearLayout errorLayout;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_follower, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Init(v);
        setData();
    }

    private void setData() {

        getUserFollowers();
        mSearchView.setQueryRefinementEnabled(true);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (followerAdapter != null) {
                    followerAdapter.getFilter().filter(query);
                    hideKeyboard();
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (followerAdapter != null) {
                    followerAdapter.getFilter().filter(query);
                }
                return false;
            }
        });

        ImageView clearButton = (ImageView) mSearchView.findViewById(R.id.search_close_btn);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchView.setQuery("", false);
                followerAdapter.getFilter().filter("");
                mSearchView.clearFocus();
            }
        });

        ImageView voiceButton = (ImageView) mSearchView.findViewById(R.id.search_voice_btn);
        voiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askSpeechInput();
            }
        });
    }

    public void getUserFollowers() {
        if (ProfileFragment.getInstance().mFollowerLst.size() > 0) {
            errorLayout.setVisibility(View.GONE);
            rcvAdminList.setVisibility(View.VISIBLE);

            followerAdapter = new UserFollowerAdapter(activity, ProfileFragment.getInstance().mFollowerLst, 0);
            rcvAdminList.setAdapter(followerAdapter);
            followerAdapter.notifyDataSetChanged();

        } else {
            errorLayout.setVisibility(View.VISIBLE);
            rcvAdminList.setVisibility(View.GONE);
        }
    }

    private void askSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Try saying something");
        try {
            startActivityForResult(intent, REQUEST_VOICE);
        } catch (ActivityNotFoundException a) {
            a.printStackTrace();
        }
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    private void Init(View v) {
        errorLayout = (LinearLayout) v.findViewById(R.id.errorLayout);
        SearchManager mSearchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mSearchView = (SearchView) v.findViewById(R.id.searchView);

        adminContainer = (CardView) v.findViewById(R.id.adminContainer);
        adminContainer.setVisibility(View.GONE);

        mSearchView.setSearchableInfo(mSearchManager.getSearchableInfo(getActivity().getComponentName()));
        mSearchView.setMaxWidth(Integer.MAX_VALUE);
        mSearchView.setIconified(false);
        mSearchView.clearFocus();

        rcvAdminList = (RecyclerView) v.findViewById(R.id.rcvAdminList);
        rcvAdminList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rcvAdminList.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}

