package com.calltoallah.dawah.fragment;

import android.app.Activity;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.adapter.HomeAdapter;
import com.calltoallah.dawah.comman.LocationTrack;
import com.calltoallah.dawah.comman.ProgressbarManager;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.HomeMasjidPostList;
import com.calltoallah.dawah.model.HomeModel;
import com.calltoallah.dawah.model.MasjidListModel;
import com.calltoallah.dawah.model.RecyclerViewItem;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class HomeFragment extends BaseFragment {

    public RecyclerView mRecyclerView;
    private Handler mHandler = new Handler();
    private boolean doubleBackToExitPressedOnce;
    private ProgressbarManager progressbarManager;

    private double latitude, longitude;
    public static SearchView mSearchPost;
    private int REQUEST_VOICE = 13;
    private HomeAdapter mHomeMediaAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private static HomeFragment instance = null;
    private LocationTrack mLocationTrack;
    private LinearLayout contentLayout, errorLayout;
    private boolean responseFailed = false;
    private Button btnFollow;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
    }

    public static HomeFragment getInstance() {
        return instance;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        hideKeyboard();
        Init(v);

        mLocationTrack = new LocationTrack(getActivity());
        if (mLocationTrack.canGetLocation()) {
            longitude = mLocationTrack.getLongitude();
            latitude = mLocationTrack.getLatitude();
            requestMedia(latitude, longitude);
        } else {
            mLocationTrack.showSettingsAlert();
        }

        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                requestMedia(latitude, longitude);
            }
        });

        btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.loadFragment(new MasjidFragment());
            }
        });
    }

    private void requestMedia(double lat, double lon) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> loginCall = apiInterface.getAllPosts(String.valueOf(lat), String.valueOf(lon),
                UserPreferences.loadUser(activity).getId());
        loginCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                swipeRefreshLayout.setRefreshing(false);

                if (response.isSuccessful()) {
                    Log.d("HOME: ", response.body().toString());

                    try {
                        Gson gson = new Gson();
                        Reader reader = new StringReader(response.body().toString());
                        HomeModel homeModel = gson.fromJson(reader, HomeModel.class);

                        if (homeModel.getHomeMasjidPostList().size() > 0) {
//                            contentLayout.setVisibility(View.VISIBLE);
//                            errorLayout.setVisibility(View.GONE);

                            List<RecyclerViewItem> recyclerViewItems = new ArrayList<>();
                            if (homeModel.getHomeNearestMasjid().size() > 0) {
                                for (MasjidListModel masjidListModel : homeModel.getHomeNearestMasjid()) {
                                    recyclerViewItems.add(masjidListModel);
                                }
                            }

                            if (homeModel.getHomeMasjidPostList().size() > 0) {
                                for (HomeMasjidPostList homeMasjidPostList : homeModel.getHomeMasjidPostList()) {
                                    recyclerViewItems.add(homeMasjidPostList);
                                }
                            }

                            mHomeMediaAdapter = new HomeAdapter(activity, recyclerViewItems, homeModel.getHomeMasjidPostList());
                            mRecyclerView.setAdapter(mHomeMediaAdapter);
                            mHomeMediaAdapter.notifyDataSetChanged();

                        } else {
                            List<RecyclerViewItem> recyclerViewItems = new ArrayList<>();
                            if (homeModel.getHomeNearestMasjid() != null) {
                                for (MasjidListModel masjidListModel : homeModel.getHomeNearestMasjid()) {
                                    if (masjidListModel.getType().equalsIgnoreCase("0"))
                                        recyclerViewItems.add(masjidListModel);
                                }
                            }

                            if (recyclerViewItems.size() > 0) {
//                                contentLayout.setVisibility(View.VISIBLE);
                                errorLayout.setVisibility(View.GONE);

                                mHomeMediaAdapter = new HomeAdapter(activity, recyclerViewItems, homeModel.getHomeMasjidPostList());
                                mRecyclerView.setAdapter(mHomeMediaAdapter);
                                mHomeMediaAdapter.notifyDataSetChanged();

                            } else {
//                                contentLayout.setVisibility(View.GONE);
                                errorLayout.setVisibility(View.VISIBLE);
                            }
                        }

                    } catch (IllegalStateException | JsonSyntaxException exception) {
                        responseFailed = true;
                    }

                    if (responseFailed) {
//                        contentLayout.setVisibility(View.GONE);
//                        errorLayout.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                progressbarManager.dismiss();
            }
        });

    }

    private void Init(View v) {
        progressbarManager = new ProgressbarManager(getActivity());

        contentLayout = (LinearLayout) v.findViewById(R.id.contentLayout);
        errorLayout = (LinearLayout) v.findViewById(R.id.errorLayout);
        btnFollow = (Button) v.findViewById(R.id.btnFollow);

        swipeRefreshLayout = v.findViewById(R.id.swipeRefresh);
        mRecyclerView = v.findViewById(R.id.exoPlayerRecyclerView);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        SearchManager mSearchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mSearchPost = (SearchView) v.findViewById(R.id.searchView);

        mSearchPost.setSearchableInfo(mSearchManager.getSearchableInfo(getActivity().getComponentName()));
        mSearchPost.setMaxWidth(Integer.MAX_VALUE);
        mSearchPost.setIconified(false);
        mSearchPost.clearFocus();

        mSearchPost.setQueryRefinementEnabled(true);
        mSearchPost.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (mHomeMediaAdapter != null) {
                    mHomeMediaAdapter.getFilter().filter(query);
                    hideKeyboard();
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (mHomeMediaAdapter != null) {
                    mHomeMediaAdapter.getFilter().filter(query);
                }
                return false;
            }
        });

        ImageView clearButton = (ImageView) mSearchPost.findViewById(R.id.search_close_btn);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchPost.setQuery("", false);
                mHomeMediaAdapter.getFilter().filter("");
                mSearchPost.clearFocus();
            }
        });

        ImageView voiceButton = (ImageView) mSearchPost.findViewById(R.id.search_voice_btn);
        voiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askSpeechInput();
            }
        });
    }

    private RequestManager initGlide() {
        RequestOptions options = new RequestOptions();
        return Glide.with(activity)
                .setDefaultRequestOptions(options);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLocationTrack.stopListener();
    }

    @Override
    public void onResume() {
        super.onResume();
        mHandler.postDelayed(mUpdateClockTask, 1000);
    }

    private Runnable mUpdateClockTask = new Runnable() {
        public void run() {
            mHandler.postDelayed(this, 1000);
        }
    };

    @Override
    public void onBack() {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                    Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS |
                    Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            getActivity().finish();
            System.exit(0);
        }

        this.doubleBackToExitPressedOnce = true;
        activity.getSnackBar(activity.getResources().getString(R.string.press_again));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void askSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Try saying something");
        try {
            startActivityForResult(intent, REQUEST_VOICE);
        } catch (ActivityNotFoundException a) {
            a.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_VOICE && resultCode == RESULT_OK) {
            String query = data.getStringExtra(SearchManager.QUERY);
            mSearchPost.setQuery(query, true);
        }
    }

}

