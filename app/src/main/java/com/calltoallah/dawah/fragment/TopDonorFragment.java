package com.calltoallah.dawah.fragment;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.adapter.TopDonorAdapter;
import com.calltoallah.dawah.comman.CircularImageView;
import com.calltoallah.dawah.model.DonorListModel;
import com.calltoallah.dawah.model.DonorModel;
import com.calltoallah.dawah.model.ProfileModel;
import com.calltoallah.dawah.model.ProfileUserInfo;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.calltoallah.dawah.network.APIInterface.API_DOMAIN;

public class TopDonorFragment extends BaseFragment {

    private RecyclerView mDonorList;
    private LinearLayout linearFirst, linearSecond, linearThird;
    private ImageView imgPhoto1, imgPhoto2, imgPhoto3;
    private TextView txtName1, txtName2, txtName3, txtAmount1, txtAmount2, txtAmount3;
    private boolean responseFailed = false;
    private LinearLayout linearDonor, errorLayout;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_donor, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Init(v);
        setData();
    }

    private void setData() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> apiCall = apiInterface.getDonor();
        apiCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (response.isSuccessful()) {
                    Log.d("TOP_DONOR: ", response.body().toString());

                    Gson gson = new Gson();
                    Reader reader = new StringReader(response.body().toString());
                    DonorModel donorModel = gson.fromJson(reader, DonorModel.class);

                    Collections.sort(donorModel.getTopDonar(), new BY_DONATION_AMT());
                    if (donorModel.getTopDonar().size() > 0) {
                        linearDonor.setVisibility(View.VISIBLE);
                        errorLayout.setVisibility(View.GONE);

                        if (donorModel.getTopDonar().size() == 1) {
                            linearFirst.setVisibility(View.VISIBLE);

                            txtName1.setText(donorModel.getTopDonar().get(0).getFullName());
                            txtAmount1.setText("₹ " + donorModel.getTopDonar().get(0).getDonationAmount() + "");

                            RequestOptions options = new RequestOptions()
                                    .placeholder(R.drawable.no_profile_image)
                                    .error(R.drawable.no_profile_image);

                            Glide.with(getActivity())
                                    .load(API_DOMAIN + donorModel.getTopDonar().get(0).getUser_avatar())
                                    .apply(options)
                                    .into(imgPhoto1);

                            imgPhoto1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    UserProfileRequest(donorModel.getTopDonar().get(0).getId());
                                }
                            });

                        } else if (donorModel.getTopDonar().size() == 2) {
                            linearFirst.setVisibility(View.VISIBLE);
                            linearSecond.setVisibility(View.VISIBLE);

                            txtName1.setText(donorModel.getTopDonar().get(0).getFullName());
                            txtAmount1.setText("₹ " + donorModel.getTopDonar().get(0).getDonationAmount() + "");

                            RequestOptions options = new RequestOptions()
                                    .placeholder(R.drawable.no_profile_image)
                                    .error(R.drawable.no_profile_image);

                            Glide.with(getActivity())
                                    .load(API_DOMAIN + donorModel.getTopDonar().get(0).getUser_avatar())
                                    .apply(options)
                                    .into(imgPhoto1);

                            txtName2.setText(donorModel.getTopDonar().get(1).getFullName());
                            txtAmount2.setText("₹ " + donorModel.getTopDonar().get(1).getDonationAmount() + "");

                            Glide.with(getActivity())
                                    .load(API_DOMAIN + donorModel.getTopDonar().get(1).getUser_avatar())
                                    .apply(options)
                                    .into(imgPhoto2);

                            imgPhoto1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    UserProfileRequest(donorModel.getTopDonar().get(0).getId());
                                }
                            });

                            imgPhoto2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    UserProfileRequest(donorModel.getTopDonar().get(1).getId());
                                }
                            });

                        } else if (donorModel.getTopDonar().size() == 3) {
                            linearFirst.setVisibility(View.VISIBLE);
                            linearSecond.setVisibility(View.VISIBLE);
                            linearThird.setVisibility(View.VISIBLE);

                            txtName1.setText(donorModel.getTopDonar().get(0).getFullName());
                            txtAmount1.setText("₹ " + donorModel.getTopDonar().get(0).getDonationAmount() + "");

                            RequestOptions options = new RequestOptions()
                                    .placeholder(R.drawable.no_profile_image)
                                    .error(R.drawable.no_profile_image);

                            Glide.with(getActivity())
                                    .load(API_DOMAIN + donorModel.getTopDonar().get(0).getUser_avatar())
                                    .apply(options)
                                    .into(imgPhoto1);

                            txtName2.setText(donorModel.getTopDonar().get(1).getFullName());
                            txtAmount2.setText("₹ " + donorModel.getTopDonar().get(1).getDonationAmount() + "");

                            Glide.with(getActivity())
                                    .load(API_DOMAIN + donorModel.getTopDonar().get(1).getUser_avatar())
                                    .apply(options)
                                    .into(imgPhoto2);

                            txtName3.setText(donorModel.getTopDonar().get(2).getFullName());
                            txtAmount3.setText("₹ " + donorModel.getTopDonar().get(2).getDonationAmount() + "");

                            Glide.with(getActivity())
                                    .load(API_DOMAIN + donorModel.getTopDonar().get(2).getUser_avatar())
                                    .apply(options)
                                    .into(imgPhoto3);

                            imgPhoto1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    UserProfileRequest(donorModel.getTopDonar().get(0).getId());
                                }
                            });

                            imgPhoto2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    UserProfileRequest(donorModel.getTopDonar().get(1).getId());
                                }
                            });

                            imgPhoto3.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    UserProfileRequest(donorModel.getTopDonar().get(2).getId());
                                }
                            });

                        } else {
                            linearFirst.setVisibility(View.VISIBLE);
                            linearSecond.setVisibility(View.VISIBLE);
                            linearThird.setVisibility(View.VISIBLE);

                            txtName1.setText(donorModel.getTopDonar().get(0).getFullName());
                            txtAmount1.setText("₹ " + donorModel.getTopDonar().get(0).getDonationAmount() + "");

                            RequestOptions options = new RequestOptions()
                                    .placeholder(R.drawable.no_profile_image)
                                    .error(R.drawable.no_profile_image);

                            Glide.with(getActivity())
                                    .load(API_DOMAIN + donorModel.getTopDonar().get(0).getUser_avatar())
                                    .apply(options)
                                    .into(imgPhoto1);

                            txtName2.setText(donorModel.getTopDonar().get(1).getFullName());
                            txtAmount2.setText("₹ " + donorModel.getTopDonar().get(1).getDonationAmount() + "");

                            Glide.with(getActivity())
                                    .load(API_DOMAIN + donorModel.getTopDonar().get(1).getUser_avatar())
                                    .apply(options)
                                    .into(imgPhoto2);

                            txtName3.setText(donorModel.getTopDonar().get(2).getFullName());
                            txtAmount3.setText("₹ " + donorModel.getTopDonar().get(2).getDonationAmount() + "");

                            Glide.with(getActivity())
                                    .load(API_DOMAIN + donorModel.getTopDonar().get(2).getUser_avatar())
                                    .apply(options)
                                    .into(imgPhoto3);

                            List<DonorListModel> mFilterList = new ArrayList<>();
                            for (int i = 3; i < donorModel.getTopDonar().size(); i++) {
                                DonorListModel donorListModel = donorModel.getTopDonar().get(i);
                                mFilterList.add(donorListModel);
                            }

                            TopDonorAdapter topDonorAdapter = new TopDonorAdapter(activity, mFilterList);
                            mDonorList.setAdapter(topDonorAdapter);

                            imgPhoto1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    UserProfileRequest(donorModel.getTopDonar().get(0).getId());
                                }
                            });

                            imgPhoto2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    UserProfileRequest(donorModel.getTopDonar().get(1).getId());
                                }
                            });

                            imgPhoto3.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    UserProfileRequest(donorModel.getTopDonar().get(2).getId());
                                }
                            });
                        }
                    } else {
                        linearDonor.setVisibility(View.GONE);
                        errorLayout.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                mDonorList.setVisibility(View.GONE);
                errorLayout.setVisibility(View.VISIBLE);
            }
        });

    }

    public class BY_DONATION_AMT implements Comparator<DonorListModel> {

        @Override
        public int compare(DonorListModel lhs, DonorListModel rhs) {
            Double distance = Double.valueOf(lhs.getDonationAmount());
            Double distance1 = Double.valueOf(rhs.getDonationAmount());
            if (distance.compareTo(distance1) < 0) {
                return 1;
            } else if (distance.compareTo(distance1) > 0) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    private void UserProfileRequest(String userID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> apiCall = apiInterface.getProfileInfo(userID);
        apiCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {

                    try {
                        String res = response.body().string();
                        Log.d("Profile: ", res);

                        try {
                            Gson gson = new Gson();
                            Reader reader = new StringReader(res);
                            ProfileModel profileModel = gson.fromJson(reader, ProfileModel.class);

                            if (profileModel.getProfileUserInfo().size() > 0) {
                                ProfileUserInfo profileUserInfo = profileModel.getProfileUserInfo().get(0);

                                final Dialog dialog = new Dialog(getActivity(), R.style.DialogTheme);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.getWindow().setBackgroundDrawableResource(R.color.colorBlackCC);
                                dialog.setContentView(R.layout.dialog_profile);
                                dialog.show();

                                CircularImageView profilePhoto = (CircularImageView) dialog.findViewById(R.id.profilePhoto);
                                TextView profileFullname = (TextView) dialog.findViewById(R.id.profileFullname);
                                TextView profileUsername = (TextView) dialog.findViewById(R.id.profileUsername);
                                TextView txtTotalFollowers = (TextView) dialog.findViewById(R.id.txtTotalFollowers);
                                TextView txtTotalFollowings = (TextView) dialog.findViewById(R.id.txtTotalFollowings);
                                TextView txtTotalDonated = (TextView) dialog.findViewById(R.id.txtTotalDonated);
                                TextView txtVerifiedMobile = (TextView) dialog.findViewById(R.id.txtVerifiedMobile);
                                TextView txtVerifiedEmail = (TextView) dialog.findViewById(R.id.txtVerifiedEmail);

                                if (profileUserInfo.getPhoneNumber().length() == 10) {
                                    txtVerifiedMobile.setText("XXXXXX" + profileUserInfo.getPhoneNumber().substring(profileUserInfo.getPhoneNumber().length() - 4));
                                }

                                txtVerifiedEmail.setText(profileUserInfo.getEmail());
                                profileFullname.setText(profileUserInfo.getFullName());

                                if (!profileUserInfo.getUser_avatar().equalsIgnoreCase("")) {
                                    Glide.with(getActivity()).load(APIInterface.API_DOMAIN + profileUserInfo.getUser_avatar()).into(profilePhoto);
                                }

                                if (profileUserInfo.getVerify_status().equalsIgnoreCase("1")) {
                                    profileFullname.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_verified, 0);
                                    profileFullname.setCompoundDrawablePadding(5);
                                }

                                if (profileUserInfo.getFollower_count() == null)
                                    txtTotalFollowers.setText("₹ " + 0 + "\n" + "Followers");
                                else
                                    txtTotalFollowers.setText("₹ " + profileUserInfo.getFollower_count() + "\n" + "Followers");

                                if (profileUserInfo.getFollowing_count() == null)
                                    txtTotalFollowings.setText("₹ " + 0 + "\n" + "Followings");
                                else
                                    txtTotalFollowings.setText("₹ " + profileUserInfo.getFollowing_count() + "\n" + "Followings");

                                if (profileUserInfo.getUser_donated() == null)
                                    txtTotalDonated.setText(0 + "\n" + "Donated");
                                else
                                    txtTotalDonated.setText(profileUserInfo.getUser_donated() + "\n" + "Donated");

                                if (profileUserInfo.getUsername().equalsIgnoreCase(""))
                                    profileUsername.setText("not found!");
                                else
                                    profileUsername.setText(profileUserInfo.getUsername());
                            }

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            Toast.makeText(getActivity(), getActivity().getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getActivity(), getActivity().getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Init(View v) {
        linearDonor = (LinearLayout) v.findViewById(R.id.linearDonor);
        errorLayout = (LinearLayout) v.findViewById(R.id.errorLayout);

        imgPhoto1 = (ImageView) v.findViewById(R.id.imgPhoto1);
        imgPhoto2 = (ImageView) v.findViewById(R.id.imgPhoto2);
        imgPhoto3 = (ImageView) v.findViewById(R.id.imgPhoto3);

        txtName1 = (TextView) v.findViewById(R.id.txtName1);
        txtName2 = (TextView) v.findViewById(R.id.txtName2);
        txtName3 = (TextView) v.findViewById(R.id.txtName3);

        txtAmount1 = (TextView) v.findViewById(R.id.txtAmount1);
        txtAmount2 = (TextView) v.findViewById(R.id.txtAmount2);
        txtAmount3 = (TextView) v.findViewById(R.id.txtAmount3);

        linearFirst = (LinearLayout) v.findViewById(R.id.linearFirst);
        linearSecond = (LinearLayout) v.findViewById(R.id.linearSecond);
        linearThird = (LinearLayout) v.findViewById(R.id.linearThird);

        mDonorList = (RecyclerView) v.findViewById(R.id.rcvDonor);
        mDonorList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mDonorList.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}

