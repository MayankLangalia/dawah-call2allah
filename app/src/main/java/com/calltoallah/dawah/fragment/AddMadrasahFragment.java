package com.calltoallah.dawah.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.calltoallah.dawah.BuildConfig;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.MapActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.comman.ProgressbarManager;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.location.SimplePlacePicker;
import com.calltoallah.dawah.model.MasjidListModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Logger;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static com.calltoallah.dawah.MainActivity.mToolbarTitle;
import static com.calltoallah.dawah.network.APIInterface.API_DOMAIN;

public class AddMadrasahFragment extends BaseFragment {

    private EditText edtMadrasahName, edtAddress, edtContactNumber,
            edtMadrasahIcon, edtDescription, edtUsername, edtStudents, edtTeachers;
    private Button btnAddMadrasah;
    private ProgressbarManager progressbarManager;
    private int PLACE_PICKER_REQUEST = 1;
    private String imagePath;
    private double madrasahLat = 0, madrasahLon = 0;

    private static final String IMAGE_DIRECTORY_NAME = "Dawah";
    private static final int REQUEST_TAKE_PHOTO = 02;
    private static final int REQUEST_PICK_PHOTO = 13;
    private static final int CAMERA_PIC_REQUEST = 16;
    private Uri fileUri;
    private String mImageFileLocation = "";
    private boolean responseFailed = false;

    private Bundle bundle;
    private MasjidListModel masjidListModel;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_add_madrasah, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Init(v);
        setData();
    }

    private void setData() {

        bundle = getArguments();
        if (bundle != null) {
            mToolbarTitle.setText("Update Madrasah");
            masjidListModel = bundle.getParcelable(Constants.DATA);

            edtMadrasahName.setText(masjidListModel.getMasjidName());
            edtAddress.setText(masjidListModel.getAddress());
            edtContactNumber.setText(masjidListModel.getPhoneNumber());
            edtDescription.setText(masjidListModel.getAboutMasjid());
            edtUsername.setText(masjidListModel.getMasjidUsername());
            edtStudents.setText(masjidListModel.getTotalStudents());
            edtTeachers.setText(masjidListModel.getTotalTeachers());

            madrasahLat = Double.parseDouble(masjidListModel.getMasjidLat());
            madrasahLon = Double.parseDouble(masjidListModel.getMasjidLng());
            edtMadrasahIcon.setText(API_DOMAIN + masjidListModel.getImagePath());

        } else{
            mToolbarTitle.setText("Add Madrasah");
        }

        edtMadrasahIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Choose your photo");
                String[] images = {"Take From Gallery", "Take From Camera"};
                builder.setItems(images, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(galleryIntent, REQUEST_PICK_PHOTO);
                                break;

                            case 1:
                                captureImage();
                                break;
                        }
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        edtAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                PingPlacePicker.IntentBuilder builder = new PingPlacePicker.IntentBuilder();
//                builder.setAndroidApiKey(getString(R.string.google_brows_api_key))
//                        .setMapsApiKey(getString(R.string.google_maps_key));
//                try {
//                    Intent placeIntent = builder.build(getActivity());
//                    startActivityForResult(placeIntent, PLACE_PICKER_REQUEST);
//                } catch (Exception ex) {
//                    // Google Play services is not available...
//                }

                String apiKey = getString(R.string.google_maps_key);
                String mCountry = "ind";
                String mLanguage = "en";
                String[] mSupportedAreas = "".split(",");
                startMapActivity(apiKey, mCountry, mLanguage, mSupportedAreas);
            }
        });

        btnAddMadrasah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtMadrasahName.getText().toString().equalsIgnoreCase("") ||
                        edtAddress.getText().toString().equalsIgnoreCase("") ||
                        edtContactNumber.getText().toString().equalsIgnoreCase("") ||
                        edtMadrasahIcon.getText().toString().equalsIgnoreCase("") ||
                        edtDescription.getText().toString().equalsIgnoreCase("") ||
                        edtUsername.getText().toString().equalsIgnoreCase("") ||
                        edtStudents.getText().toString().equalsIgnoreCase("") ||
                        edtTeachers.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(activity, "All Fields Required!", Toast.LENGTH_SHORT).show();
                } else {
                    progressbarManager.show();

                    if (masjidListModel != null) {
                        updateMadrasah();
                    } else {
                        addMadrasah();
                    }
                }
            }
        });
    }

    private void updateMadrasah() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<String> callAPI;

        RequestBody masjid_id = RequestBody.create(MediaType.parse("multipart/form-data"), masjidListModel.getId());
        RequestBody masjid_username = RequestBody.create(MediaType.parse("multipart/form-data"), edtUsername.getText().toString());
        RequestBody masjid_name = RequestBody.create(MediaType.parse("multipart/form-data"), edtMadrasahName.getText().toString());
        RequestBody phone_number = RequestBody.create(MediaType.parse("multipart/form-data"), edtContactNumber.getText().toString());
        RequestBody address = RequestBody.create(MediaType.parse("multipart/form-data"), edtAddress.getText().toString());
        RequestBody about_masjid = RequestBody.create(MediaType.parse("multipart/form-data"), edtDescription.getText().toString());

        RequestBody masjid_latitude = RequestBody.create(MediaType.parse("multipart/form-data"), madrasahLat + "");
        RequestBody masjid_longitude = RequestBody.create(MediaType.parse("multipart/form-data"), madrasahLon + "");
        RequestBody total_students = RequestBody.create(MediaType.parse("multipart/form-data"), edtStudents.getText().toString());
        RequestBody total_teacher = RequestBody.create(MediaType.parse("multipart/form-data"), edtTeachers.getText().toString());

        File file = new File(imagePath);
        RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part file_name = MultipartBody.Part.createFormData("file_name", file.getName(), mFile);

        callAPI = apiInterface.updateMasjidMadrasah(masjid_id,
                masjid_name, masjid_username,  address, phone_number,
                about_masjid, masjid_latitude, masjid_longitude, null, null,
                total_students, total_teacher, file_name);
        callAPI.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                progressbarManager.dismiss();

                try {
                    String res = response.body().toString();
                    Log.d("UPDATE_MADRASAH: ", res);

                    try {

                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getInt("status") == 1) {
                            showDialog();
                        } else {
                            Toast.makeText(activity, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (IllegalStateException |
                            JsonSyntaxException exception) {
                        responseFailed = true;
                    }

                    if (responseFailed) {
                        progressbarManager.dismiss();
                        Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(activity, "" + t.getMessage().toString(), Toast.LENGTH_SHORT).show();
                progressbarManager.dismiss();
            }
        });
    }

    private void addMadrasah() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentDate = df.format(c.getTime());

        RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), UserPreferences.loadUser(getActivity()).getId());
        RequestBody masjid_username = RequestBody.create(MediaType.parse("multipart/form-data"), edtUsername.getText().toString());
        RequestBody masjid_name = RequestBody.create(MediaType.parse("multipart/form-data"), edtMadrasahName.getText().toString());
        RequestBody phone_number = RequestBody.create(MediaType.parse("multipart/form-data"), edtContactNumber.getText().toString());
        RequestBody address = RequestBody.create(MediaType.parse("multipart/form-data"), edtAddress.getText().toString());
        RequestBody created_on = RequestBody.create(MediaType.parse("multipart/form-data"), currentDate);
        RequestBody about_masjid = RequestBody.create(MediaType.parse("multipart/form-data"), edtDescription.getText().toString());
        RequestBody masjid_latitude = RequestBody.create(MediaType.parse("multipart/form-data"), madrasahLat + "");
        RequestBody masjid_longitude = RequestBody.create(MediaType.parse("multipart/form-data"), madrasahLon + "");
        RequestBody namaz_time = RequestBody.create(MediaType.parse("multipart/form-data"), "");
        RequestBody namaz_name = RequestBody.create(MediaType.parse("multipart/form-data"), "");
        RequestBody type = RequestBody.create(MediaType.parse("multipart/form-data"), "1");
        RequestBody total_students = RequestBody.create(MediaType.parse("multipart/form-data"), edtStudents.getText().toString());
        RequestBody total_teacher = RequestBody.create(MediaType.parse("multipart/form-data"), edtTeachers.getText().toString());

        File file = new File(imagePath);
        RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part image_path = MultipartBody.Part.createFormData("image_path", file.getName(), mFile);

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<String> callAPI;

        callAPI = apiInterface.addMasjidMadrasah(
                user_id, masjid_username, masjid_name, phone_number, address,
                created_on, about_masjid, masjid_latitude, masjid_longitude,
                namaz_time, namaz_name, type, image_path, total_students, total_teacher);
        callAPI.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                progressbarManager.dismiss();

                try {
                    String res = response.body().toString();
                    Log.d("RESPONSE: ", res);

                    try {

                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getInt("status") == 1) {
                            showDialog();
                        } else {
                            Toast.makeText(activity, "" + jsonObject.getString("file_name"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (IllegalStateException |
                            JsonSyntaxException exception) {
                        responseFailed = true;
                    }

                    if (responseFailed) {
                        progressbarManager.dismiss();
                        Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(activity, "" + t.getMessage().toString(), Toast.LENGTH_SHORT).show();
                progressbarManager.dismiss();
            }
        });
    }

    private void showDialog() {
        final Dialog dialog = new Dialog(getActivity(), R.style.DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_success);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorBlackCC)));
        dialog.show();

        TextView txtUsername = dialog.findViewById(R.id.txtUsername);
        Button btnMail = dialog.findViewById(R.id.btnMail);
        Button btnOK = dialog.findViewById(R.id.btnOK);
        ImageView imgClose = dialog.findViewById(R.id.imgClose);

        txtUsername.setText("Hey " + UserPreferences.loadUser(getActivity()).getFullName());
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                activity.startActivity(new Intent(getActivity(), MainActivity.class));
            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                activity.startActivity(new Intent(getActivity(), MainActivity.class));
            }
        });

        btnMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String masjidInformation =
                        "Madrasah Username: " + edtUsername.getText().toString() + "\n" +
                                "Madrasah Name: " + edtMadrasahName.getText().toString() + "\n" +
                                "Madrasah Phone: " + edtContactNumber.getText().toString() + "\n" +
                                "Madrasah Address: " + edtAddress.getText().toString() + "\n" +
                                "Madrasah Description: " + edtDescription.getText().toString() + "\n" +
                                "Madrasah Latitude: " + madrasahLat + "\n" +
                                "Madrasah Longitude: " + madrasahLon + "\n" +
                                "Madrasah Total Teachers: " + edtTeachers.getText().toString() + "\n" +
                                "Madrasah Total Students: " + edtStudents.getText().toString();

                Intent intent = new Intent(Intent.ACTION_SEND);
                String[] recipients = {"dawah@calltoallah.com"};
                intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                intent.putExtra(Intent.EXTRA_SUBJECT, "Customer Support");
                intent.putExtra(Intent.EXTRA_TEXT, masjidInformation);
                intent.setType("text/html");
                intent.setPackage("com.google.android.gm");
                startActivity(Intent.createChooser(intent, "Send mail"));

                dialog.dismiss();
                activity.startActivity(new Intent(getActivity(), MainActivity.class));
            }
        });
    }

    private void Init(View v) {
        progressbarManager = new ProgressbarManager(getActivity());

        edtMadrasahName = (EditText) v.findViewById(R.id.edtMadrasahName);
        edtAddress = (EditText) v.findViewById(R.id.edtAddress);
        edtContactNumber = (EditText) v.findViewById(R.id.edtContactNumber);
        edtMadrasahIcon = (EditText) v.findViewById(R.id.edtMadrasahIcon);
        edtDescription = (EditText) v.findViewById(R.id.edtDescription);
        edtUsername = (EditText) v.findViewById(R.id.edtUsername);
        edtStudents = (EditText) v.findViewById(R.id.edtStudents);
        edtTeachers = (EditText) v.findViewById(R.id.edtTeachers);

        btnAddMadrasah = (Button) v.findViewById(R.id.btnAddMadrasah);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.gc();

        if (requestCode == SimplePlacePicker.SELECT_LOCATION_REQUEST_CODE && resultCode == RESULT_OK) {
            if (data != null) {
                madrasahLat = data.getDoubleExtra(SimplePlacePicker.LOCATION_LAT_EXTRA, -1);
                madrasahLon = data.getDoubleExtra(SimplePlacePicker.LOCATION_LNG_EXTRA, -1);
                edtAddress.setText(data.getStringExtra(SimplePlacePicker.SELECTED_ADDRESS));
            }
        }

//        if (requestCode == PLACE_PICKER_REQUEST && resultCode == RESULT_OK) {
//            Place place = PingPlacePicker.getPlace(data);
//            if (place != null) {
//                madrasahLat = place.getLatLng().latitude;
//                madrasahLon = place.getLatLng().longitude;
//
//                edtAddress.setText(place.getAddress());
//            }
//        }

        if (requestCode == REQUEST_TAKE_PHOTO || requestCode == REQUEST_PICK_PHOTO && resultCode == RESULT_OK) {
            if (data != null) {
                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imagePath = cursor.getString(columnIndex);
                edtMadrasahIcon.setText(imagePath);
//                previewBanner.setImageBitmap(BitmapFactory.decodeFile(imagePath));
                cursor.close();
            }

        } else if (requestCode == CAMERA_PIC_REQUEST && resultCode == RESULT_OK) {
            if (Build.VERSION.SDK_INT > 21) {
//                Glide.with(getActivity()).load(mImageFileLocation).into(previewBanner);
                imagePath = mImageFileLocation;
                edtMadrasahIcon.setText(imagePath);

            } else {
//                Glide.with(getActivity()).load(fileUri).into(previewBanner);
                imagePath = fileUri.getPath();
                edtMadrasahIcon.setText(imagePath);
            }
        }
    }

    private void captureImage() {
        if (Build.VERSION.SDK_INT > 21) { //use this if Lollipop_Mr1 (API 22) or above
            Intent callCameraApplicationIntent = new Intent();
            callCameraApplicationIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

            // We give some instruction to the intent to save the image
            File photoFile = null;

            // If the createImageFile will be successful, the photo file will have the address of the file
            photoFile = createImageFile();
            // Here we call the function that will try to catch the exception made by the throw function
            // Here we add an extra file to the intent to put the address on to. For this purpose we use the FileProvider, declared in the AndroidManifest.
            Uri outputUri = FileProvider.getUriForFile(
                    getActivity(),
                    BuildConfig.APPLICATION_ID + ".provider",
                    photoFile);
            callCameraApplicationIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

            // The following is a new line with a trying attempt
            callCameraApplicationIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);

            Logger.getAnonymousLogger().info("Calling the camera App by intent");

            // The following strings calls the camera app and wait for his file in return.
            startActivityForResult(callCameraApplicationIntent, CAMERA_PIC_REQUEST);
        } else {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

            // start the image capture Intent
            startActivityForResult(intent, CAMERA_PIC_REQUEST);
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("Directory Error: ", "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    private File createImageFile() {
        Logger.getAnonymousLogger().info("Generating the image - method started");

        // Here we create a "non-collision file name", alternatively said, "an unique filename" using the "timeStamp" functionality
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmSS").format(new Date());
        String imageFileName = "IMAGE_" + timeStamp;
        // Here we specify the environment location and the exact path where we want to save the so-created file
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/photo_saving_app");
        Logger.getAnonymousLogger().info("Storage directory set");

        // Then we create the storage directory if does not exists
        if (!storageDirectory.exists()) storageDirectory.mkdir();

        // Here we create the file using a prefix, a suffix and a directory
        File image = new File(storageDirectory, imageFileName + ".jpg");
        // File image = File.createTempFile(imageFileName, ".jpg", storageDirectory);

        // Here the location is saved into the string mImageFileLocation
        Logger.getAnonymousLogger().info("File name and path set");

        mImageFileLocation = image.getAbsolutePath();
        // fileUri = Uri.parse(mImageFileLocation);
        // The file is returned to the previous intent across the camera application
        return image;
    }

    private void startMapActivity(String apiKey, String country, String language, String[] supportedAreas) {
        Intent intent = new Intent(getActivity(), MapActivity.class);
        Bundle bundle = new Bundle();

        bundle.putString(SimplePlacePicker.API_KEY, apiKey);
        bundle.putString(SimplePlacePicker.COUNTRY, country);
        bundle.putString(SimplePlacePicker.LANGUAGE, language);
        bundle.putStringArray(SimplePlacePicker.SUPPORTED_AREAS, supportedAreas);

        intent.putExtras(bundle);
        startActivityForResult(intent, SimplePlacePicker.SELECT_LOCATION_REQUEST_CODE);
    }
}

