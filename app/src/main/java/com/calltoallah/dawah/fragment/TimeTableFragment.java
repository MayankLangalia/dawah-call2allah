package com.calltoallah.dawah.fragment;

import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.MasjidListModel;
import com.calltoallah.dawah.model.TimeTableModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TimeTableFragment extends BaseFragment {

    private ListView mTimeTable;
    public static List<TimeTableModel> timeTableModelList = new ArrayList<>();
    private MasjidListModel masjidListModel;
    public static Button btnEdit, btnDone;
    public boolean flag = false;
    private LinearLayout linearUpdateTT;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_timetable, container, false);
    }

    public static TimeTableFragment newInstance(MasjidListModel masjidListModel) {
        TimeTableFragment aFragment = new TimeTableFragment();
        Bundle aBundle = new Bundle();
        aBundle.putParcelable(Constants.DATA, masjidListModel);
        aFragment.setArguments(aBundle);
        return aFragment;
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        Init(v);

        if (getArguments() != null) {
            masjidListModel = getArguments().getParcelable(Constants.DATA);
        }

        if (UserPreferences.loadUser(getActivity()).getId().contains(masjidListModel.getUserId())) {
            linearUpdateTT.setVisibility(View.VISIBLE);
        } else {
            linearUpdateTT.setVisibility(View.GONE);
        }

        if (masjidListModel.getPrayerTime() != null) {
            List<String> prayerTime = Arrays.asList(masjidListModel.getPrayerTime().split(","));
            List<String> prayerName = Arrays.asList(masjidListModel.getPrayerName().split(","));

            if (prayerTime != null) {
                timeTableModelList = new ArrayList<>();
                for (int i = 0; i < prayerTime.size(); i++) {
                    TimeTableModel timeTableModel = new TimeTableModel();
                    timeTableModel.setTt_time(prayerTime.get(i));
                    timeTableModel.setTt_name(prayerName.get(i));
                    timeTableModelList.add(timeTableModel);
                }

//                Collections.sort(timeTableModelList, new Comparator<TimeTableModel>() {
//                    @Override
//                    public int compare(TimeTableModel lhs, TimeTableModel rhs) {
//                        return lhs.getTt_time().compareTo(rhs.getTt_time());
//                    }
//                });

                TimeTableListAdapter adapter = new TimeTableListAdapter(activity, timeTableModelList);
                mTimeTable.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        }
    }

    private class TimeTableListAdapter extends BaseAdapter {

        private ViewHolder holder;
        private List<TimeTableModel> mItem;
        private Context context;

        public TimeTableListAdapter(@NonNull Context context, List<TimeTableModel> list) {
            this.context = context;
            this.mItem = list;
        }


        @Override
        public int getCount() {
            return mItem.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            convertView = inflater.inflate(R.layout.item_timetable, null);

            holder = new ViewHolder();

            holder.txtTTTime = convertView.findViewById(R.id.txtTTTime);
            holder.txtTTName = convertView.findViewById(R.id.txtTTName);
            holder.ttContainer = convertView.findViewById(R.id.ttContainer);
            holder.imgAdd = convertView.findViewById(R.id.imgAdd);

            final TimeTableModel timeTableModel = mItem.get(position);
            holder.txtTTTime.setText(timeTableModel.getTt_time().trim());
            holder.txtTTName.setText(timeTableModel.getTt_name().trim());

            if (position % 2 == 0) {
                holder.ttContainer.setBackgroundColor(Color.parseColor("#F4F8F7"));
            } else {
                holder.ttContainer.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }

            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    flag = true;
                    holder.imgAdd.setVisibility(View.VISIBLE);
                    btnEdit.setVisibility(View.GONE);
                    btnDone.setVisibility(View.VISIBLE);
                    notifyDataSetChanged();
                }
            });

            btnDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    flag = false;
                    holder.imgAdd.setVisibility(View.GONE);
                    btnDone.setVisibility(View.GONE);
                    btnEdit.setVisibility(View.VISIBLE);
                    notifyDataSetChanged();

                    List<String> mFilterTime = new ArrayList<>();
                    List<String> mFilterName = new ArrayList<>();

                    for (TimeTableModel timeTableModel : mItem) {
                        mFilterTime.add(timeTableModel.getTt_time());
                        mFilterName.add(timeTableModel.getTt_name());
                    }

                    String prayertime = mFilterTime.toString().replace(", ", ", ").replaceAll("[\\[.\\]]", "");
                    String prayername = mFilterName.toString().replace(", ", ", ").replaceAll("[\\[.\\]]", "");

                    APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                    final Call<String> loginCall = apiInterface.addMasjidTiming(
                            UserPreferences.loadUser(getActivity()).getId(), masjidListModel.getId(),
                            prayertime, prayername);

                    loginCall.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            if (response.isSuccessful()) {
                                Log.d("UPDATE_TIMETABLE: ", response.body().toString());
                            } else {
                                Toast.makeText(context, " " + response.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Log.d("Error: ", t.getMessage().toString());
                        }
                    });
                }
            });

            final Calendar calendar = Calendar.getInstance();
            holder.imgAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TimePickerDialog timePickerDialog = new TimePickerDialog(context, R.style.DatePickerTheme, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            boolean isPM = (hourOfDay >= 12);
                            String newTime = String.format("%02d:%02d %s", (hourOfDay == 12 || hourOfDay == 0) ? 12 : hourOfDay % 12, minute, isPM ? "PM" : "AM");
                            holder.txtTTTime.setText(newTime);
                            timeTableModel.setTt_time(newTime);
                            notifyDataSetChanged();
//                            notifyItemChanged(position, timeTableModel);
                        }
                    }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false);
                    timePickerDialog.show();
                    timePickerDialog.getButton(TimePickerDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                }
            });

            convertView.setTag(holder);
            if (flag == true) {
                holder.imgAdd.setVisibility(View.VISIBLE);
                btnEdit.setVisibility(View.GONE);
                btnDone.setVisibility(View.VISIBLE);
            }
            return convertView;
        }
    }

    private class ViewHolder {
        protected TextView txtTTTime, txtTTName;
        protected LinearLayout ttContainer;
        protected ImageView imgAdd;
    }

    private void Init(View v) {
        linearUpdateTT = (LinearLayout) v.findViewById(R.id.linearUpdateTT);
        btnEdit = (Button) v.findViewById(R.id.btnEdit);
        btnDone = (Button) v.findViewById(R.id.btnDone);
        mTimeTable = (ListView) v.findViewById(R.id.lstTimeTable);
//        mTimeTable = (RecyclerView) v.findViewById(R.id.rcvTimeTable);
//        mTimeTable.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//        mTimeTable.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}

