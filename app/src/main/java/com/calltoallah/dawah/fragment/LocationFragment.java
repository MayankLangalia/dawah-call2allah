package com.calltoallah.dawah.fragment;

import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.comman.DirectionsJSONParser;
import com.calltoallah.dawah.comman.LocationTrack;
import com.calltoallah.dawah.model.MasjidListModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class LocationFragment extends BaseFragment implements OnMapReadyCallback {

    private GoogleMap mRouteMap;
    private double sLatitude = 0;
    private double sLongitude = 0;
    private double dLatitude = 0;
    private double dLongitude = 0;
    private MapStyleOptions style;
    private ImageView mMyLocation;
    private LatLng origin, dest;
    private TextView myDistance;
    private MasjidListModel masjidListModel;
    private static final int REQUEST_CHECK_SETTINGS = 100;

    private LocationTrack mLocationTrack;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        View myView = inflater.inflate(R.layout.fragment_location, container, false);
        SupportMapFragment routeMap = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.routeMap);
        routeMap.getMapAsync(this);
        return myView;
    }

    public static LocationFragment newInstance(MasjidListModel masjidListModel) {
        LocationFragment aFragment = new LocationFragment();
        Bundle aBundle = new Bundle();
        aBundle.putParcelable(Constants.DATA, masjidListModel);
        aFragment.setArguments(aBundle);
        return aFragment;
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {

        if (getArguments() != null) {
            masjidListModel = getArguments().getParcelable(Constants.DATA);
            dLatitude = Double.parseDouble(masjidListModel.getMasjidLat());
            dLongitude = Double.parseDouble(masjidListModel.getMasjidLng());
        }

        myDistance = (TextView) v.findViewById(R.id.myDistance);
        mMyLocation = (ImageView) v.findViewById(R.id.mMyLocation);
        mMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRouteMap != null && origin != null) {
                    mRouteMap.animateCamera(CameraUpdateFactory.newLatLngZoom(origin, 9));
                }
            }
        });
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + getString(R.string.google_brows_api_key);

        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mRouteMap = googleMap;
        if (mRouteMap != null) {

            if (ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
//            mRouteMap.setTrafficEnabled(true);
//            mRouteMap.setIndoorEnabled(true);
            mRouteMap.setMyLocationEnabled(true); // Default My Location icon Hide ...

            mRouteMap.getUiSettings().setCompassEnabled(true);
            mRouteMap.getUiSettings().setRotateGesturesEnabled(true);
            mRouteMap.getUiSettings().setMyLocationButtonEnabled(false);
            mRouteMap.getUiSettings().setRotateGesturesEnabled(true);
            mRouteMap.getUiSettings().setMapToolbarEnabled(false);

            mLocationTrack = new LocationTrack(getActivity());
            if (mLocationTrack.canGetLocation()) {
                sLatitude = mLocationTrack.getLatitude();
                sLongitude = mLocationTrack.getLongitude();

                origin = new LatLng(sLatitude, sLongitude);
                dest = new LatLng(dLatitude, dLongitude);

                String totDistance = getDistance(origin, dest);
                myDistance.setText("Distance: " + totDistance);

                MarkerOptions dmarkerOptions = new MarkerOptions();
                dmarkerOptions.position(origin);
                dmarkerOptions.title("Destination");
                dmarkerOptions.icon(BitmapDescriptorFactory
                        .fromResource(R.mipmap.pin_source));
                mRouteMap.addMarker(dmarkerOptions);
                mRouteMap.moveCamera(CameraUpdateFactory.newLatLngZoom(origin, 9));

                style = MapStyleOptions.loadRawResourceStyle(activity, R.raw.mapstyle_silver);
                mRouteMap.setMapStyle(style);

                // Getting URL to the Google Directions API
                String url = getDirectionsUrl(origin, dest);

                DownloadTask downloadTask = new DownloadTask();

                // Start downloading json data from Google Directions API
                downloadTask.execute(url);

            } else {
                mLocationTrack.showSettingsAlert();
            }
        }
    }

    public class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    public class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        PolylineOptions lineOptions = new PolylineOptions();

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                List<PatternItem> pattern = Arrays.<PatternItem>asList(new Dot(), new Gap(5f));
                lineOptions.pattern(pattern);

                lineOptions.color(getResources().getColor(R.color.colorPrimaryDark));
                lineOptions.geodesic(true);
            }

            // Drawing polyline in the Google Map for the i-th route
            mRouteMap.addPolyline(lineOptions);
        }
    }

    public String getDistance(LatLng source, LatLng dest) {
        Location l1 = new Location("One");
        l1.setLatitude(source.latitude);
        l1.setLongitude(source.longitude);

        Location l2 = new Location("Two");
        l2.setLatitude(dest.latitude);
        l2.setLongitude(dest.longitude);

        float distance = l1.distanceTo(l2);
        DecimalFormat format = new DecimalFormat("#.##");

        String dist = format.format(distance) + " M";

        if (distance > 1000.0f) {
            distance = distance / 1000.0f;
            dist = format.format(distance) + " KM";
        }

        return dist;
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}

