package com.calltoallah.dawah.fragment;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.calltoallah.dawah.LoginActivity;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.WelcomeActivity;
import com.calltoallah.dawah.comman.CircularImageView;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.comman.PrefManager;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.UserFollowerModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends BaseFragment {

    private CircularImageView profilePhoto;
    private TextView profileFullname, profileUsername;
    private TextView txtUpdateProfile, txtUserVerification, txtMyWallet, txtPendingForApproval, txtRefer, txtMyReport, txtTopDonor, txtAbout, txtPrivacy, txtTerms, txtCustomerSupport,
            txtLogout, txtVersion, txtIntro;
    private CardView cardUpdateProfile, cardUserVerification, cardMyWallet, cardPendingForApproval, cardRefer, cardMyReport,
            cardMyDocuments, cardLogout;
    private TextView txtTotalFollowers, txtTotalFollowings, txtTotalDonated;
    private static ProfileFragment instance = null;
    public static ArrayList<UserFollowerModel> mFollowingLst = new ArrayList<>();
    public static ArrayList<UserFollowerModel> mFollowerLst = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
    }

    public static ProfileFragment getInstance() {
        return instance;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Init(v);
        setData();
    }

    private void setData() {

        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            txtVersion.setText("Version. " + pInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        txtIntro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrefManager prefManager = new PrefManager(getActivity());
                prefManager.setFirstTimeLaunch(true);

                Intent intent = new Intent(getActivity(), WelcomeActivity.class);
                intent.putExtra(Constants.MODE, 0);
                startActivity(intent);
            }
        });

        profileFullname.setText(UserPreferences.loadUser(getActivity()).getFullName());

        txtTotalFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.loadFragment(new UserFollowersFragment());
            }
        });

        txtTotalFollowings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.loadFragment(new UserFollowingsFragment());
            }
        });

        RequestOptions options = new RequestOptions()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.no_profile_image);

        if (!UserPreferences.loadUser(getActivity()).getUser_avatar().equalsIgnoreCase("")) {

            if (UserPreferences.loadUser(getActivity()).getUser_avatar().contains("https://lh3.googleusercontent.com/")) {
                Glide.with(getActivity())
                        .load(UserPreferences.loadUser(getActivity()).getUser_avatar())
                        .apply(options)
                        .into(profilePhoto);
            } else {
                Glide.with(getActivity())
                        .load(APIInterface.API_DOMAIN + UserPreferences.loadUser(getActivity()).getUser_avatar())
                        .apply(options)
                        .into(profilePhoto);
            }
        }

        if (UserPreferences.loadUser(getActivity()).getVerify_status().equalsIgnoreCase("1")) {
            profileFullname.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_verified, 0);
            profileFullname.setCompoundDrawablePadding(5);

            cardUserVerification.setVisibility(View.GONE);
            cardMyWallet.setVisibility(View.VISIBLE);
            cardMyReport.setVisibility(View.VISIBLE);
            cardMyDocuments.setVisibility(View.VISIBLE);
        } else {
            cardUserVerification.setVisibility(View.VISIBLE);
            cardMyWallet.setVisibility(View.GONE);
            cardMyReport.setVisibility(View.GONE);
            cardMyDocuments.setVisibility(View.GONE);
        }

        if (UserPreferences.loadUser(getActivity()).getUser_donated() == null)
            txtTotalDonated.setText("₹ " + 0 + "\n" + "Donated");
        else
            txtTotalDonated.setText("₹ " + UserPreferences.loadUser(getActivity()).getUser_donated() + "\n" + "Donated");

        if (UserPreferences.loadUser(getActivity()).getUsername().equalsIgnoreCase(""))
            profileUsername.setText("not found!");
        else
            profileUsername.setText(UserPreferences.loadUser(getActivity()).getUsername());

        if (UserPreferences.loadUser(getActivity()).getId().equalsIgnoreCase("1")) {
            cardPendingForApproval.setVisibility(View.VISIBLE);
        } else {
            cardPendingForApproval.setVisibility(View.GONE);
        }

        cardMyDocuments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundleDocs = new Bundle();
                bundleDocs.putInt(Constants.MODE, 0);

                DocumentFragment documentFragment = new DocumentFragment();
                documentFragment.setArguments(bundleDocs);
                activity.loadFragment(documentFragment);
            }
        });

        txtUserVerification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.loadFragment(new UserVerificationFragment());
            }
        });

        txtMyWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.loadFragment(new WalletFragment());
            }
        });

        txtUpdateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.loadFragment(new ProfileEditFragment());
            }
        });

        txtPendingForApproval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.loadFragment(new ApprovalFragment());
            }
        });

        txtTopDonor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.loadFragment(new TopDonorFragment());
            }
        });

        txtMyReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.loadFragment(new ReportFragment());
            }
        });

        txtRefer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.loadFragment(new ReferFragment());
            }
        });

        txtPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.MODE, 0);
                bundle.putString(Constants.DATA, "http://calltoallah.com/privacy.html");

                WebFragment webFragment = new WebFragment();
                webFragment.setArguments(bundle);
                activity.loadFragment(webFragment);
            }
        });

        txtTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.MODE, 1);
                bundle.putString(Constants.DATA, "http://calltoallah.com/terms.html");

                WebFragment webFragment = new WebFragment();
                webFragment.setArguments(bundle);
                activity.loadFragment(webFragment);
            }
        });

        txtAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.MODE, 2);
                bundle.putString(Constants.DATA, "http://calltoallah.com/about.html");

                WebFragment webFragment = new WebFragment();
                webFragment.setArguments(bundle);
                activity.loadFragment(webFragment);
            }
        });

        txtCustomerSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.loadFragment(new CustomerSupportFragment());
            }
        });

        txtLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.saveLogin(Constants.LOGIN, 0);
                UserPreferences.clearUser(getActivity());

                Intent intent = new Intent(getActivity(),
                        LoginActivity.class);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                activity.finish();
            }
        });
    }

    public void getUserFollowers() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> loginCall = apiInterface.getUserFollowerList(UserPreferences.loadUser(getActivity()).getId());
        loginCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (response.isSuccessful()) {
                    Log.d("USER_FOLLOWERS: ", response.body().toString());

                    Type listType = new TypeToken<ArrayList<UserFollowerModel>>() {
                    }.getType();
                    ArrayList<UserFollowerModel> userFollowerLst = new Gson().fromJson(response.body().toString(), listType);

                    if (userFollowerLst.size() > 0) {
                        ArrayList<UserFollowerModel> mFilterLst = new ArrayList<>();
                        for (UserFollowerModel userFollowerModel : userFollowerLst) {
                            if (!userFollowerModel.getId().equalsIgnoreCase(UserPreferences.loadUser(getActivity()).getId())) {
                                mFilterLst.add(userFollowerModel);
                            }
                        }

                        mFollowerLst = mFilterLst;
                        txtTotalFollowers.setText(userFollowerLst.size() + "\n" + "Followers");

                    } else {
                        mFollowerLst = new ArrayList<>();
                        txtTotalFollowers.setText(0 + "\n" + "Followers");
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    public void getUserFollowings() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> loginCall = apiInterface.getUserFollowingList(UserPreferences.loadUser(getActivity()).getId());
        loginCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (response.isSuccessful()) {
                    Log.d("USER_FOLLOWINGS: ", response.body().toString());

                    Type listType = new TypeToken<ArrayList<UserFollowerModel>>() {
                    }.getType();
                    ArrayList<UserFollowerModel> userFollowingList = new Gson().fromJson(response.body().toString(), listType);

                    if (userFollowingList.size() > 0) {
                        ArrayList<UserFollowerModel> mFilterLst = new ArrayList<>();
                        for (UserFollowerModel userFollowerModel : userFollowingList) {
                            if (!userFollowerModel.getId().equalsIgnoreCase(UserPreferences.loadUser(getActivity()).getId())) {
                                mFilterLst.add(userFollowerModel);
                            }
                        }

                        mFollowingLst = mFilterLst;
                        txtTotalFollowings.setText(userFollowingList.size() + "\n" + "Followings");

                    } else {
                        mFollowingLst = new ArrayList<>();
                        txtTotalFollowings.setText(0 + "\n" + "Followings");
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private void Init(View v) {
        cardUpdateProfile = (CardView) v.findViewById(R.id.cardUpdateProfile);
        cardUserVerification = (CardView) v.findViewById(R.id.cardUserVerification);
        cardMyWallet = (CardView) v.findViewById(R.id.cardMyWallet);
        cardMyDocuments = (CardView) v.findViewById(R.id.cardMyDocuments);
        cardPendingForApproval = (CardView) v.findViewById(R.id.cardPendingForApproval);
        cardRefer = (CardView) v.findViewById(R.id.cardRefer);
        cardMyReport = (CardView) v.findViewById(R.id.cardMyReport);
        cardLogout = (CardView) v.findViewById(R.id.cardLogout);

        profilePhoto = (CircularImageView) v.findViewById(R.id.profilePhoto);
        profileFullname = (TextView) v.findViewById(R.id.profileFullname);
        profileUsername = (TextView) v.findViewById(R.id.profileUsername);

        txtTotalFollowers = (TextView) v.findViewById(R.id.txtTotalFollowers);
        txtTotalFollowings = (TextView) v.findViewById(R.id.txtTotalFollowings);
        txtTotalDonated = (TextView) v.findViewById(R.id.txtTotalDonated);

        txtUpdateProfile = (TextView) v.findViewById(R.id.txtUpdateProfile);
        txtUserVerification = (TextView) v.findViewById(R.id.txtUserVerification);
        txtMyWallet = (TextView) v.findViewById(R.id.txtMyWallet);
        txtTopDonor = (TextView) v.findViewById(R.id.txtTopDonor);
        txtPendingForApproval = (TextView) v.findViewById(R.id.txtPendingForApproval);
        txtMyReport = (TextView) v.findViewById(R.id.txtMyReport);
        txtRefer = (TextView) v.findViewById(R.id.txtRefer);
        txtAbout = (TextView) v.findViewById(R.id.txtAbout);
        txtPrivacy = (TextView) v.findViewById(R.id.txtPrivacy);
        txtTerms = (TextView) v.findViewById(R.id.txtTerms);
        txtCustomerSupport = (TextView) v.findViewById(R.id.txtCustomerSupport);
        txtLogout = (TextView) v.findViewById(R.id.txtLogout);

        txtVersion = (TextView) v.findViewById(R.id.txtVersion);
        txtIntro = (TextView) v.findViewById(R.id.txtIntro);
    }

    @Override
    public void onStart() {
        super.onStart();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getUserFollowers();
                getUserFollowings();
            }
        }, 500);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}

