package com.calltoallah.dawah.fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.adapter.ReferAdapter;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.ReferModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReferFragment extends BaseFragment {

    private EditText edtReferalCode;
    private RecyclerView mReferList;
    private boolean responseFailed = false;
    private Button btnReferFriends;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_refer, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Init(v);
        setData();
    }

    private void setData() {
        edtReferalCode.setText(UserPreferences.loadUser(getActivity()).getShareCode());
        SnapHelper helper = new PagerSnapHelper();
        helper.attachToRecyclerView(mReferList);

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> apiCall = apiInterface.getUserRefer(UserPreferences.loadUser(getActivity()).getId());
        apiCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {

                    try {
                        String res = response.body().toString();
                        Log.d("REFER: ", res);

                        try {
                            Gson gson = new Gson();
                            Reader reader = new StringReader(res);
                            ReferModel referModel = gson.fromJson(reader, ReferModel.class);

                            mReferList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                            mReferList.setHasFixedSize(true);

                            mReferList.setItemAnimator(new DefaultItemAnimator());
                            ReferAdapter referAdapter = new ReferAdapter(getActivity(), referModel.getUserAddedList());
                            mReferList.setAdapter(referAdapter);

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
            }
        });

        btnReferFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = "Adaab, I am Inviting to Join me on Dawah App\n\n" +
                        "Dawah is the crowdfunding initiative to help the community to support our beloved Masjids and Madrasahs\n\n" +
                        "Moreover you can Find Nearest Masjids and Madrasahs, Get Notified on Salah Timings and Record your Salah, Calculate and Record your Tasbihaats, Find any Word in Quran Directory, Check Qibla Direction with Our Compass, View Events of Our Islamic Dates Calendar, Calculate and Pay Zakat Instantly, Donate your beloved Masjids and Madrasahs, Connect with Top Donors and many more\n\n" +
                        "My Referral Code is :" + UserPreferences.loadUser(getActivity()).getShareCode() + "\n\n" +
                        "Get it downloaded on : dawah.calltoallah.com";

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, text);
                sendIntent.setType("text/plain");

                Intent shareIntent = Intent.createChooser(sendIntent, null);
                startActivity(shareIntent);
            }
        });
    }

    private void Init(View v) {
        mReferList = (RecyclerView) v.findViewById(R.id.mReferList);
        edtReferalCode = (EditText) v.findViewById(R.id.edtReferalCode);
        btnReferFriends = (Button) v.findViewById(R.id.btnReferFriends);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}
