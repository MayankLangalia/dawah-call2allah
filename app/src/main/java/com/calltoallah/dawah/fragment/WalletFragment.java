package com.calltoallah.dawah.fragment;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.calltoallah.dawah.DonationActivity;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.adapter.TransactionAdapter;
import com.calltoallah.dawah.comman.ProgressbarManager;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.TransactionModel;
import com.calltoallah.dawah.model.TransectionListModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletFragment extends BaseFragment {

    private TextView txtWalletAmount;
    private RecyclerView mTransaction;
    private ProgressbarManager progressbarManager;
    private boolean responseFailed = false;
    private Button btnWidthdraw, btnDonateNow;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout contentLayout, errorLayout;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_wallet, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Init(v);
        setData();
    }

    private void setData() {
        btnWidthdraw.setVisibility(View.GONE);

        progressbarManager.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getWalletAmount();
                getTransaction();
            }
        }, 1000);

        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getTransaction();
            }
        });

        btnDonateNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.loadFragment(new HomeFragment());
            }
        });
    }

    private void getWalletAmount() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> loginCall = apiInterface.getUserWallet(UserPreferences.loadUser(getActivity()).getId());
        loginCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {

                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        Log.d("UWALLET: ", response.body().toString());

                        if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                           int userWalletAmt = Integer.parseInt(jsonObject.getString("amount"));

                            if (userWalletAmt > 0) {
                                txtWalletAmount.setText("₹ " + userWalletAmt);
                            } else {
                                txtWalletAmount.setText("₹ " + "0");
                            }

                        } else {
                            Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    private void getTransaction() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> apiCall = apiInterface.getTransactionHistory(UserPreferences.loadUser(getActivity()).getId());
        apiCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    swipeRefreshLayout.setRefreshing(false);
                    progressbarManager.dismiss();

                    try {
                        String res = response.body().toString();
                        Log.d("WALLET: ", res);

                        try {
//                            Type listType = new TypeToken<ArrayList<LoginModel>>() {
//                            }.getType();
//                            ArrayList<LoginModel> workList = new Gson().fromJson(res, listType);

                            Gson gson = new Gson();
                            Reader reader = new StringReader(res);
                            TransactionModel transactionModel = gson.fromJson(reader, TransactionModel.class);

                            List<TransectionListModel> mFilterList = new ArrayList<>();
                            for (TransectionListModel transectionListModel : transactionModel.getUserTransectionList()) {
                                if (!transectionListModel.getAmountStatus().equalsIgnoreCase("view")) {
                                    mFilterList.add(transectionListModel);
                                }
                            }

                            Collections.sort(mFilterList, new BY_DATE(true));
                            if (mFilterList.size() > 0) {
                                contentLayout.setVisibility(View.VISIBLE);
                                errorLayout.setVisibility(View.GONE);

                                TransactionAdapter transactionAdapter = new TransactionAdapter(activity, mFilterList);
                                mTransaction.setAdapter(transactionAdapter);

//                                if (transactionAdapter.getWalletTotal(mFilterList) == 0) {
//                                    txtWalletAmount.setText("₹ " + "0");
//                                } else {
//                                    txtWalletAmount.setText("₹ " + transactionAdapter.getWalletTotal(mFilterList) + "");
//                                }

                            } else {
                                contentLayout.setVisibility(View.GONE);
                                errorLayout.setVisibility(View.VISIBLE);
                            }

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            contentLayout.setVisibility(View.GONE);
                            errorLayout.setVisibility(View.VISIBLE);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    progressbarManager.dismiss();
                    contentLayout.setVisibility(View.GONE);
                    errorLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                progressbarManager.dismiss();
                contentLayout.setVisibility(View.GONE);
                errorLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    public class BY_DATE implements Comparator<TransectionListModel> {
        private int mod = 1;

        public BY_DATE(boolean desc) {
            if (desc) mod = -1;
        }

        @Override
        public int compare(TransectionListModel arg0, TransectionListModel arg1) {
            return mod * arg0.getCreated_on().compareTo(arg1.getCreated_on());
        }
    }

    private void Init(View v) {
        progressbarManager = new ProgressbarManager(getActivity());
        swipeRefreshLayout = v.findViewById(R.id.swipeRefresh);
        txtWalletAmount = (TextView) v.findViewById(R.id.txtWalletAmount);
        btnWidthdraw = (Button) v.findViewById(R.id.btnWidthdraw);
        btnDonateNow = (Button) v.findViewById(R.id.btnDonateNow);
        contentLayout = v.findViewById(R.id.contentLayout);
        errorLayout = v.findViewById(R.id.errorLayout);

        mTransaction = (RecyclerView) v.findViewById(R.id.rcvTransaction);
        mTransaction.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mTransaction.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}

