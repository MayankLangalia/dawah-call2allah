package com.calltoallah.dawah.notification;

import android.annotation.SuppressLint;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class LogTag {
    private static final String TAG = "ALARM";

    public static void e(String msg) {
        Log.e(TAG, msg);
    }

    public static void d(String msg) {
        Log.d(TAG, msg);
        appendLog(msg, "log");
    }

    @SuppressLint("SimpleDateFormat")
    public static void appendLog(String text, String prefix) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd_MM_HH_");
        prefix = simpleDateFormat.format(Calendar.getInstance().getTime()) + prefix;

        File logFile = new File(Environment.getExternalStorageDirectory().getPath() + "/" + prefix + ".txt");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try {
            // BufferedWriter for performance, true to set append to file
            // flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile,
                    true));
            simpleDateFormat.applyPattern("dd-MM-yyyy HH:mm:ss");
            buf.append(simpleDateFormat.format(new Date()) + "\t" + text + "\n");


            buf.newLine();
            buf.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
