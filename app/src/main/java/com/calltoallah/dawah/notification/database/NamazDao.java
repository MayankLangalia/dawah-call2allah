package com.calltoallah.dawah.notification.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface NamazDao {
    @Query("SELECT * FROM NamazMaster")
    List<NamazMaster> getAll();

    @Query("SELECT * FROM NamazMaster where namaz_time>:currentTime order by namaz_time")
    List<NamazMaster> getAll(long currentTime);


    @Query("SELECT * FROM NamazMaster where uid =:id order by namaz_time limit 1")
    NamazMaster getNamazFromId(long id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long addData(NamazMaster data);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addData(List<NamazMaster> data);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateData(NamazMaster... data);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int updateData(List<NamazMaster> data);

    @Query("DELETE FROM NamazMaster")
    void deleteData();
}
