package com.calltoallah.dawah.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.CircularImageView;
import com.calltoallah.dawah.model.ReferUserAddedListModel;

import java.util.List;

import static com.calltoallah.dawah.network.APIInterface.API_DOMAIN;

public class ReferAdapter extends RecyclerView.Adapter {

    private FragmentActivity context;
    private List<ReferUserAddedListModel> mItem;

    public ReferAdapter(FragmentActivity context, List<ReferUserAddedListModel> items) {
        this.context = context;
        this.mItem = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_refer, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (!mItem.isEmpty() && mItem != null) {
            final ViewHolder viewHolders = (ViewHolder) viewHolder;

            final ReferUserAddedListModel referUserAddedListModel = mItem.get(position);
            viewHolders.txtReferName.setText(referUserAddedListModel.getFullName());

            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.no_profile_image)
                    .error(R.drawable.no_profile_image);

            Glide.with(context)
                    .load(API_DOMAIN + referUserAddedListModel.getUserAvatar())
                    .apply(options)
                    .into(viewHolders.imgReferImage);
        }
    }

    @Override
    public int getItemCount() {
        return mItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView txtReferName;
        protected CircularImageView imgReferImage;

        public ViewHolder(View itemView) {
            super(itemView);
            txtReferName = itemView.findViewById(R.id.txtReferName);
            imgReferImage = itemView.findViewById(R.id.imgReferImage);
        }
    }
}
