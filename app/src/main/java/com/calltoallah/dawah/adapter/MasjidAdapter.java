package com.calltoallah.dawah.adapter;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.comman.LocationTrack;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.firebase.SendNotification;
import com.calltoallah.dawah.fragment.MasjidDetailsFragment;
import com.calltoallah.dawah.model.FollowerModel;
import com.calltoallah.dawah.model.MasjidListModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Reader;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.calltoallah.dawah.network.APIInterface.API_DOMAIN;
import static java.lang.Math.acos;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class MasjidAdapter extends RecyclerView.Adapter implements Filterable {

    private MainActivity context;
    private List<MasjidListModel> mItem;
    private MasjidFilter mFilter;
    private String mSearchText = "";
    private OnItemClickListener mItemClickListener;

    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public MasjidAdapter(MainActivity context, List<MasjidListModel> items) {
        this.context = context;
        this.mItem = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_masjid, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public Typeface getFont() {
        return Typeface.createFromAsset(context.getAssets(), "utsaah.ttf");
    }

    private static double distance(double lat1, double lon1, double lat2, double lon2, String unit) {
        if ((lat1 == lat2) && (lon1 == lon2)) {
            return 0;
        }
        else {
            double theta = lon1 - lon2;
            double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2)) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            dist = dist * 60 * 1.1515;
            if (unit.equals("K")) {
                dist = dist * 1.609344;
            } else if (unit.equals("N")) {
                dist = dist * 0.8684;
            }
            return (dist);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (!mItem.isEmpty() && mItem != null) {
            final ViewHolder viewHolders = (ViewHolder) viewHolder;

            MasjidListModel masjidListModel = mItem.get(position);

            LocationTrack mLocationTrack = new LocationTrack(context);
            if (mLocationTrack.canGetLocation()) {
                double slongitude = mLocationTrack.getLongitude();
                double slatitude = mLocationTrack.getLatitude();

                double dlongitude = Double.parseDouble(masjidListModel.getMasjidLng());
                double dlatitude = Double.parseDouble(masjidListModel.getMasjidLat());

                Location l1 = new Location("One");
                l1.setLatitude(slatitude);
                l1.setLongitude(slongitude);

                Location l2 = new Location("Two");
                l2.setLatitude(dlatitude);
                l2.setLongitude(dlongitude);

                float distance = l1.distanceTo(l2);
                DecimalFormat format = new DecimalFormat("#.##");

                String dist = format.format(distance) + " M";

                if (distance > 1000.0f) {
                    distance = distance / 1000.0f;
                    dist = format.format(distance) + " KM";
                }

                viewHolders.txtMasjidDistance.setText(dist);
            } else {
                mLocationTrack.showSettingsAlert();
            }

            viewHolders.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.MODE, 1);
                    bundle.putParcelable(Constants.DATA, mItem.get(position));

                    MasjidDetailsFragment masjidDetailsFragment = new MasjidDetailsFragment();
                    masjidDetailsFragment.setArguments(bundle);
                    context.loadFragment(masjidDetailsFragment);
//                    if (mItemClickListener != null) {
//                        mItemClickListener.onItemClick(view, position);
//                    }
                }
            });

            if (masjidListModel.getFollow_status() == 1) {
                viewHolders.imgFollow.setImageResource(R.drawable.ic_action_follow);
            } else {
                viewHolders.imgFollow.setImageResource(R.drawable.ic_action_unfollow);
            }

            viewHolders.imgFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                    final Call<String> loginCall = apiInterface.addMasjidFollow(masjidListModel.getId(),
                            UserPreferences.loadUser(context).getId());
                    loginCall.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            Log.d("FOLLOW: ", response.body().toString());
                            if (response.isSuccessful()) {

                                try {
                                    JSONObject jsonObject = new JSONObject(response.body().toString());
                                    if (jsonObject.getString("message").equalsIgnoreCase("Masjid Followed Successfully")) {
                                        masjidListModel.setFollow_status(1);
                                        viewHolders.imgFollow.setImageResource(R.drawable.ic_action_follow);

                                        SendNotification.getUserNotificationToken(masjidListModel.getUserId(),
                                                context.getString(R.string.app_name),
                                                UserPreferences.loadUser(context).getFullName() + " Followed your Masjid (" + masjidListModel.getMasjidName() + ")", context);

                                    } else {
                                        masjidListModel.setFollow_status(0);
                                        viewHolders.imgFollow.setImageResource(R.drawable.ic_action_unfollow);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                        }
                    });

                    notifyDataSetChanged();
                }
            });

            viewHolders.btnDirection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + masjidListModel.getMasjidLat() + "," + masjidListModel.getMasjidLng());
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    if (mapIntent.resolveActivity(context.getPackageManager()) != null) {
                        context.startActivity(mapIntent);
                    }
                }
            });

            if (masjidListModel.getVerified_status().equalsIgnoreCase("1")) {
                viewHolders.txtMasjidName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_verified, 0);
                viewHolders.txtMasjidName.setCompoundDrawablePadding(5);
            }

            if (mSearchText != null && !mSearchText.isEmpty()) {
                // Masjid name ...
                int startName = masjidListModel.getMasjidName().toUpperCase().indexOf(mSearchText.toUpperCase());
                int endName = startName + mSearchText.length();

                if (startName != -1) {
                    Spannable spannable = new SpannableString(masjidListModel.getMasjidName());
                    spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#A9A9A9")), startName, endName, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    viewHolders.txtMasjidName.setText(spannable);
                } else {
                    viewHolders.txtMasjidName.setText(masjidListModel.getMasjidName());
                }

                // Masjid username ...
                int startUsername = masjidListModel.getMasjidUsername()
                        .replaceAll(" ", "").toUpperCase().indexOf(mSearchText.toUpperCase());
                int endUsername = startUsername + mSearchText.length();

                if (startUsername != -1) {
                    Spannable spannable = new SpannableString(masjidListModel.getMasjidUsername());
                    spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#A9A9A9")), startUsername, endUsername, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    viewHolders.txtMasjidUsername.setText(spannable);
                } else {
                    viewHolders.txtMasjidUsername.setText(masjidListModel.getMasjidUsername());
                }

                // Masjid Address ...
                int startAddress = masjidListModel.getAddress().toUpperCase().indexOf(mSearchText.toUpperCase());
                int endAddress = startAddress + mSearchText.length();

                if (startAddress != -1) {
                    Spannable spannable = new SpannableString(masjidListModel.getAddress());
                    spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#A9A9A9")), startAddress, endAddress, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    viewHolders.txtMasjidAddress.setText(spannable);
                } else {
                    viewHolders.txtMasjidAddress.setText(masjidListModel.getAddress());
                }

            } else {
                viewHolders.txtMasjidName.setText(masjidListModel.getMasjidName());
                viewHolders.txtMasjidUsername.setText(masjidListModel.getMasjidUsername());
                viewHolders.txtMasjidAddress.setText(masjidListModel.getAddress());
            }

            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.no_profile_image)
                    .error(R.drawable.no_profile_image);

            Glide.with(context)
                    .load(API_DOMAIN + masjidListModel.getImagePath())
                    .apply(options)
                    .into(viewHolders.imgMasjidIcon);

            getFollowers(viewHolders, masjidListModel.getId());
        }
    }

    public void getFollowers(ViewHolder viewHolder, String masjid_id) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> apiCall = apiInterface.getFollowers(masjid_id, UserPreferences.loadUser(context).getId());
        apiCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    Log.d("FOLLOWER: ", response.body().toString());

                    Gson gson = new Gson();
                    Reader reader = new StringReader(response.body().toString());
                    FollowerModel followerModel = gson.fromJson(reader, FollowerModel.class);

                    if (followerModel.getUserList() != null) {
                        viewHolder.txtFollowers.setText(followerModel.getUserList().size() + "+  Followers");

//                        viewHolder.mRecyclerViewAvatar.setLayoutManager(new LinearLayoutManager(context,
//                                LinearLayoutManager.HORIZONTAL, false));
//                        viewHolder.mRecyclerViewAvatar.addItemDecoration(new OverlapDecoration());
//                        viewHolder.mRecyclerViewAvatar.setHasFixedSize(true);
//
//                        AvatarAdapter avatarAdapter = new AvatarAdapter(context, followerModel.getUserList());
//                        viewHolder.mRecyclerViewAvatar.setAdapter(avatarAdapter);
//                        avatarAdapter.notifyDataSetChanged();

                    } else {
                        viewHolder.txtFollowers.setText("0  Followers");
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new MasjidFilter(mItem, this);
        }
        return mFilter;
    }

    public class MasjidFilter extends Filter {
        private MasjidAdapter adapter;
        private List<MasjidListModel> filterList;

        public MasjidFilter(List<MasjidListModel> filterList, MasjidAdapter adapter) {
            this.adapter = adapter;
            this.filterList = filterList;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint.toString().isEmpty()) {
                mSearchText = "";
            } else {
                mSearchText = (String) constraint;
            }

            if (constraint != null && constraint.length() > 0) {
                constraint = constraint.toString().toUpperCase();
                ArrayList<MasjidListModel> filteredPlayers = new ArrayList<>();
                for (int i = 0; i < filterList.size(); i++) {
                    if (filterList.get(i).getMasjidName().toUpperCase().contains(constraint) ||
                            filterList.get(i).getMasjidUsername().toUpperCase().contains(constraint) ||
                            filterList.get(i).getAddress().toUpperCase().contains(constraint)) {
                        filteredPlayers.add(filterList.get(i));
                    }
                }
                results.count = filteredPlayers.size();
                results.values = filteredPlayers;
            } else {
                results.count = filterList.size();
                results.values = filterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mItem = (List<MasjidListModel>) results.values;
            adapter.notifyDataSetChanged();
        }
    }


    @Override
    public int getItemCount() {
        return mItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView txtMasjidName, txtMasjidUsername, txtMasjidDistance, txtMasjidAddress, txtFollowers;
        protected ImageView imgMasjidIcon, imgFollow;
        protected Button btnDirection;
//        protected RecyclerView mRecyclerViewAvatar;

        public ViewHolder(View itemView) {
            super(itemView);

            txtMasjidName = itemView.findViewById(R.id.txtMasjidName);
            txtMasjidUsername = itemView.findViewById(R.id.txtMasjidUsername);
            txtFollowers = itemView.findViewById(R.id.txtFollowers);
            txtMasjidDistance = itemView.findViewById(R.id.txtMasjidDistance);
            txtMasjidAddress = itemView.findViewById(R.id.txtMasjidAddress);

            imgMasjidIcon = itemView.findViewById(R.id.imgMasjidIcon);
            imgFollow = itemView.findViewById(R.id.imgFollow);
            btnDirection = itemView.findViewById(R.id.btnDirection);

//            mRecyclerViewAvatar = itemView.findViewById(R.id.rcvAvatarList);
        }
    }
}
