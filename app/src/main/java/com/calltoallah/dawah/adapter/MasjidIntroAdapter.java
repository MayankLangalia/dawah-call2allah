package com.calltoallah.dawah.adapter;

import android.graphics.Typeface;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.MasjidIntroActivity;
import com.calltoallah.dawah.R;

import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.firebase.SendNotification;
import com.calltoallah.dawah.model.FollowerModel;
import com.calltoallah.dawah.model.MasjidListModel;
import com.calltoallah.dawah.model.ProfileModel;
import com.calltoallah.dawah.model.ProfileUserInfo;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.calltoallah.dawah.MasjidIntroActivity.btnNext;
import static com.calltoallah.dawah.network.APIInterface.API_DOMAIN;

public class MasjidIntroAdapter extends RecyclerView.Adapter {

    private MasjidIntroActivity context;
    private List<MasjidListModel> mItem;
    private OnItemClickListener mItemClickListener;

    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public MasjidIntroAdapter(MasjidIntroActivity context, List<MasjidListModel> items) {
        this.context = context;
        this.mItem = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_masjid_follow, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public Typeface getFont() {
        return Typeface.createFromAsset(context.getAssets(), "utsaah.ttf");
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (!mItem.isEmpty() && mItem != null) {
            final ViewHolder viewHolders = (ViewHolder) viewHolder;

            MasjidListModel masjidListModel = mItem.get(position);
            viewHolders.txtMasjidName.setText(masjidListModel.getMasjidName());

            viewHolders.txtFeedCount.setText(Html.fromHtml("<font color=" + context.getResources().getColor(R.color.colorDarkGray) + "> " + "Feeds" + "</font><br>" +
                    "<b><font color=" + context.getResources().getColor(R.color.colorBlack) + "> " + masjidListModel.getPost_count() + "</font></b>"));

            viewHolders.txtFollowersCount.setText(Html.fromHtml("<font color=" + context.getResources().getColor(R.color.colorDarkGray) + "> " + "Followers" + "</font><br>" +
                    "<b><font color=" + context.getResources().getColor(R.color.colorBlack) + "> " + masjidListModel.getFollowers_count() + "</font></b>"));

            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.no_profile_image)
                    .error(R.drawable.no_profile_image);

            Glide.with(context)
                    .load(API_DOMAIN + masjidListModel.getImagePath())
                    .apply(options)
                    .into(viewHolders.imgMasjidIcon);

            if (masjidListModel.getFollow_status() == 1) {
                viewHolders.btnFollow.setBackgroundResource(R.drawable.button_focus);
                viewHolders.btnFollow.setTextColor(context.getResources().getColor(R.color.colorWhite));

            } else {
                viewHolders.btnFollow.setBackgroundResource(R.drawable.button_normal);
                viewHolders.btnFollow.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            }

            showNext();
            viewHolders.btnFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                    final Call<String> loginCall = apiInterface.addMasjidFollow(masjidListModel.getId(),
                            UserPreferences.loadUser(context).getId());
                    loginCall.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            Log.d("FOLLOW: ", response.body().toString());
                            if (response.isSuccessful()) {

                                try {
                                    JSONObject jsonObject = new JSONObject(response.body().toString());
                                    if (jsonObject.getString("message").equalsIgnoreCase("Masjid Followed Successfully")) {
                                        masjidListModel.setFollow_status(1);
                                        masjidListModel.setFollowers_count(masjidListModel.getFollowers_count() + 1);

                                        showNext();
                                        SendNotification.getUserNotificationToken(masjidListModel.getUserId(),
                                                context.getString(R.string.app_name),
                                                UserPreferences.loadUser(context).getFullName() + " Followed your Masjid (" + masjidListModel.getMasjidName() + ")", context);

                                    } else {
                                        masjidListModel.setFollow_status(0);
                                        masjidListModel.setFollowers_count(masjidListModel.getFollowers_count() - 1);

                                        showNext();
                                    }

                                    notifyItemChanged(position);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                        }
                    });
                }
            });
        }
    }

    private void showNext() {
        List<MasjidListModel> mTemp2 = new ArrayList<>();
        for (MasjidListModel model : mItem) {
            if (model.getFollow_status() == 1)
                mTemp2.add(model);
        }

        if (mTemp2.size() > 2) {
            btnNext.setVisibility(View.VISIBLE);
        } else {
            btnNext.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView txtMasjidName, txtFeedCount, txtFollowersCount;
        protected ImageView imgMasjidIcon;
        protected Button btnFollow;

        public ViewHolder(View itemView) {
            super(itemView);

            txtMasjidName = itemView.findViewById(R.id.masjidName);
            txtFeedCount = itemView.findViewById(R.id.txtFeedCount);
            txtFollowersCount = itemView.findViewById(R.id.txtFollowersCount);
            imgMasjidIcon = itemView.findViewById(R.id.masjidIcon);
            btnFollow = itemView.findViewById(R.id.btnFollow);
        }
    }
}
