package com.calltoallah.dawah.adapter;

import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.model.QuranDataModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class QuranAdapter extends RecyclerView.Adapter implements Filterable {

    private MainActivity context;
    private List<QuranDataModel> mItem;
    private String mSearchText;
    private ArrayFilter mFilter;

    public QuranAdapter(MainActivity context, List<QuranDataModel> items) {
        this.context = context;
        this.mItem = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_quran, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public Typeface getFont() {
        return Typeface.createFromAsset(context.getAssets(), "utsaah.ttf");
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (!mItem.isEmpty() && mItem != null) {
            final ViewHolder viewHolders = (ViewHolder) viewHolder;
            viewHolders.txtLanguageNo.setText("Surah: " + mItem.get(position).getSura() + " "
                    + "Ayah: " + mItem.get(position).getAya());

            viewHolders.txtEnglishTranslator.setText("English: " + mItem.get(position).getTranslator_english());
            viewHolders.txtHindiTranslator.setText("Hindi: " + mItem.get(position).getTranslator_hindi());
            viewHolders.txtUrduTranslator.setText("Urdu: " + mItem.get(position).getTranslator_urdu());
            viewHolders.txtArabicTranslator.setText("Arabic: " + mItem.get(position).getTranslator_arabic());

            if (mSearchText != null && !mSearchText.isEmpty()) {
                // For English ...
                int startEnglish = mItem.get(position).getText_english().toLowerCase(Locale.US).indexOf(mSearchText.toLowerCase(Locale.US));
                int endEnglish = startEnglish + mSearchText.length();

                if (startEnglish != -1) {
                    Spannable spannable = new SpannableString(mItem.get(position).getText_english());
                    spannable.setSpan(new ForegroundColorSpan(Color.RED), startEnglish, endEnglish, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    viewHolders.txtEnglishText.setText(spannable);
                } else {
                    viewHolders.txtEnglishText.setText(mItem.get(position).getText_english());
                }

                // For Hindi ...
                int startHindi = mItem.get(position).getText_hindi().indexOf(mSearchText);
                int endHindi = startHindi + mSearchText.length();

                if (startHindi != -1) {
                    Spannable spannable = new SpannableString(mItem.get(position).getText_hindi());
                    spannable.setSpan(new ForegroundColorSpan(Color.RED), startHindi, endHindi, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    viewHolders.txtHindiText.setText(spannable);
                } else {
                    viewHolders.txtHindiText.setText(mItem.get(position).getText_hindi());
                }

                // For Urdu ...
                int startUrdu = mItem.get(position).getText_urdu().indexOf(mSearchText);
                int endUrdu = startUrdu + mSearchText.length();

                if (startUrdu != -1) {
                    Spannable spannable = new SpannableString(mItem.get(position).getText_urdu());
                    spannable.setSpan(new ForegroundColorSpan(Color.RED), startUrdu, endUrdu, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    viewHolders.txtUrduText.setText(spannable);
                } else {
                    viewHolders.txtUrduText.setText(mItem.get(position).getText_urdu());
                }

                // For Arabic ...
                int startArabic = mItem.get(position).getText_arabic().indexOf(mSearchText);
                int endArabic = startArabic + mSearchText.length();

                if (startArabic != -1) {
                    Spannable spannable = new SpannableString(mItem.get(position).getText_arabic());
                    spannable.setSpan(new ForegroundColorSpan(Color.RED), startArabic, endArabic, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    viewHolders.txtArabicText.setText(spannable);
                } else {
                    viewHolders.txtArabicText.setText(mItem.get(position).getText_arabic());
                }

            } else {
                viewHolders.txtEnglishText.setText(mItem.get(position).getText_english());
                viewHolders.txtHindiText.setText(mItem.get(position).getText_hindi());
                viewHolders.txtUrduText.setText(mItem.get(position).getText_urdu());
                viewHolders.txtArabicText.setText(mItem.get(position).getText_arabic());
            }
        }
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new ArrayFilter(mItem, this);
        }
        return mFilter;
    }

    public class ArrayFilter extends Filter {
        private QuranAdapter adapter;
        private List<QuranDataModel> mFilterList;

        public ArrayFilter(List<QuranDataModel> filterList, QuranAdapter adapter) {
            this.adapter = adapter;
            this.mFilterList = filterList;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint.toString().isEmpty()) {
                mSearchText = "";
            } else {
                mSearchText = (String) constraint;
            }

            if (constraint != null && constraint.length() > 0) {
                constraint = constraint.toString();
                ArrayList<QuranDataModel> filteredPlayers = new ArrayList<>();
                for (int i = 0; i < mFilterList.size(); i++) {
                    if (mFilterList.get(i).getText_english().toLowerCase().contains(constraint) ||
                            mFilterList.get(i).getText_hindi().toLowerCase().contains(constraint) ||
                            mFilterList.get(i).getText_urdu().toLowerCase().contains(constraint) ||
                            mFilterList.get(i).getText_arabic().toLowerCase().contains(constraint)) {
                        filteredPlayers.add(mFilterList.get(i));
                    }
                }
                results.count = filteredPlayers.size();
                results.values = filteredPlayers;
            } else {
                results.count = mFilterList.size();
                results.values = mFilterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mItem = (ArrayList<QuranDataModel>) results.values;
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return mItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtLanguageNo, txtEnglishTranslator, txtHindiTranslator, txtUrduTranslator, txtArabicTranslator,
                txtEnglishText, txtHindiText, txtUrduText, txtArabicText;

        public ViewHolder(View itemView) {
            super(itemView);
            txtLanguageNo = itemView.findViewById(R.id.txtLanguageNo);

            txtEnglishTranslator = itemView.findViewById(R.id.txtEnglishTranslator);
            txtHindiTranslator = itemView.findViewById(R.id.txtHindiTranslator);
            txtUrduTranslator = itemView.findViewById(R.id.txtUrduTranslator);
            txtArabicTranslator = itemView.findViewById(R.id.txtArabicTranslator);

            txtEnglishText = itemView.findViewById(R.id.txtEnglishText);
            txtHindiText = itemView.findViewById(R.id.txtHindiText);
            txtUrduText = itemView.findViewById(R.id.txtUrduText);
            txtArabicText = itemView.findViewById(R.id.txtArabicText);
        }
    }
}
