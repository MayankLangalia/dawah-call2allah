package com.calltoallah.dawah.adapter;

import android.app.Dialog;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.CircularImageView;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.ProfileModel;
import com.calltoallah.dawah.model.ProfileUserInfo;
import com.calltoallah.dawah.model.UserFollowerModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.calltoallah.dawah.network.APIInterface.API_DOMAIN;

public class UserFollowerAdapter extends RecyclerView.Adapter implements Filterable {

    private MainActivity context;
    private static List<UserFollowerModel> mItem;
    private FollowerFilter mFilter;
    private String mSearchText = "";
    private boolean responseFailed = false;
    private int mode;

    public UserFollowerAdapter(MainActivity context, List<UserFollowerModel> items, int mode) {
        this.context = context;
        this.mItem = items;
        this.mode = mode;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new FollowerFilter(mItem, this);
        }
        return mFilter;
    }

    public class FollowerFilter extends Filter {
        private UserFollowerAdapter adapter;
        private List<UserFollowerModel> filterList;

        public FollowerFilter(List<UserFollowerModel> filterList, UserFollowerAdapter adapter) {
            this.adapter = adapter;
            this.filterList = filterList;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint.toString().isEmpty()) {
                mSearchText = "";
            } else {
                mSearchText = (String) constraint;
            }

            if (constraint != null && constraint.length() > 0) {
                constraint = constraint.toString().toUpperCase();
                ArrayList<UserFollowerModel> filteredPlayers = new ArrayList<>();
                for (int i = 0; i < filterList.size(); i++) {
                    if (filterList.get(i).getFullName().toUpperCase().contains(constraint) ||
                            filterList.get(i).getUsername().toUpperCase().contains(constraint)) {
                        filteredPlayers.add(filterList.get(i));
                    }
                }
                results.count = filteredPlayers.size();
                results.values = filteredPlayers;
            } else {
                results.count = filterList.size();
                results.values = filterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mItem = (ArrayList<UserFollowerModel>) results.values;
            adapter.notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_followers, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (!mItem.isEmpty() && mItem != null) {
            final ViewHolder viewHolders = (ViewHolder) viewHolder;

            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.no_profile_image)
                    .error(R.drawable.no_profile_image);

            Glide.with(context)
                    .load(API_DOMAIN + mItem.get(position).getUserAvatar())
                    .apply(options)
                    .into(viewHolders.imgFollowerIcon);

            viewHolders.imgFollowerIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserProfileRequest(mItem.get(position).getId());
                }
            });

            viewHolders.txtFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    followRequest(viewHolders, mItem.get(position).getId(), position);
                }
            });

            if (mode == 0) {
                viewHolders.txtFollow.setText("Follow");
            } else {
                viewHolders.txtFollow.setText("Following");
            }

            if (mSearchText != null && !mSearchText.isEmpty()) {
                // Admin name ...
                int startName = mItem.get(position).getFullName().toUpperCase().indexOf(mSearchText.toUpperCase());
                int endName = startName + mSearchText.length();

                if (startName != -1) {
                    Spannable spannable = new SpannableString(mItem.get(position).getFullName());
                    spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#A9A9A9")), startName, endName, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    viewHolders.txtFollowerName.setText(spannable);
                } else {
                    viewHolders.txtFollowerName.setText(mItem.get(position).getFullName());
                }

                // Admin username ...
                int startUsername = mItem.get(position).getUsername()
                        .replaceAll(" ", "").toUpperCase().indexOf(mSearchText.toUpperCase());
                int endUsername = startUsername + mSearchText.length();

                if (startUsername != -1) {
                    Spannable spannable = new SpannableString(mItem.get(position).getUsername());
                    spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#A9A9A9")), startUsername, endUsername, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    viewHolders.txtFollowerUsername.setText(spannable);
                } else {
                    viewHolders.txtFollowerUsername.setText(mItem.get(position).getUsername());
                }

            } else {
                viewHolders.txtFollowerName.setText(mItem.get(position).getFullName());
                if (mItem.get(position).getUsername().equalsIgnoreCase(""))
                    viewHolders.txtFollowerUsername.setText("not found!");
                else
                    viewHolders.txtFollowerUsername.setText(mItem.get(position).getUsername());
            }

            viewHolders.txtFollow.setBackgroundResource(R.drawable.box_fill);
            viewHolders.txtFollow.setTextColor(context.getResources().getColor(R.color.colorWhite));
        }
    }

    private void followRequest(UserFollowerAdapter.ViewHolder viewHolder, String user_id, int position) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> apiCall = apiInterface.addUserFollowFollowingRequest(
                UserPreferences.loadUser(context).getId(), user_id);
        apiCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {

                    try {
                        String res = response.body().toString();
                        Log.d("USER_ADD_FOLLOW: ", res);

                        try {
                            JSONObject jsonObject = new JSONObject(res);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                viewHolder.txtFollow.setBackgroundResource(R.drawable.box_fill);
                                viewHolder.txtFollow.setTextColor(context.getResources().getColor(R.color.colorWhite));

                            } else {
                                viewHolder.txtFollow.setBackgroundResource(R.drawable.box);
                                viewHolder.txtFollow.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                            }

                            mItem.remove(position);
                            notifyItemRemoved(position);
                            notifyItemRangeChanged(position, mItem.size());

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            Toast.makeText(context, context.getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(context, context.getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, context.getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
            }
        });
    }


//    private void ChangeAdminRequest(String newAdminID) {
//        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
//        final Call<ResponseBody> apiCall = apiInterface.addChangeAdminRequest(
//                masjidModel.getUserId(), masjidModel.getId(), newAdminID);
//        apiCall.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                if (response.isSuccessful()) {
//
//                    try {
//                        String res = response.body().string();
//
//                        try {
//                            JSONObject jsonObject = new JSONObject(res);
//                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
//                                Toast.makeText(context, "Admin changed successfully!", Toast.LENGTH_SHORT).show();
//
//                            } else {
//                                if (jsonObject.getString("message").equalsIgnoreCase("Current User is not a admin of this masjid")) {
//                                    Toast.makeText(context, "You are not admin of this masjid/madrasah!", Toast.LENGTH_SHORT).show();
//                                }
//                            }
//
//                        } catch (IllegalStateException | JsonSyntaxException exception) {
//                            responseFailed = true;
//                        }
//
//                        if (responseFailed) {
//                            Toast.makeText(context, context.getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
//                        }
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                } else {
//                    Toast.makeText(context, context.getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Toast.makeText(context, context.getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    @Override
    public int getItemCount() {
        return mItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView txtFollowerName, txtFollowerUsername, txtFollow;
        protected ImageView imgFollowerIcon;
        protected CardView followerContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            txtFollowerName = itemView.findViewById(R.id.txtFollowerName);
            txtFollowerUsername = itemView.findViewById(R.id.txtFollowerUsername);
            txtFollow = itemView.findViewById(R.id.txtFollow);
            imgFollowerIcon = itemView.findViewById(R.id.imgFollowerIcon);
            followerContainer = itemView.findViewById(R.id.followerContainer);
        }
    }

    private void UserProfileRequest(String userID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> apiCall = apiInterface.getProfileInfo(userID);
        apiCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    try {
                        String res = response.body().string();

                        try {
                            Gson gson = new Gson();
                            Reader reader = new StringReader(res);
                            ProfileModel profileModel = gson.fromJson(reader, ProfileModel.class);

                            if (profileModel.getProfileUserInfo().size() > 0) {
                                ProfileUserInfo profileUserInfo = profileModel.getProfileUserInfo().get(0);

                                final Dialog dialog = new Dialog(context, R.style.DialogTheme);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.getWindow().setBackgroundDrawableResource(R.color.colorBlackCC);
                                dialog.setContentView(R.layout.dialog_profile);
                                dialog.show();

                                CircularImageView profilePhoto = (CircularImageView) dialog.findViewById(R.id.profilePhoto);
                                TextView profileFullname = (TextView) dialog.findViewById(R.id.profileFullname);
                                TextView profileUsername = (TextView) dialog.findViewById(R.id.profileUsername);
                                TextView txtTotalFollowers = (TextView) dialog.findViewById(R.id.txtTotalFollowers);
                                TextView txtTotalFollowings = (TextView) dialog.findViewById(R.id.txtTotalFollowings);
                                TextView txtTotalDonated = (TextView) dialog.findViewById(R.id.txtTotalDonated);
                                TextView txtVerifiedMobile = (TextView) dialog.findViewById(R.id.txtVerifiedMobile);
                                TextView txtVerifiedEmail = (TextView) dialog.findViewById(R.id.txtVerifiedEmail);

                                if (profileUserInfo.getPhoneNumber().length() == 10) {
                                    txtVerifiedMobile.setText("XXXXXX" + profileUserInfo.getPhoneNumber().substring(profileUserInfo.getPhoneNumber().length() - 4));
                                }

                                txtVerifiedEmail.setText(profileUserInfo.getEmail());
                                profileFullname.setText(profileUserInfo.getFullName());

                                if (!profileUserInfo.getUser_avatar().equalsIgnoreCase("")) {
                                    Glide.with(context).load(APIInterface.API_DOMAIN + profileUserInfo.getUser_avatar()).into(profilePhoto);
                                }

                                if (profileUserInfo.getVerify_status().equalsIgnoreCase("1")) {
                                    profileFullname.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_verified, 0);
                                    profileFullname.setCompoundDrawablePadding(5);
                                }

                                if (profileUserInfo.getFollower_count() == null)
                                    txtTotalFollowers.setText(0 + "\n" + "Followers");
                                else
                                    txtTotalFollowers.setText(profileUserInfo.getFollower_count() + "\n" + "Followers");

                                if (profileUserInfo.getFollowing_count() == null)
                                    txtTotalFollowings.setText(0 + "\n" + "Followings");
                                else
                                    txtTotalFollowings.setText(profileUserInfo.getFollowing_count() + "\n" + "Followings");

                                if (profileUserInfo.getUser_donated() == null)
                                    txtTotalDonated.setText(0 + "\n" + "Donated");
                                else
                                    txtTotalDonated.setText(profileUserInfo.getUser_donated() + "\n" + "Donated");

                                if (profileUserInfo.getUsername().equalsIgnoreCase(""))
                                    profileUsername.setText("not found!");
                                else
                                    profileUsername.setText(profileUserInfo.getUsername());
                            }

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            Toast.makeText(context, context.getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(context, context.getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, context.getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
