package com.calltoallah.dawah.adapter;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.calltoallah.dawah.DonationActivity;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.comman.ProgressbarManager;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.firebase.SendNotification;
import com.calltoallah.dawah.fragment.MasjidDetailsFragment;
import com.calltoallah.dawah.fragment.PostFragment;
import com.calltoallah.dawah.fragment.UserVerificationFragment;
import com.calltoallah.dawah.model.HomeMasjidPostList;
import com.calltoallah.dawah.model.MasjidListModel;
import com.calltoallah.dawah.model.MasjidModel;
import com.calltoallah.dawah.model.RecyclerViewItem;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.Reader;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.calltoallah.dawah.network.APIInterface.API_DOMAIN;


public class HomeSelectedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private MainActivity context;
    private List<RecyclerViewItem> mediaObjects;
    private String mSearchText = "";
    private static final int FOOTER_ITEM = 1;

    private static final int REQUEST_PICK_VIDEO = 01;
    private static final int REQUEST_PICK_PHOTO = 13;
    public static ImageView previewMedia;
    private ProgressbarManager progressbarManager;
    private boolean responseFailed = false;
    private MasjidListModel masjidListModels;

    public HomeSelectedAdapter(MainActivity ctx, List<RecyclerViewItem> mediaObjects,
                               MasjidListModel masjidListModels) {
        this.context = ctx;
        this.mediaObjects = mediaObjects;
        this.masjidListModels = masjidListModels;
        progressbarManager = new ProgressbarManager(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View row;

        if (viewType == FOOTER_ITEM) {
            row = inflater.inflate(R.layout.item_home, parent, false);
            return new FooterHolder(row);
        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        RecyclerViewItem recyclerViewItem = mediaObjects.get(position);

        if (holder instanceof FooterHolder) {
            FooterHolder footerHolder = (FooterHolder) holder;
            HomeMasjidPostList homeMasjidPostList = (HomeMasjidPostList) recyclerViewItem;
            //set data
            configurePosts(footerHolder, homeMasjidPostList, position);
        }
    }

    private void configurePosts(FooterHolder holder, HomeMasjidPostList mediaObject, int position) {
        if (mediaObject.getDocPath().equalsIgnoreCase("")) {
            holder.media_container.setVisibility(View.GONE);
            holder.txtPostInfo.setBackgroundResource(R.color.colorPrimary);
            holder.txtPostInfo.setTextColor(Color.WHITE);
            holder.txtPostInfo.setGravity(Gravity.CENTER);

            ViewGroup.LayoutParams params = holder.txtPostInfo.getLayoutParams();
            params.height = 400;
            holder.txtPostInfo.setLayoutParams(params);

        } else {
            holder.media_container.setVisibility(View.VISIBLE);
            holder.txtPostInfo.setBackgroundResource(R.color.colorTransparent);
            holder.txtPostInfo.setTextColor(Color.BLACK);
            holder.txtPostInfo.setGravity(Gravity.LEFT);

            ViewGroup.LayoutParams params = holder.txtPostInfo.getLayoutParams();
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            holder.txtPostInfo.setLayoutParams(params);

            Glide.with(context)
                    .load(API_DOMAIN + mediaObject.getDocPath())
                    .into(holder.thumbnail);
        }

        // For Danation ...
        if (mediaObject.getDonationButton().equalsIgnoreCase("1")) {
            holder.donationContainer.setVisibility(View.VISIBLE);

            int userDonated = Integer.parseInt(mediaObject.getUser_donated());
            int userDonatedAmt = Integer.parseInt(mediaObject.getAsked_donation_amount());

            holder.progressDonation.setMax(userDonatedAmt);
            holder.progressDonation.setProgress(userDonated);
            holder.txtDonationAmt.setText("₹ " + mediaObject.getUser_donated() + " Collected from the target of ₹ " + mediaObject.getAsked_donation_amount());

            if (userDonated >= userDonatedAmt) {
                holder.txtDonate.setText("Donated");
                holder.txtDonate.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorGray)));
                holder.txtDonate.setEnabled(false);
            } else {
                holder.txtDonate.setEnabled(true);
            }

        } else {
            holder.donationContainer.setVisibility(View.GONE);
        }

        holder.txtLikeCount.setText(prettyCount(Integer.parseInt(mediaObject.getLike_count())));
        holder.txtShareCount.setText(prettyCount(Integer.parseInt(mediaObject.getShare_count())));

        if (mSearchText != null && !mSearchText.isEmpty()) {
            // Masjid name ...
            int startName = mediaObject.getMasjidName().toLowerCase().indexOf(mSearchText.toLowerCase());
            int endName = startName + mSearchText.length();

            if (startName != -1) {
                Spannable spannable = new SpannableString(mediaObject.getMasjidName());
                spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#A9A9A9")), startName, endName, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.txtMasjidName.setText(spannable);
            } else {
                holder.txtMasjidName.setText(mediaObject.getMasjidName());
            }

            // Post Text  ...
            int startText = mediaObject.getPostText().toLowerCase().indexOf(mSearchText.toLowerCase());
            int endText = startText + mSearchText.length();

            if (startText != -1) {
                Spannable spannable = new SpannableString(mediaObject.getPostText());
                spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#A9A9A9")), startText, endText, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.txtPostInfo.setText(spannable);
            } else {
                holder.txtPostInfo.setText(mediaObject.getPostText());
            }

        } else {
            holder.txtMasjidName.setText(mediaObject.getMasjidName());
            holder.txtPostInfo.setText(mediaObject.getPostText());
        }

        if (mediaObject.getVerified_status().equalsIgnoreCase("1")) {
            holder.txtMasjidName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_verified, 0);
            holder.txtMasjidName.setCompoundDrawablePadding(5);
        }

        if (mediaObject.getCreatedOn() != null) {
            String inputPattern = "yyyy-MM-dd HH:mm:ss";
            String outputPattern = "EEEE, dd MMMM, yyyy";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

            try {
                Date date = inputFormat.parse(mediaObject.getCreatedOn());
                String createdDate = outputFormat.format(date);
                holder.txtCreatedDate.setText(createdDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        Glide.with(context)
                .load(API_DOMAIN + mediaObject.getMasjidImage())
                .into(holder.imgMasjidIcon);

        holder.masjidContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMasjidDetails(mediaObject.getMasjidId());
            }
        });

        holder.txtPostInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.MODE, 15);
                bundle.putParcelable(Constants.DATA, mediaObject);

                PostFragment postFragment = new PostFragment();
                postFragment.setArguments(bundle);
                context.loadFragment(postFragment);
            }
        });

        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.MODE, 15);
                bundle.putParcelable(Constants.DATA, mediaObject);

                PostFragment postFragment = new PostFragment();
                postFragment.setArguments(bundle);
                context.loadFragment(postFragment);
            }
        });

        if (mediaObject.isPost_liked() == true) {
            holder.postLike.setImageResource(R.drawable.ic_action_like);
        } else {
            holder.postLike.setImageResource(R.drawable.ic_action_unlike);
        }

        holder.postLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                final Call<String> loginCall = apiInterface.getPostsLikeUnlike(mediaObject.getMasjidId(),
                        mediaObject.getId(), UserPreferences.loadUser(context).getId());
                loginCall.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {

                            try {
                                JSONObject jsonObject = new JSONObject(response.body().toString());
                                if (jsonObject.getString("message").equalsIgnoreCase("Post liked")) {
                                    int likeCount = Integer.parseInt(mediaObject.getLike_count()) + 1;
                                    mediaObject.setLike_count(String.valueOf(likeCount));
                                    mediaObject.setPost_liked(true);

                                    SendNotification.getUserNotificationToken(mediaObject.getUser_id(),
                                            context.getString(R.string.app_name),
                                            UserPreferences.loadUser(context).getFullName() + " Liked your Post", context);

                                } else {
                                    int likeCount = Integer.parseInt(mediaObject.getLike_count()) - 1;
                                    mediaObject.setLike_count(String.valueOf(likeCount));
                                    mediaObject.setPost_liked(false);
                                }
                                ;

                                notifyItemChanged(position);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                    }
                });
            }
        });

        holder.postShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                final Call<String> loginCall = apiInterface.getPostsShare(
                        mediaObject.getMasjidId(), mediaObject.getId(),
                        UserPreferences.loadUser(context).getId());
                loginCall.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {
                            Log.d("SHARE: ", response.body().toString());

                            try {
                                JSONObject jsonObject = new JSONObject(response.body().toString());
                                String message = jsonObject.getString("message");

                                String shareBody = mediaObject.getMasjidName() + "\n\n" +
                                        "Get this masjid/madrasah post from below link:" + "\n" +
                                        mediaObject.getQr_code() + "/post/" + mediaObject.getId();
                                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                                sharingIntent.setType("text/plain");
                                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name));
                                context.startActivity(Intent.createChooser(sharingIntent, context.getResources().getString(R.string.app_name)));

                                int shareCount = Integer.parseInt(mediaObject.getShare_count()) + 1;
                                mediaObject.setShare_count(String.valueOf(shareCount));

                                SendNotification.getUserNotificationToken(mediaObject.getUser_id(),
                                        context.getString(R.string.app_name),
                                        UserPreferences.loadUser(context).getFullName() + " Shared your Post", context);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            notifyItemChanged(position);
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                    }
                });
            }
        });

        holder.txtDonate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (UserPreferences.loadUser(context).getVerify_status().equalsIgnoreCase("1")) {
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.MODE, 0);
                    bundle.putParcelable(Constants.DATA, mediaObject);

                    Intent intent = new Intent(context, DonationActivity.class);
                    intent.putExtras(bundle);
                    context.startActivity(intent);

                } else {
                    context.loadFragment(new UserVerificationFragment());
                }
            }
        });

        if (UserPreferences.loadUser(context).getId().equalsIgnoreCase(masjidListModels.getUserId())) {
            holder.imgPostUpdate.setVisibility(View.VISIBLE);
        } else {
            holder.imgPostUpdate.setVisibility(View.GONE);
        }

        holder.imgPostUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View parentView = context.getLayoutInflater().inflate(R.layout.dialog_posts, null);
                BottomSheetDialog dialog = new BottomSheetDialog(context);
                dialog.setContentView(parentView);

                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        BottomSheetDialog dialogc = (BottomSheetDialog) dialog;
                        FrameLayout bottomSheet = dialogc.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                        BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
                    }
                });

                dialog.show();

                previewMedia = (ImageView) dialog.findViewById(R.id.previewMedia);
                ImageView imgPMasjidLogo = (ImageView) dialog.findViewById(R.id.imgPMasjidLogo);
                TextView imgPMasjidName = (TextView) dialog.findViewById(R.id.imgPMasjidName);
                TextView imgPMasjidUsername = (TextView) dialog.findViewById(R.id.imgPMasjidUsername);
                LinearLayout donationContainer = (LinearLayout) dialog.findViewById(R.id.donationContainer);
                Button btnPOST = (Button) dialog.findViewById(R.id.btnPOST);
                EditText edtMessage = (EditText) dialog.findViewById(R.id.edtMessage);
                EditText editGoalAmt = (EditText) dialog.findViewById(R.id.editGoalAmt);

                if (UserPreferences.loadUser(context).getVerify_status().equalsIgnoreCase("1") &&
                        UserPreferences.loadUser(context).getStatus().equalsIgnoreCase("1")) {
                    donationContainer.setVisibility(View.VISIBLE);
                } else if (UserPreferences.loadUser(context).getVerify_status().equalsIgnoreCase("0") &&
                        UserPreferences.loadUser(context).getStatus().equalsIgnoreCase("1")) {
                    donationContainer.setVisibility(View.GONE);
                }

                imgPMasjidName.setText(mediaObject.getMasjidName() + "");
                imgPMasjidUsername.setText(mediaObject.getMasjid_username() + "");

                RequestOptions options = new RequestOptions()
                        .placeholder(R.drawable.ic_action_camera)
                        .error(R.drawable.ic_action_camera);

                Glide.with(context)
                        .load(API_DOMAIN + mediaObject.getMasjidImage())
                        .apply(options)
                        .into(imgPMasjidLogo);

                edtMessage.setText(mediaObject.getPostText());
                editGoalAmt.setText(mediaObject.getAsked_donation_amount());
                Glide.with(context)
                        .load(API_DOMAIN + mediaObject.getDocPath())
                        .apply(options)
                        .into(previewMedia);

                btnPOST.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edtMessage.getText().toString().equalsIgnoreCase("")) {
                            Toast.makeText(context, "Message should not be blank!", Toast.LENGTH_SHORT).show();
                        } else {
                            if (MainActivity.getInstance().imagePath != null || MainActivity.getInstance().videoPath != null)
                                addPostImageVideo(mediaObject.getId(), edtMessage.getText().toString(), editGoalAmt.getText().toString(), dialog);
                            else
                                addPost("", mediaObject.getId(), edtMessage.getText().toString(), editGoalAmt.getText().toString(), dialog);
                        }

                    }
                });

                previewMedia.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Choose your photo/video");
                        String[] images = {"Take From Gallery", "Take Video From Gallery"};
                        builder.setItems(images, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                        context.startActivityForResult(galleryIntent, REQUEST_PICK_PHOTO);
                                        break;

                                    case 1:
                                        Intent pickVideoIntent = new Intent(Intent.ACTION_PICK,
                                                MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                                        context.startActivityForResult(pickVideoIntent, REQUEST_PICK_VIDEO);
                                        break;
                                }
                            }
                        });

                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                });
            }
        });
    }

    private void addPostImageVideo(String postID, String message, String goalAmt, BottomSheetDialog dialog) {

        MultipartBody.Part image_path = null;
        if (MainActivity.getInstance().imagePath != null) {
            File file = new File(MainActivity.getInstance().imagePath);
            RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            image_path = MultipartBody.Part.createFormData("file_name", file.getName(), mFile);
        }

        if (MainActivity.getInstance().videoPath != null) {
            File file = new File(MainActivity.getInstance().videoPath);
            RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            image_path = MultipartBody.Part.createFormData("file_name", file.getName(), mFile);
        }

        RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), UserPreferences.loadUser(context).getId());

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<String> callAPI;

        callAPI = apiInterface.addMasjidPostFile(
                user_id, image_path);
        callAPI.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                progressbarManager.dismiss();
                dialog.dismiss();

                try {
                    String res = response.body().toString();
                    Log.d("UPLOAD: ", res + "");

                    try {

                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getInt("status") == 1) {
                            addPost(jsonObject.getString("file_name"), postID, message, goalAmt, dialog);
                        } else {
                            Toast.makeText(context, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (IllegalStateException | JsonSyntaxException exception) {
                        responseFailed = true;
                    }

                    if (responseFailed) {
                        progressbarManager.dismiss();
                        Toast.makeText(context, context.getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, "" + t.getMessage().toString(), Toast.LENGTH_SHORT).show();
                progressbarManager.dismiss();
            }
        });
    }

    private void addPost(String docPath, String postID, String message, String goalAmt, BottomSheetDialog dialog) {

        String donation = "0";
        if (goalAmt.equalsIgnoreCase("")) {
            donation = "0";
        } else {
            donation = "1";
        }

        String postType = "0";
        if (MainActivity.getInstance().imagePath != null) {
            postType = "1";
        } else if (MainActivity.getInstance().videoPath != null) {
            postType = "2";
        }

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<String> callAPI;

        callAPI = apiInterface.updateMasjidPost(postID, message,
                postType, docPath, donation, goalAmt);
        callAPI.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                progressbarManager.dismiss();
                dialog.dismiss();

                try {
                    String res = response.body().toString();
                    Log.d("UPDATE_POST: ", res + "");

                    try {

                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getInt("status") == 1) {
                            Toast.makeText(context, "Post has been updated successfully!", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();

                        } else {
                            Toast.makeText(context, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (IllegalStateException | JsonSyntaxException exception) {
                        responseFailed = true;
                    }

                    if (responseFailed) {
                        progressbarManager.dismiss();
                        Toast.makeText(context, context.getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, "" + t.getMessage().toString(), Toast.LENGTH_SHORT).show();
                progressbarManager.dismiss();
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        RecyclerViewItem recyclerViewItem = mediaObjects.get(position);

        if (recyclerViewItem instanceof HomeMasjidPostList)
            return FOOTER_ITEM;
        else
            return super.getItemViewType(position);

    }

    public class FooterHolder extends RecyclerView.ViewHolder {

        // Post ...
        private FrameLayout media_container;
        private LinearLayout donationContainer, masjidContainer;
        public ProgressBar progressDonation;
        private TextView txtPostInfo, txtMasjidName, txtCreatedDate, txtDonate, txtDonationAmt, txtLikeCount, txtShareCount;
        private ImageView thumbnail, postLike, postShare, imgMasjidIcon, imgPostUpdate;

        FooterHolder(View itemView) {
            super(itemView);
            thumbnail = itemView.findViewById(R.id.thumbnail);
            media_container = itemView.findViewById(R.id.media_container);
            masjidContainer = itemView.findViewById(R.id.masjidContainer);
            txtLikeCount = (TextView) itemView.findViewById(R.id.txtLikeCount);
            txtShareCount = (TextView) itemView.findViewById(R.id.txtShareCount);

            // Post ...
            txtPostInfo = itemView.findViewById(R.id.txtPostInfo);
            txtMasjidName = itemView.findViewById(R.id.txtMasjidName);
            txtDonate = itemView.findViewById(R.id.txtDonate);
            txtCreatedDate = itemView.findViewById(R.id.txtCreatedDate);
            postLike = itemView.findViewById(R.id.postLike);
            postShare = itemView.findViewById(R.id.postShare);
            imgMasjidIcon = itemView.findViewById(R.id.imgMasjidIcon);
            txtDonationAmt = itemView.findViewById(R.id.txtDonationAmt);
            donationContainer = itemView.findViewById(R.id.donationContainer);
            imgPostUpdate = itemView.findViewById(R.id.imgPostUpdate);

            progressDonation = (ProgressBar) itemView.findViewById(R.id.progressDonation);
            txtDonationAmt.setSelected(true);
        }
    }

    @Override
    public int getItemCount() {
        return mediaObjects.size();
    }

    public String prettyCount(long numValue) {
        char[] suffix = {' ', 'K', 'M', 'B', 'T', 'P', 'E'};
        int value = (int) Math.floor(Math.log10(numValue));
        int base = value / 3;
        if (value >= 3 && base < suffix.length) {
            return new DecimalFormat("#0.0").format(numValue / Math.pow(10, base * 3)) + suffix[base];
        } else {
            return new DecimalFormat("#,##0").format(numValue);
        }
    }

    private void getMasjidDetails(String masjidID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> loginCall = apiInterface.getMasjidDetails(masjidID);
        loginCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (response.isSuccessful()) {
                    Log.d("MASJID_DETAIL: ", response.body().toString());

                    try {
                        Gson gson = new Gson();
                        Reader reader = new StringReader(response.body().toString());
                        final MasjidModel masjidModel = gson.fromJson(reader, MasjidModel.class);

                        if (masjidModel.getMasjidListModel().size() > 0) {
                            Bundle bundle = new Bundle();
                            bundle.putInt(Constants.MODE, 0);
                            bundle.putParcelable(Constants.DATA, masjidModel.getMasjidListModel().get(0));

                            MasjidDetailsFragment masjidDetailsFragment = new MasjidDetailsFragment();
                            masjidDetailsFragment.setArguments(bundle);
                            context.loadFragment(masjidDetailsFragment);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }
}