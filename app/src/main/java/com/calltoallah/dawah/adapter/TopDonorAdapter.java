package com.calltoallah.dawah.adapter;

import android.app.Dialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.CircularImageView;
import com.calltoallah.dawah.model.DonorListModel;
import com.calltoallah.dawah.model.ProfileModel;
import com.calltoallah.dawah.model.ProfileUserInfo;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.calltoallah.dawah.network.APIInterface.API_DOMAIN;

public class TopDonorAdapter extends RecyclerView.Adapter {

    private MainActivity context;
    private List<DonorListModel> mItem;
    private boolean responseFailed = false;

    public TopDonorAdapter(MainActivity context, List<DonorListModel> items) {
        this.context = context;
        this.mItem = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_donor, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (!mItem.isEmpty() && mItem != null) {
            final ViewHolder viewHolders = (ViewHolder) viewHolder;

            final DonorListModel donorListModel = mItem.get(position);
            viewHolders.txtRank.setText(position + 4 + "");
            viewHolders.txtDonorName.setText(donorListModel.getFullName());
            viewHolders.txtDonorAmt.setText("₹ " + donorListModel.getDonationAmount() + "");

            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.no_profile_image)
                    .error(R.drawable.no_profile_image);

            Glide.with(context)
                    .load(API_DOMAIN + donorListModel.getUser_avatar())
                    .apply(options)
                    .into(viewHolders.imgDonor);

            viewHolders.imgDonor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserProfileRequest(donorListModel.getId());
                }
            });
        }
    }

    private void UserProfileRequest(String userID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> apiCall = apiInterface.getProfileInfo(userID);
        apiCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    try {
                        String res = response.body().string();
                        Log.d("Profile: ", res);

                        try {
                            Gson gson = new Gson();
                            Reader reader = new StringReader(res);
                            ProfileModel profileModel = gson.fromJson(reader, ProfileModel.class);

                            if (profileModel.getProfileUserInfo().size() > 0) {
                                ProfileUserInfo profileUserInfo = profileModel.getProfileUserInfo().get(0);

                                final Dialog dialog = new Dialog(context, R.style.DialogTheme);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.getWindow().setBackgroundDrawableResource(R.color.colorBlackCC);
                                dialog.setContentView(R.layout.dialog_profile);
                                dialog.show();

                                CircularImageView profilePhoto = (CircularImageView) dialog.findViewById(R.id.profilePhoto);
                                TextView profileFullname = (TextView) dialog.findViewById(R.id.profileFullname);
                                TextView profileUsername = (TextView) dialog.findViewById(R.id.profileUsername);
                                TextView txtTotalFollowers = (TextView) dialog.findViewById(R.id.txtTotalFollowers);
                                TextView txtTotalFollowings = (TextView) dialog.findViewById(R.id.txtTotalFollowings);
                                TextView txtTotalDonated = (TextView) dialog.findViewById(R.id.txtTotalDonated);
                                TextView txtVerifiedMobile = (TextView) dialog.findViewById(R.id.txtVerifiedMobile);
                                TextView txtVerifiedEmail = (TextView) dialog.findViewById(R.id.txtVerifiedEmail);

                                if (profileUserInfo.getPhoneNumber().length() == 10) {
                                    txtVerifiedMobile.setText("XXXXXX" + profileUserInfo.getPhoneNumber().substring(profileUserInfo.getPhoneNumber().length() - 4));
                                }

                                txtVerifiedEmail.setText(profileUserInfo.getEmail());
                                profileFullname.setText(profileUserInfo.getFullName());

                                if (!profileUserInfo.getUser_avatar().equalsIgnoreCase("")) {
                                    Glide.with(context).load(APIInterface.API_DOMAIN + profileUserInfo.getUser_avatar()).into(profilePhoto);
                                }

                                if (profileUserInfo.getVerify_status().equalsIgnoreCase("1")) {
                                    profileFullname.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_verified, 0);
                                    profileFullname.setCompoundDrawablePadding(5);
                                }

                                if (profileUserInfo.getFollower_count() == null)
                                    txtTotalFollowers.setText("₹ " + 0 + "\n" + "Followers");
                                else
                                    txtTotalFollowers.setText("₹ " + profileUserInfo.getFollower_count() + "\n" + "Followers");

                                if (profileUserInfo.getFollowing_count() == null)
                                    txtTotalFollowings.setText("₹ " + 0 + "\n" + "Followings");
                                else
                                    txtTotalFollowings.setText("₹ " + profileUserInfo.getFollowing_count() + "\n" + "Followings");

                                if (profileUserInfo.getUser_donated() == null)
                                    txtTotalDonated.setText("₹ " + 0 + "\n" + "Donated");
                                else
                                    txtTotalDonated.setText("₹ " + profileUserInfo.getUser_donated() + "\n" + "Donated");

                                if (profileUserInfo.getUsername().equalsIgnoreCase(""))
                                    profileUsername.setText("not found!");
                                else
                                    profileUsername.setText(profileUserInfo.getUsername());
                            }

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            Toast.makeText(context, context.getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(context, context.getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, context.getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView txtRank, txtDonorName, txtDonorAmt;
        protected ImageView imgDonor;

        public ViewHolder(View itemView) {
            super(itemView);
            txtRank = itemView.findViewById(R.id.txtRank);
            txtDonorName = itemView.findViewById(R.id.txtDonorName);
            txtDonorAmt = itemView.findViewById(R.id.txtDonorAmt);
            imgDonor = itemView.findViewById(R.id.imgDonor);
        }
    }
}
