package com.calltoallah.dawah;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.calltoallah.dawah.calendar.MenuAdapter;
import com.calltoallah.dawah.comman.App;
import com.calltoallah.dawah.comman.ConnectivityReceiver;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.firebase.Token;
import com.calltoallah.dawah.fragment.AddMadrasahFragment;
import com.calltoallah.dawah.fragment.AddMasjidFragment;
import com.calltoallah.dawah.fragment.ApprovalFragment;
import com.calltoallah.dawah.fragment.BarcodeFragment;
import com.calltoallah.dawah.fragment.BaseFragment;
import com.calltoallah.dawah.fragment.CalendarFragment;
import com.calltoallah.dawah.fragment.ChangeEmailFragment;
import com.calltoallah.dawah.fragment.ChangePasswordFragment;
import com.calltoallah.dawah.fragment.ChangePhoneFragment;
import com.calltoallah.dawah.fragment.ChangeUsernameFragment;
import com.calltoallah.dawah.fragment.CompassFragment;
import com.calltoallah.dawah.fragment.DocumentFragment;
import com.calltoallah.dawah.fragment.HomeFragment;
import com.calltoallah.dawah.fragment.MasjidDetailsFragment;
import com.calltoallah.dawah.fragment.MasjidFollowersFragment;
import com.calltoallah.dawah.fragment.MasjidFragment;
import com.calltoallah.dawah.fragment.PostFragment;
import com.calltoallah.dawah.fragment.ProfileEditFragment;
import com.calltoallah.dawah.fragment.ProfileFragment;
import com.calltoallah.dawah.fragment.QuranFragment;
import com.calltoallah.dawah.fragment.ReferFragment;
import com.calltoallah.dawah.fragment.ReportFragment;
import com.calltoallah.dawah.fragment.TasbihCalcFragment;
import com.calltoallah.dawah.fragment.TimeTableFragment;
import com.calltoallah.dawah.fragment.TopDonorFragment;
import com.calltoallah.dawah.fragment.TopFundRaiserFragment;
import com.calltoallah.dawah.fragment.UserFollowersFragment;
import com.calltoallah.dawah.fragment.UserFollowingsFragment;
import com.calltoallah.dawah.fragment.UserVerificationFragment;
import com.calltoallah.dawah.fragment.WalletFragment;
import com.calltoallah.dawah.fragment.WebFragment;
import com.calltoallah.dawah.fragment.ZakatCalcFragment;
import com.calltoallah.dawah.model.MasjidListModel;
import com.calltoallah.dawah.model.MenuModel;
import com.calltoallah.dawah.model.ProfileModel;
import com.calltoallah.dawah.model.ProfileUserInfo;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.calltoallah.dawah.notification.AlarmService;
import com.calltoallah.dawah.notification.ExceptionHandler;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.calltoallah.dawah.adapter.HomeSelectedAdapter.previewMedia;

public class MainActivity extends MasterActivity implements
        ActivityCompat.OnRequestPermissionsResultCallback, BottomNavigationView.OnNavigationItemSelectedListener, ConnectivityReceiver.ConnectivityReceiverListener {
    public static TextView mToolbarTitle;
    public static Toolbar mToolbar;
    public static BottomNavigationView mBottomNavigationView;
    public static BottomSheetDialog dialog;
    private boolean responseFailed = false;
    private Bundle bundle;
    private int mode;
    private MasjidListModel masjidListModel;
    private String userIdentifier;
    private static final int REQUEST_PICK_VIDEO = 01;
    private static final int REQUEST_PICK_PHOTO = 13;
    private static MainActivity instance = null;
    public static String videoPath, imagePath;

    public static MainActivity getInstance() {
        return instance;
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance = this;

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        Constants.sContext = this;

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(MainActivity.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String fcmToken = instanceIdResult.getToken();

                Token token1 = new Token(fcmToken);
                FirebaseDatabase.getInstance().getReference("Tokens").child(UserPreferences.loadUser(getApplicationContext()).getId()).setValue(token1);
                Log.d("Firebase reg id. ", "Firebase reg id: " + fcmToken);
            }
        });

//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        mBottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigationView);

        if (AlarmService.isInstanceCreated()) {
            stopService(new Intent(this, AlarmService.class));
        } else {
            startService(new Intent(this, AlarmService.class));
        }

        // Getting User Profile ...
        UserProfileRequest();

        if (savedInstanceState == null) {
            loadFragment(new HomeFragment());
        }

        bundle = getIntent().getExtras();
        if (bundle != null) {
            mode = bundle.getInt(Constants.MODE);
            if (mode == 13) {

                MasjidDetailsFragment masjidDetailsFragment = new MasjidDetailsFragment();
                masjidDetailsFragment.setArguments(bundle);
                loadFragment(masjidDetailsFragment);

            } else if (mode == 14) {
                PostFragment postFragment = new PostFragment();
                postFragment.setArguments(bundle);
                loadFragment(postFragment);
            }
        }

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mBottomNavigationView.setSelectedItemId(R.id.nav_item_1);
        mBottomNavigationView.setOnNavigationItemSelectedListener(this);

        getSupportFragmentManager().addOnBackStackChangedListener(
                new FragmentManager.OnBackStackChangedListener() {

                    @Override
                    public void onBackStackChanged() {
                        Fragment f = getSupportFragmentManager()
                                .findFragmentById(R.id.container);

                        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                            finish();
                        } else {
                            if (f != null) {
                                updateToolbarTitle(f);
                            }
                        }

                    }
                });

    }


    private void UserProfileRequest() {
        Log.d("UserID: ", UserPreferences.loadUser(MainActivity.this).getId() + "");
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> apiCall = apiInterface.getProfileInfo(UserPreferences.loadUser(MainActivity.this).getId());
        apiCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    try {
                        String res = response.body().string();
                        Log.d("Profile: ", res);

                        try {
                            Gson gson = new Gson();
                            Reader reader = new StringReader(res);
                            ProfileModel profileModel = gson.fromJson(reader, ProfileModel.class);

                            if (profileModel.getProfileUserInfo().size() > 0) {
                                ProfileUserInfo profileUserInfo = profileModel.getProfileUserInfo().get(0);
                                UserPreferences.clearUser(MainActivity.this);
                                UserPreferences.saveUser(MainActivity.this, profileUserInfo);
                            }

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            Toast.makeText(MainActivity.this, getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(MainActivity.this, getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(MainActivity.this, getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void updateToolbarTitle(Fragment fragment) {
        String fragClassName = fragment.getClass().getName();

        if (fragClassName.equals(HomeFragment.class.getName())) {
            mToolbarTitle.setText(getString(R.string.app_name));
            toggleUpdate(true);
            toggleToolbar(true);

        } else if (fragClassName.equals(ProfileFragment.class.getName())) {
            mToolbarTitle.setText(getString(R.string.nav_item_3));
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(CompassFragment.class.getName())) {
            mToolbarTitle.setText(getString(R.string.nav_item_5));
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(TasbihCalcFragment.class.getName())) {
            mToolbarTitle.setText(getString(R.string.nav_item_6));
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(ZakatCalcFragment.class.getName())) {
            mToolbarTitle.setText(getString(R.string.nav_item_7));
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(CalendarFragment.class.getName())) {
            mToolbarTitle.setText(getString(R.string.nav_item_8));
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(QuranFragment.class.getName())) {
            mToolbarTitle.setText(getString(R.string.nav_item_9));
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(ReportFragment.class.getName())) {
            mToolbarTitle.setText(getString(R.string.nav_item_13));
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(MasjidFragment.class.getName())) {
            mToolbarTitle.setText(getString(R.string.nav_item_2));
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(AddMasjidFragment.class.getName())) {
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(AddMadrasahFragment.class.getName())) {
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(TimeTableFragment.class.getName())) {
            mToolbarTitle.setText(getString(R.string.timetable));
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(TopFundRaiserFragment.class.getName())) {
            mToolbarTitle.setText(getString(R.string.nav_item_10));
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(TopDonorFragment.class.getName())) {
            mToolbarTitle.setText("Top Donor");
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(WalletFragment.class.getName())) {
            mToolbarTitle.setText("My Wallet");
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(ApprovalFragment.class.getName())) {
            mToolbarTitle.setText("Pending for Approval");
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(ReferFragment.class.getName())) {
            mToolbarTitle.setText("Refer & Earn");
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(UserVerificationFragment.class.getName())) {
            mToolbarTitle.setText("User Verification");
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(BarcodeFragment.class.getName())) {
            mToolbarTitle.setText("QR Scan");
            toggleUpdate(false);
            toggleToolbar(false);

        } else if (fragClassName.equals(MasjidDetailsFragment.class.getName())) {
            mToolbarTitle.setText("Masjid");
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(ProfileEditFragment.class.getName())) {
            mToolbarTitle.setText("Change Profile");
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(ChangePasswordFragment.class.getName())) {
            mToolbarTitle.setText("Change Password");
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(ChangeEmailFragment.class.getName())) {
            mToolbarTitle.setText("Change Email");
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(ChangePhoneFragment.class.getName())) {
            mToolbarTitle.setText("Change Mobile");
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(ChangeUsernameFragment.class.getName())) {
            mToolbarTitle.setText("Change Username");
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(MasjidFollowersFragment.class.getName())) {
            mToolbarTitle.setText("My Followers");
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(UserFollowersFragment.class.getName())) {
            mToolbarTitle.setText("My Followers");
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(UserFollowingsFragment.class.getName())) {
            mToolbarTitle.setText("My Followings");
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(DocumentFragment.class.getName())) {
            mToolbarTitle.setText("My Documents");
            toggleUpdate(false);
            toggleToolbar(true);

        } else if (fragClassName.equals(WebFragment.class.getName())) {
            toggleUpdate(false);
            toggleToolbar(true);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void toggleUpdate(boolean update) {
        if (update) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//            mToolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
//            mToolbarTitle.setTextColor(getResources().getColor(R.color.colorWhite));

        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_back);
//            mToolbar.setBackgroundColor(getResources().getColor(R.color.colorBackground));
//            getWindow().setStatusBarColor(getResources().getColor(R.color.colorBackground));
//            mToolbarTitle.setTextColor(getResources().getColor(R.color.colorPrimary));

            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBack();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            });

            HomeFragment.getInstance().onDestroyView();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void toggleToolbar(boolean update) {
        if (update) {
            mToolbar.setVisibility(View.VISIBLE);
            mBottomNavigationView.setVisibility(View.VISIBLE);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimaryDark));

        } else {
            mToolbar.setVisibility(View.GONE);
            mBottomNavigationView.setVisibility(View.GONE);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorBlack));
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorBlack));
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return onBack();
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onBack() {
        try {
            FragmentManager fm = getSupportFragmentManager();
            BaseFragment fragment = (BaseFragment) fm.findFragmentById(R.id.container);
            fragment.onBack();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @SuppressLint("WrongConstant")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public String getSnackBar(String msg) {
        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.container), msg, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        sbView.setBackgroundColor(getResources().getColor(R.color.colorError));
        TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setGravity(Gravity.CENTER_HORIZONTAL);
        textView.setTextAlignment(Gravity.CENTER_HORIZONTAL);
        textView.setTextColor(getResources().getColor(R.color.colorWhite));
        snackbar.show();
        return msg;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.nav_item_1:
                fragment = new HomeFragment();
                break;

            case R.id.nav_item_2:
                fragment = new MasjidFragment();
                break;

            case R.id.nav_item_3:
                fragment = new BarcodeFragment();
                break;

            case R.id.nav_item_4:
                fragment = new ProfileFragment();
                break;

            case R.id.nav_item_5:
                View parentView = getLayoutInflater().inflate(R.layout.dialog_list, null);
                dialog = new BottomSheetDialog(MainActivity.this);
                dialog.setContentView(parentView);
                dialog.show();

                RecyclerView rcvMenuList = (RecyclerView) dialog.findViewById(R.id.menuList);
                GridLayoutManager lm = new GridLayoutManager(MainActivity.this, 3);
                lm.setOrientation(LinearLayoutManager.VERTICAL);
                rcvMenuList.setLayoutManager(lm);
                rcvMenuList.setHasFixedSize(true);

                final List<MenuModel> menuList = new ArrayList<>();

//                if (loadUser(MainActivity.this).getId().equalsIgnoreCase("1")) {
//                    MenuModel menuModel1 = new MenuModel("1", getString(R.string.nav_item_5), R.drawable.ic_action_approve);
//                    menuList.add(menuModel1);
//                }

                MenuModel menuModel1 = new MenuModel("1", getString(R.string.nav_item_5), R.drawable.ic_action_qibla);
                MenuModel menuModel2 = new MenuModel("2", getString(R.string.nav_item_6), R.drawable.ic_action_calculator);
                MenuModel menuModel3 = new MenuModel("3", getString(R.string.nav_item_7), R.drawable.ic_action_calculator);
                MenuModel menuModel4 = new MenuModel("4", getString(R.string.nav_item_8), R.drawable.ic_action_icalendar);
                MenuModel menuModel5 = new MenuModel("5", getString(R.string.nav_item_9), R.drawable.ic_action_quran);
                MenuModel menuModel6 = new MenuModel("6", getString(R.string.nav_item_10), R.drawable.ic_action_donor);

                menuList.add(menuModel1);
                menuList.add(menuModel2);
                menuList.add(menuModel3);
                menuList.add(menuModel4);
                menuList.add(menuModel5);
                menuList.add(menuModel6);

                MenuAdapter adapter = new MenuAdapter(MainActivity.this, menuList);
                rcvMenuList.setAdapter(adapter);

                adapter.setOnItemClickListener(new MenuAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (menuList.get(position).getMenuID().equalsIgnoreCase("1")) {
                            loadFragment(new CompassFragment());

                        } else if (menuList.get(position).getMenuID().equalsIgnoreCase("2")) {
                            loadFragment(new TasbihCalcFragment());

                        } else if (menuList.get(position).getMenuID().equalsIgnoreCase("3")) {
                            loadFragment(new ZakatCalcFragment());

                        } else if (menuList.get(position).getMenuID().equalsIgnoreCase("4")) {
                            loadFragment(new CalendarFragment());

                        } else if (menuList.get(position).getMenuID().equalsIgnoreCase("5")) {
                            loadFragment(new QuranFragment());

                        } else if (menuList.get(position).getMenuID().equalsIgnoreCase("6")) {
                            loadFragment(new TopFundRaiserFragment());
                        }

                        dialog.dismiss();
                    }
                });

                break;
        }

        return loadFragment(fragment);
    }

    public boolean loadFragment(final Fragment fragment) {
        if (fragment != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    FragmentTransaction transaction = getSupportFragmentManager()
                            .beginTransaction();
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    transaction.add(R.id.container, fragment);
                    transaction.addToBackStack(fragment.getClass().getName());
                    transaction.commitAllowingStateLoss();
                }
            }, 200);

            return true;
        }
        return false;
    }

    public void removeFragment(final Fragment fragments) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                FragmentTransaction transaction = getSupportFragmentManager()
                        .beginTransaction();
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                transaction.remove(fragments);
                transaction.addToBackStack(fragments.getClass().getName());
                transaction.commitAllowingStateLoss();
            }
        }, 200);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        System.gc();
        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_PICK_VIDEO) {
                if (data != null) {
                    Uri selectedVideo = data.getData();
                    videoPath = getPath(selectedVideo);
//                    previewMedia.setImageBitmap(ThumbnailUtils.createVideoThumbnail(
//                            videoPath, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND));

                    Glide.with(MainActivity.this)
                            .load(ThumbnailUtils.createVideoThumbnail(
                                    videoPath, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND))
                            .into(previewMedia);
                }
            }

            if (requestCode == REQUEST_PICK_PHOTO) {
                if (data != null) {
                    // Get the Image from data
                    Uri selectedImage = data.getData();

                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    assert cursor != null;
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imagePath = cursor.getString(columnIndex);
//                    previewMedia.setImageBitmap(BitmapFactory.decodeFile(imagePath));

                    Glide.with(MainActivity.this)
                            .load(BitmapFactory.decodeFile(imagePath))
                            .into(previewMedia);

                    cursor.close();
                }
            }

        } else if (resultCode != RESULT_CANCELED) {
            getSnackBar("Sorry, there was an error!");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        ConnectivityReceiver connectivityReceiver = new ConnectivityReceiver();
        registerReceiver(connectivityReceiver, intentFilter);
        App.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        final Dialog dialog = new Dialog(MainActivity.this, R.style.FullScreenDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_nointernet);
        dialog.setCancelable(false);

        if (isConnected) {
            Toast.makeText(this, "Connected to internet", Toast.LENGTH_SHORT).show();

        } else {
            Toast.makeText(this, "Waiting for internet connection", Toast.LENGTH_SHORT).show();
            dialog.show();

            Button btnTryAgain = (Button) dialog.findViewById(R.id.btnTryAgain);
            btnTryAgain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ConnectivityReceiver.isConnected())
                        dialog.dismiss();
                    else
                        Toast.makeText(MainActivity.this, "Waiting for internet connection", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
