package com.calltoallah.dawah;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.calltoallah.dawah.comman.ProgressbarManager;
import com.calltoallah.dawah.model.RegisterModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends MasterActivity {

    private Button btnSignUp;
    private EditText edtFullname, edtPhoneNumber, edtEmailAddress, edtUsername, edtPassword, edtConfPassword, edtReferalCode;
    private boolean responseFailed = false;
    private ProgressbarManager progressbarManager;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        progressbarManager = new ProgressbarManager(RegisterActivity.this);

        Init();
        setData();
    }

    private void setData() {
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edtFullname.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(RegisterActivity.this, "Please enter fullname!", Toast.LENGTH_SHORT).show();
                } else if (edtPhoneNumber.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(RegisterActivity.this, "Please enter phone number!", Toast.LENGTH_SHORT).show();
                } else if (edtEmailAddress.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(RegisterActivity.this, "Please enter email address!", Toast.LENGTH_SHORT).show();
                } else if (edtUsername.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(RegisterActivity.this, "Please enter username!", Toast.LENGTH_SHORT).show();
                } else if (edtPassword.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(RegisterActivity.this, "Please enter password!", Toast.LENGTH_SHORT).show();
                } else if (edtConfPassword.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(RegisterActivity.this, "Please enter confirm password!", Toast.LENGTH_SHORT).show();
                } else if (!edtPassword.getText().toString().equalsIgnoreCase(edtConfPassword.getText().toString())) {
                    Toast.makeText(RegisterActivity.this, "Password does not match!", Toast.LENGTH_SHORT).show();
                } else {
                    progressbarManager.show();
                    UserRegisterRequest();
//                    auth.createUserWithEmailAndPassword(edtEmailAddress.getText().toString(), edtPassword.getText().toString())
//                            .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
//                                @Override
//                                public void onComplete(@NonNull Task<AuthResult> task) {
//                                    Toast.makeText(RegisterActivity.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
//
//                                    if (!task.isSuccessful()) {
//                                        Log.d("ERROR: ", task.getException().toString());
//
//                                        Toast.makeText(RegisterActivity.this, "Authentication failed." + task.getException(),
//                                                Toast.LENGTH_SHORT).show();
//                                    } else {
//                                        Toast.makeText(RegisterActivity.this, "Logged In successfull", Toast.LENGTH_SHORT).show();
//                                    }
//                                }
//                            });

                }
            }
        });
    }

    private void UserRegisterRequest() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentDate = df.format(c.getTime());

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> apiCall = apiInterface.getUserRegister(
                edtFullname.getText().toString(),
                edtPhoneNumber.getText().toString(),
                edtEmailAddress.getText().toString(),
                currentDate,
                edtUsername.getText().toString(),
                edtPassword.getText().toString(),
                edtReferalCode.getText().toString(),
                "0");

        apiCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    progressbarManager.dismiss();

                    try {
                        String res = response.body().string();

                        try {
//                            Type listType = new TypeToken<ArrayList<LoginModel>>() {
//                            }.getType();
//                            ArrayList<LoginModel> workList = new Gson().fromJson(res, listType);

                            Gson gson = new Gson();
                            Reader reader = new StringReader(res);
                            RegisterModel registerModel = gson.fromJson(reader, RegisterModel.class);
//
                            if (registerModel.getStatus().equalsIgnoreCase("1")) {
                                Toast.makeText(RegisterActivity.this, "" + registerModel.getMessage(), Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                                finish();

                            } else {
                                Toast.makeText(RegisterActivity.this, "" + registerModel.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            progressbarManager.dismiss();
                            Toast.makeText(RegisterActivity.this, getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    progressbarManager.dismiss();
                    Toast.makeText(RegisterActivity.this, getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressbarManager.dismiss();
                Toast.makeText(RegisterActivity.this, getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Init() {
        auth = FirebaseAuth.getInstance();
        btnSignUp = (Button) findViewById(R.id.btnSignUp);

        edtFullname = (EditText) findViewById(R.id.edtFullname);
        edtPhoneNumber = (EditText) findViewById(R.id.edtPhoneNumber);
        edtEmailAddress = (EditText) findViewById(R.id.edtEmailAddress);
        edtUsername = (EditText) findViewById(R.id.edtUsername);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        edtConfPassword = (EditText) findViewById(R.id.edtConfPassword);
        edtReferalCode = (EditText) findViewById(R.id.edtReferalCode);
    }

    @Override
    protected void onResume() {
//        overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
        super.onResume();
    }
}
