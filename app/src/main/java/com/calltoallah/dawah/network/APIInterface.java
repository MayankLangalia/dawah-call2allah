package com.calltoallah.dawah.network;

import com.calltoallah.dawah.firebase.MyResponse;
import com.calltoallah.dawah.firebase.NotificationSender;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface APIInterface {

    public static String API_FIREBASE = "https://fcm.googleapis.com";
    public static String API_DOMAIN = "http://calltoallah.com/dawah/";

    // Firebase Server Key ..
    public static String SERVER_KEY = "AAAASyMwQ5g:APA91bG5O1PSKyS12Emg1I4ozsc-_tUdu-RZ0pKO3OAEZYH9HKs6SDC8JTKRT22co5Qns8LDLQl608ZSS8IF9xRekrdXgHwe4Uze22LFaPD-FvLzYpH6vFbx_vOe_Fa6s4SqfjFLDrtq";

    @Headers({"Content-Type:application/json", "Authorization:key=" + SERVER_KEY})
    @POST("fcm/send")
    Call<MyResponse> sendNotifcation(@Body NotificationSender body);

    /* USER ----------------------------------------------------------- */
    // Login
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "user_login.php")
    @FormUrlEncoded
    Call<ResponseBody> getUserLogin(
            @Field("username") String username,
            @Field("password") String password,
            @Field("device_id") String device_id);

    // Login using Google
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "user_gmail_login.php")
    @FormUrlEncoded
    Call<String> getGoogleLogin(
            @Field("email") String email,
            @Field("name") String name,
            @Field("share_code") String share_code,
            @Field("file_name") String file_name);

    // Register
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "user_signup.php")
    @FormUrlEncoded
    Call<ResponseBody> getUserRegister(
            @Field("full_name") String full_name,
            @Field("phone_number") String phone_number,
            @Field("email") String email,
            @Field("created_on") String created_on,
            @Field("username") String username,
            @Field("password") String password,
            @Field("share_code") String share_code,
            @Field("account_type") String account_type
    );

    // Profile Update
    @Multipart
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "update_user.php")
    public Call<String> getUserUpdate(
            @Part("user_id") RequestBody user_id,
            @Part("phone_number") RequestBody phone_number,
            @Part("email") RequestBody email,
            @Part("username") RequestBody username,
            @Part MultipartBody.Part user_avatars
    );

    // Profile Information
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @GET(API_DOMAIN + "user_information.php?")
    public Call<ResponseBody> getProfileInfo(
            @Query("user_id") String user_id);

    // Change Password
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "change_password.php")
    @FormUrlEncoded
    Call<String> getUserPasswordChange(
            @Field("user_id") String user_id,
            @Field("new_password") String new_password
    );

    // Verification
    @Multipart
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "user_verification_process.php")
    public Call<String> addUserVerification(
            @Part("full_name") RequestBody full_name,
            @Part("address") RequestBody address,
            @Part("pan_card_number") RequestBody pan_card_number,
            @Part("uidai_number") RequestBody uidai_number,
            @Part MultipartBody.Part front_photo_id,
            @Part MultipartBody.Part back_photo_id,
            @Part MultipartBody.Part short_video,
            @Part MultipartBody.Part pan_card_photo,
            @Part MultipartBody.Part signature_photo,
            @Part("user_id") RequestBody user_id
    );

    // Forgot Password
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "forgot_password.php")
    @FormUrlEncoded
    public Call<String> getForgotPassword(
            @Field("email_id") String email_id
    );

    // For Getting Transaction list
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "user_transaction_list.php")
    @FormUrlEncoded
    public Call<String> getTransactionHistory(
            @Field("user_id") String user_id
    );

    // For Getting Wallet Amount
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "get_user_wallet_amount.php")
    @FormUrlEncoded
    public Call<String> getUserWallet(
            @Field("user_id") String user_id
    );

    // User Follower List
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @GET(API_DOMAIN + "get_follower_list.php?")
    public Call<String> getUserFollowerList(
            @Query("user_id") String user_id);

    // User Following List
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @GET(API_DOMAIN + "get_following_list.php?")
    public Call<String> getUserFollowingList(
            @Query("user_id") String user_id);

    // User Add Follow/Following
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "user_follow_unfollow.php")
    @FormUrlEncoded
    public Call<String> addUserFollowFollowingRequest(
            @Field("follower_id") String follower_id,
            @Field("following_id") String following_id);

    /* QURAN ----------------------------------------------------------- */
    // For Getting Quran Info
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "get_quran_dictionary_info.php")
    @FormUrlEncoded
    public Call<ResponseBody> getQuran(
            @Field("user_id") String user_id);

    /* TASBIH ----------------------------------------------------------- */
    // For Add
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "addTasbih.php")
    @FormUrlEncoded
    public Call<ResponseBody> addTasbih(
            @Field("user_id") String user_id,
            @Field("tasbih_count") String tasbih_count,
            @Field("tasbih_name") String tasbih_name,
            @Field("tasbih_timestamp") String tasbih_timestamp);

    // For Getting All
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "getTasbih.php")
    @FormUrlEncoded
    public Call<ResponseBody> getTasbih(
            @Field("user_id") String user_id);

    /* SALAAH ----------------------------------------------------------- */
    // For Add
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "addSalaah.php")
    @FormUrlEncoded
    public Call<ResponseBody> addSalaah(
            @Field("user_id") String user_id,
            @Field("salaah_name") String salaah_name,
            @Field("salaah_location") String salaah_location,
            @Field("salaah_timestamp") String salaah_timestamp);

    // For Getting All
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "getSalaah.php")
    @FormUrlEncoded
    public Call<ResponseBody> getSalaah(
            @Field("user_id") String user_id);

    /* TOP DONOR ----------------------------------------------------------- */
    // For Getting All
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @GET(API_DOMAIN + "top_donor.php?")
    public Call<String> getDonor();

    /* FUND RAISER ----------------------------------------------------------- */
    // For Getting All
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "get_top_fundraiser_list.php")
    @FormUrlEncoded
    public Call<String> getFundRaiser(
            @Field("user_id") String user_id);

    /* REFER&EARN ----------------------------------------------------------- */
    // For Getting All
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "user_added_through_earn_refer.php")
    @FormUrlEncoded
    public Call<String> getUserRefer(
            @Field("user_id") String user_id
    );

    /* Masjid/Madrasah ----------------------------------------------------------- */
    // For Add
    @Multipart
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "user_add_masjid.php")
    public Call<String> addMasjidMadrasah(
            @Part("user_id") RequestBody user_id,
            @Part("masjid_username") RequestBody masjid_username,
            @Part("masjid_name") RequestBody masjid_name,
            @Part("phone_number") RequestBody phone_number,
            @Part("address") RequestBody address,
            @Part("created_on") RequestBody created_on,
            @Part("about_masjid") RequestBody about_masjid,
            @Part("masjid_lat") RequestBody masjid_lat,
            @Part("masjid_lng") RequestBody masjid_lng,
            @Part("prayer_time") RequestBody prayer_time,
            @Part("prayer_name") RequestBody prayer_name,
            @Part("type") RequestBody type,
            @Part MultipartBody.Part image_path,
            @Part("total_students") RequestBody total_students,
            @Part("total_teacher") RequestBody total_teacher
    );

    @Multipart
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "update_masjid_madrassa_info.php")
    public Call<String> updateMasjidMadrasah(
            @Part("masjid_id") RequestBody masjid_id,
            @Part("masjid_name") RequestBody masjid_name,
            @Part("masjid_username") RequestBody masjid_username,
            @Part("address") RequestBody address,
            @Part("phone_number") RequestBody phone_number,
            @Part("about_masjid") RequestBody about_masjid,
            @Part("masjid_lat") RequestBody masjid_lat,
            @Part("masjid_lng") RequestBody masjid_lng,
            @Part("prayer_time") RequestBody prayer_time,
            @Part("prayer_name") RequestBody prayer_name,
            @Part("total_students") RequestBody total_students,
            @Part("total_teacher") RequestBody total_teacher,
            @Part MultipartBody.Part file_name
    );

    // For Delete (Admin)
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "delete_masjid.php")
    @FormUrlEncoded
    public Call<String> deleteMasjidRequest(
            @Field("user_id") String user_id,
            @Field("masjid_id") String masjid_id);

    // For Ownership Change
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "transfer_masjid_ownership.php")
    @FormUrlEncoded
    public Call<ResponseBody> addChangeAdminRequest(
            @Field("user_id") String user_id,
            @Field("masjid_id") String masjid_id,
            @Field("transfer_to_user_id") String transfer_to_user_id);

    // For Getting All
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "user_masjid_list.php")
    @FormUrlEncoded
    public Call<ResponseBody> getAllMasjidList(
            @Field("user_id") String user_id,
            @Field("masjid_lat") String masjid_lat,
            @Field("masjid_lng") String masjid_lng);

    // For Update Namaz timings
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "update_prayer_time.php")
    @FormUrlEncoded
    public Call<String> addMasjidTiming(
            @Field("user_id") String user_id,
            @Field("masjid_id") String masjid_id,
            @Field("prayer_time") String prayer_time,
            @Field("prayer_name") String prayer_name
    );

    // For Getting Transaction List
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "masjid_transaction_list.php")
    @FormUrlEncoded
    public Call<String> getMasjidTransactionHistory(
            @Field("masjid_id") String masjid_id
    );

    // For Upload Masjid Post
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "upload_masjid_post.php")
    @FormUrlEncoded
    public Call<String> addMasjidPost(
            @Field("doc_path") String doc_path,
            @Field("post_text") String post_text,
            @Field("donation_button") String donation_button,
            @Field("post_type") String post_type,
            @Field("masjid_id") String masjid_id,
            @Field("created_on") String created_on,
            @Field("user_id") String user_id,
            @Field("asked_donation_amount") String asked_donation_amount
    );

    // For Update Masjid
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "update_masjid_post.php")
    @FormUrlEncoded
    public Call<String> updateMasjidPost(
            @Field("post_id") String post_id,
            @Field("post_text") String post_text,
            @Field("post_type") String post_type,
            @Field("doc_path") String doc_path,
            @Field("donation_button") String donation_button,
            @Field("asked_donation_amount") String asked_donation_amount
    );

    // For Post Image Uplaod
    @Multipart
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "masjid_post_file_upload.php")
    public Call<String> addMasjidPostFile(
            @Part("user_id") RequestBody user_id,
            @Part MultipartBody.Part file_name
    );

    // For Getting masjid followers
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "get_masjid_followers_list.php")
    @FormUrlEncoded
    public Call<String> getFollowers(
            @Field("masjid_id") String masjid_id,
            @Field("user_id") String user_id);

    // For Add Like/Unlike
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "user_like_unlike_posts.php")
    @FormUrlEncoded
    public Call<String> getPostsLikeUnlike(
            @Field("masjid_id") String masjid_id,
            @Field("post_id") String post_id,
            @Field("user_id") String user_id
    );

    // For Add Share
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "user_like_share_view.php")
    @FormUrlEncoded
    public Call<String> getPostsShare(
            @Field("masjid_id") String masjid_id,
            @Field("post_id") String post_id,
            @Field("user_id") String user_id
    );

    // For Getting All Post
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "user_all_posts.php")
    @FormUrlEncoded
    public Call<String> getAllPosts(
            @Field("masjid_lat") String masjid_lat,
            @Field("masjid_lng") String masjid_lng,
            @Field("user_id") String user_id
    );

    // For Getting User All Post
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "user_all_posts.php")
    @FormUrlEncoded
    public Call<String> getUserAllPosts(
            @Field("user_id") String user_id,
            @Field("masjid_id") String masjid_id
    );

    // For Masjid/Madrasah Verification
    @Multipart
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "masjid_madrasah_verification.php")
    public Call<String> addMasjidVerification(
            @Part("masjid_id") RequestBody masjid_id,
            @Part("masjid_name") RequestBody masjid_name,
            @Part("registration_number") RequestBody registration_number,
            @Part MultipartBody.Part short_video,
            @Part MultipartBody.Part pan_card_photo,
            @Part MultipartBody.Part signature_photo,
            @Part("user_id") RequestBody user_id,
            @Part("bank_name") RequestBody bank_name,
            @Part("account_holder_name") RequestBody account_holder_name,
            @Part("account_number") RequestBody account_number,
            @Part("ifsc_code") RequestBody ifsc_code);

    // For Widrawal Request Status: Pending ..... 18-12-2020
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "masjid_widrawal_request_status.php")
    @FormUrlEncoded
    public Call<String> getRequestMasjidWidthdrawStatus(
            @Field("masjid_id") String masjid_id
    );

    // For Widrawal Request: Pending ..... 18-12-2020
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "masjid_widrawal_request.php")
    @FormUrlEncoded
    public Call<String> addWithdrawalRequest(
            @Field("masjid_id") String masjid_id,
            @Field("amount") String amount,
            @Field("user_id") String user_id
    );

    // For Add Follow/Unfollow
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "user_masjid_follow_list.php")
    @FormUrlEncoded
    public Call<String> addMasjidFollow(
            @Field("masjid_id") String masjid_id,
            @Field("user_id") String user_id
    );

    // For QR Code... Masjid Details
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "user_masjid_details.php")
    @FormUrlEncoded
    public Call<String> getMasjidDetails(
            @Field("masjid_id") String masjid_id
    );

    // For QR Code... Post Details
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "single_post_data.php")
    @FormUrlEncoded
    public Call<String> getPostDetails(
            @Field("post_id") String post_id
    );

    /* RAZORPAY ----------------------------------------------------------- */
    // For Order Using Wallet
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "payment_using_wallet.php")
    @FormUrlEncoded
    public Call<String> getPaymentUsingWallet(
            @Field("user_id") String user_id,
            @Field("amount") String amount,
            @Field("post_id") String post_id,
            @Field("masjid_id") String masjid_id,
            @Field("custom_order_id") String custom_order_id
    );

    // For Order Generate
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "get_user_order_id.php")
    @FormUrlEncoded
    public Call<String> getOrder(
            @Field("amount") String amount,
            @Field("charge_amount") String charge_amount,
            @Field("use_wallet") String use_wallet,
            @Field("masjid_id") String masjid_id,
            @Field("remaining_wallet_amount") String remaining_wallet_amount,
            @Field("user_id") String user_id,
            @Field("post_id") String post_id
    );

    // For Order Verification
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "payment_verification.php")
    @FormUrlEncoded
    public Call<String> addPaymentVerification(
            @Field("payment_id") String payment_id,
            @Field("order_id") String order_id,
            @Field("digital_signature") String digital_signature,
            @Field("usedWallet") String usedWallet,
            @Field("user_id") String user_id,
            @Field("post_id") String post_id,
            @Field("custom_order_id") String custom_order_id
    );

    /* CALENDAR ----------------------------------------------------------- */
    // For Add
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "addCalender.php")
    @FormUrlEncoded
    public Call<String> addCalender(
            @Field("event_date") String event_date,
            @Field("event_name") String event_name,
            @Field("user_id") String user_id
    );

    // For Getting All
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @GET(API_DOMAIN + "getCalender.php?")
    public Call<String> getCalender();

    /* UNUSED METHOD ----------------------------------------------------------- */
    // Getting Unverified User: Pending ..... 18-12-2020
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @GET(API_DOMAIN + "user_unverified_list.php?")
    public Call<String> getApprovalUserList();

    // Masjid Delete/Reject: Pending ..... 18-12-2020
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "masjid_delete.php")
    @FormUrlEncoded
    public Call<String> addMasjidReject(
            @Field("masjid_id") String masjid_id
    );

    /* ADMIN ----------------------------------------------------------- */
    // For Getting All users
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @GET(API_DOMAIN + "all_user_list.php?")
    public Call<String> getAllUsers();

    // For Masjid/Madrasah Approval
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "all_masjid_list.php")
    @FormUrlEncoded
    public Call<String> getApprovalList(
            @Field("admin_id") String admin_id
    );

    // For Masjid/Madrasah Approval
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "masjid_approve.php")
    @FormUrlEncoded
    public Call<String> addMasjidApprove(
            @Field("masjid_id") String masjid_id
    );

    // For User Approval
    @Headers({"Cache-control: no-cache", "Accept: application/json"})
    @POST(API_DOMAIN + "approve_user.php")
    @FormUrlEncoded
    public Call<String> addUserApprove(
            @Field("user_id") String user_id
    );
}
