package com.calltoallah.dawah.firebase;

public class NotificationSender {
    public Data data;
    public String to;
    public Data notification;

    public NotificationSender(Data data, String to) {
        this.data = data;
        this.to = to;
        this.notification = data;
    }
}